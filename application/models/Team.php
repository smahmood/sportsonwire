<?php
class Team extends CI_Model {
	
	function __construct(){
		parent::__construct();
		$this->load->database();
	}

	function create($val_array){
		$this->db->insert("teams", $val_array);
		return $this->db->insert_id();
	}
	

	function addMember($val_array){
		$this->db->insert("team_members", $val_array);
		return $this->db->insert_id();
	}

	function addGroundInfo($val_array){
		$this->db->insert("grounds", $val_array);
		return $this->db->insert_id();
	}

	function linkTeamGround($val_array){
		$this->db->insert("team_ground", $val_array);
		return $this->db->insert_id();	
	}

	function getTeamsManaged($user_id,$sports){
		$this->db->select("*");
		$this->db->from('teams');
		$this->db->join('team_members', 'team_members.team_id = teams.id');
		$this->db->where(array("user_id"=>$user_id,"sports"=>$sports,"role"=>1));
		$query = $this->db->get();
		
		if($query->num_rows() == 0)
			return array();				  
		else{
			return $query->result_array();			  
		}
	}

	function getTeamsCaptained($user_id,$sports){
		$this->db->select("*");
		$this->db->from('teams');
		$this->db->join('team_members', 'team_members.team_id = teams.id');
		$this->db->where(array("user_id"=>$user_id,"sports"=>$sports,"role"=>2));
		$query = $this->db->get();
		
		if($query->num_rows() == 0)
			return array();				  
		else{
			return $query->result_array();			  
		}
	}

	function getTeamsMember($user_id,$sports){
		$this->db->select("*");
		$this->db->from('teams');
		$this->db->join('team_members', 'team_members.team_id = teams.id');
		$this->db->where(array("user_id"=>$user_id,"sports"=>$sports,"role"=>3));
		$query = $this->db->get();
		
		if($query->num_rows() == 0)
			return array();				  
		else{
			return $query->result_array();			  
		}
	}
	function getTeamsManager($id){
		$this->db->select("*");
		$this->db->from('users');
		$this->db->join('team_members', 'team_members.user_id = users.id');
		$this->db->where(array("team_members.team_id"=>$id,"role"=>1));
		$query = $this->db->get();
		
		if($query->num_rows() == 0)
			return array();				  
		else{
			return $query->result_array();			  
		}
	}
	function getTeamsMembers($id){
		$this->db->select("*");
		$this->db->from('users');
		$this->db->join('team_members', 'team_members.user_id = users.id');
		$this->db->where(array("team_members.team_id"=>$id));
		$query = $this->db->get();
		
		if($query->num_rows() == 0)
			return array();				  
		else{
			return $query->result_array();			  
		}
	}
	function getTeamsMembersFooty($id){
		$this->db->select("users.*,team_members.user_id,team_members.status,footy_profile.player_position");
		$this->db->from('users');
		$this->db->join('team_members', 'team_members.user_id = users.id');
		$this->db->join('footy_profile', 'footy_profile.user_id = users.id');
		$this->db->where(array("team_members.team_id"=>$id));
		$query = $this->db->get();
		
		if($query->num_rows() == 0)
			return array();				  
		else{
			return $query->result_array();			  
		}
	}
	function getUserTeams($id){
		$this->db->select("teams.*,team_members.status");
		$this->db->from('team_members');
		$this->db->join('teams', 'team_members.team_id = teams.id');
		$this->db->where(array("team_members.user_id"=>$id,"status"=>1));
		$query = $this->db->get();
		
		if($query->num_rows() == 0)
			return array();				  
		else{
			return $query->result();			  
		}
	}

}

?>