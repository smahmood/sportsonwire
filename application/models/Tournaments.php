<?php
class Tournaments extends CI_Model {
	
	function __construct(){
		parent::__construct();
		$this->load->database();
	}

	function create($val_array){
		$this->db->insert("tournaments", $val_array);
		return $this->db->insert_id();
	}
	
    function getTournis($select,$table,$whereArr='',$sLimit=0,$order='',$sort='',$result_array=false){
		$this->db->select("tournaments.id AS id,tournaments.min_players AS aside,tournaments.name AS name,tournaments.location AS location,tournaments.image AS image ,cities.name AS cityName");
		$this->db->from($table);
		$this->db->join('cities', 'tournaments.city= cities.id');
		if($whereArr!="")
		$this->db->where($whereArr);
		if($sLimit!=0)
		$this->db->limit($sLimit);
		if($order!='' && $sort!='')
		{
			$this->db->order_by($order,$sort);
		}
		$query = $this->db->get();

		if($query->num_rows() == 0)
			return false;				  
		else
		{
			if($result_array==true)
				$result = $query->result_array();
			else
				$result = $query->result();
			return $result;				  
		}

    }

}

?>
