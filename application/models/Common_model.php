<?php
class Common_model  extends CI_Model 
{
	function __construct() 
	{
		parent::__construct();
	  	$this->load->database();
  	}
  
  	function isExists($param,$table,$return_id=false)
  	{
   		$whereClause='';
   		if($param!='')
   		{
			$this->db->select('*');
			$this->db->from($table);
			foreach($param as $index => $value)
			{
				$whereClause[$index]=$value;
			}
	
			$this->db->where($whereClause);
			$query = $this->db->get();

			if($query->num_rows() == 0)
				return false;
			else
			{
				$result=$query->result();

				if($return_id==true)
					return $result[0]->id;
				
				return true;
			}
		}
		else
			return false;
  	}
	function save($db_array,$table)
	{
		//echo "hehe";
		$this->db->insert($table, $db_array);
		//echo$this->db->last_query();
		return $this->db->insert_id();
	} 
	function update($db_array,$table,$whereArr,$escape=true)
	{
		$this->db->where($whereArr);
		$this->db->update($table, $db_array,NULL,NULL,$escape);
		return $this->db->affected_rows();
	}
	
	function getParameters($select,$table,$whereArr='',$result_array=false)
  	{
		$this->db->select($select);
		$this->db->from($table);
		if($whereArr!='')
		$this->db->where($whereArr);
		$query = $this->db->get();
		//echo$this->db->last_query();
		if($query->num_rows() == 0)
			return false;				  
		else
		{
			if($result_array==true)
				$result = $query->result_array();
			else
				$result = $query->result();
			return $result[0];				  
		}
	}
	

	function get_num_of_rows($select,$table,$whereArr)
	{
		$this->db->select($select);
		$this->db->from($table);
		if($whereArr!="")
		$this->db->where($whereArr);
		$query = $this->db->get();
		return $query->num_rows();
	}
	function getAllParameters($select,$table,$whereArr='',$result_array=false,$sLimit='',$db_group="default",$num_rows=false,$offset='',$order='',$sort='')
  	{
		if($db_group !="default")
			$this->load->database($db_group, TRUE);
		$this->db->select($select);
		$this->db->from($table);
		if($whereArr!="")
		$this->db->where($whereArr);
		if($sLimit!='' && $offset!='')
		$this->db->limit($sLimit,$offset);
		elseif($sLimit!='')
		$this->db->limit($sLimit);
		if($order!='' && $sort!='')
		{
			$this->db->order_by($order,$sort);
		}
		$query = $this->db->get();

		if($query->num_rows() == 0)
			return false;				  
		else
		{
			if($num_rows==true)
			return $query->num_rows();
			if($result_array==true)
				$result = $query->result_array();
			else
				$result = $query->result();
			return $result;				  
		}
	}
	function delete($table,$whereArr)
	{
		$this->db->where($whereArr);
		$this->db->delete($table);
	}
	function is_unique($value,$field,$table,$id)
  	{
    
	$this->db->select($field);
	
	
    $this->db->where($field,$value);
    $this->db->where('id !=', $id);


    $query = $this->db->get($table);
    
	if($query->num_rows() > 0){
    // the query returned data, so the email already exist.
	return false;
    }else{
    // record does not exists, so you can insert it.
	return true;
    }
		
  	}
	function get_max_id($table,$where="")
	{
		$this->db->select_max('id');
		$this->db->from($table);
		if($where!='')
        $this->db->where($where);
		$query = $this->db->get();
		if ($query->num_rows()==0)
        return null;
		return $query->result();
	}

	function getMatches($table,$query_col,$query,$where){
		$this->db->select("teams.id AS id,teams.team_logo AS logo,teams.team_name AS name,cities.name AS cityName,countries.name AS countryName");
		$this->db->from('teams');
		$this->db->join('cities', 'teams.city_id = cities.id');
		$this->db->join('countries','teams.country_id = countries.id');
		$this->db->like('teams.'.$query_col,$query);
		$this->db->where($where);
		$query = $this->db->get();


		/*$this->db->select("*");
		$this->db->from($table);
		$this->db->like($query_col,$query);
		$res = $this->db->get();*/

		return $query->result_array();
	}
    
    function get_teams_for_join_tournament($tournament_id,$user_id)
    {
        $data=array();
        $query=$this->db->select('team_id')->where('tournament_id',$tournament_id)->get('tournament_teams');
        $result=$query->result_array();
        $new_array='';
        $temp_array='';
        for ($k=0;$k<count($result);$k=$k+1)
        {
            $new_array[$k]=intval($result[$k]['team_id']);
            $temp_array[$k]=intval($result[$k]['team_id']);
        }
        
        $query=$this->db->select('team_id')->where('user_id',$user_id)->where('role',1)->where('status',1)->where_not_in('team_id',$new_array)->get('team_members');
        $result=$query->result_array();
        $new_array='';
        for ($k=0;$k<count($result);$k=$k+1)
        {
            $new_array[$k]=intval($result[$k]['team_id']);
        }
        
        $query=$this->db->select('team_id')->where('user_id',$user_id)->where('role',1)->where('status',1)->where_in('team_id',$temp_array)->get('team_members');
        $result_temp=$query->result_array();
        $temp_array='';
        for ($k=0;$k<count($result_temp);$k=$k+1)
        {
            $temp_array[$k]=intval($result_temp[$k]['team_id']);
        }
        $query=$this->db->select('id,team_name')->where_in('id',$temp_array)->get('teams');
        $data['teams_to_leave']=$query->result_array();
        
        $query1=$this->db->select('sports,gender,agegroup')->where('id',$tournament_id)->get('tournaments');
        $result1=$query1->row_array();
        
        $query=$this->db->select('id,team_name')->where('sports',$result1['sports'])->where('gender',$result1['gender'])->where('age_group',$result1['agegroup'])->where_in('id',$new_array)->get('teams');
        $data['teams_to_join']=$query->result_array();
        
        return $data;
        /*SELECT * FROM `teams` WHERE sports in (SELECT sports FROM `tournaments` WHERE id=1) and id in (SELECT team_id FROM `team_members` WHERE user_id=13 and role=1 and status=1 and team_id NOT IN (SELECT team_id FROM `tournament_teams` WHERE tournament_id=1));*/
    }
    
    function get_participating_teams_in_tournament($tournament_id)
    {
        $query=$this->db->select('teams.id,team_name,team_logo')->from('tournament_teams')->join('teams','tournament_teams.team_id = teams.id')->where('tournament_id',$tournament_id)->get();
        if ($query->num_rows() > 0)
            return $query->result_array();
        else
            return NULL;
    }
}

?>