<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// this line loads the library 
require('../sportsonwire/twilio-php/Services/Twilio.php'); 

class Sports extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/sports
	 *	- or -
	 * 		http://example.com/index.php/sports/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/sports/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));
		$this->load->model('Common_model');
		$this->load->model('Team');
		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
	}

	function user_should_not_be_logged_in(){

		if ($this->ion_auth->logged_in()){
			redirect('/', 'refresh');
		}else{
			return;
		}

	}

	function user_should_be_logged_in(){

		if (!$this->ion_auth->logged_in()){
			redirect('sports/login', 'refresh');
		}else{
			$this->data['logged_in'] = true;
			$this->data['basic'] = $this->ion_auth->user()->row();
		}

	}

	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			//redirect them to the login page
			$data['logged_in'] = false;
			$data['page_name'] = "landing";
			$this->load->template('landing',$data);
		}
		elseif (!$this->ion_auth->is_admin()) //remove this elseif if you want to enable this for non-admins
		{
			//redirect them to the home page because they must be an administrator to view this
			$data['logged_in'] = true;
			$data['basic'] = $this->ion_auth->user()->row();
            if ($data['basic']->activation_code == 0)
                $this->load->template('next_step', $data);
            else
                redirect('Sports/activate_account','refresh');
		}
		else
		{
			//yahan admin panel ka code aye ga GUJJAR 
		}
		
	}
    
    function activate_account()
    {
        $this->user_should_be_logged_in();
        
        if (is_null($this->data['basic']->msgs_day))
        {
            $this->send_msg("activation");
            date_default_timezone_set('UTC');
            $this->data['basic']->msgs_day=date("Y:m:d");
            $this->Common_model->update(array("msgs_day"=>$this->data['basic']->msgs_day),"users",array("id"=>$this->data['basic']->id));
        }
        
        date_default_timezone_set('UTC');
        if ($this->data['basic']->msgs_day != date("Y:m:d"))
        {
            $this->data['basic']->msgs_day=date("Y:m:d");
            $this->data['basic']->msgs_count=0;
            $this->Common_model->update(array("msgs_day"=>$this->data['basic']->msgs_day, "msgs_count" => $this->data['basic']->msgs_count),"users",array("id"=>$this->data['basic']->id));
        }
        
        $this->load->view('activate_account_header',$this->data);
        $this->load->view('activate_account',$this->data);
        $this->load->view('footer',$this->data);
    }
    
    function send_msg($msg_type)
    {
        $userdata=$this->ion_auth->user()->row();
        $msg_number=$userdata->phone;
        if ($msg_type == "activation")
        {
            $this->Common_model->update(array("msgs_count"=>$userdata->msgs_count+1),"users",array("id"=>$userdata->id));
            $msg_body="Please Use this Code: ".$userdata->activation_code." to activate your Sportsonwire Account. Thanks.\r\n\r\nSportsonwire Team";
        }
        $account_sid = 'AC9973a6c06b1cd71ea8a1982ceb392764'; 
        $auth_token = '471d797405638610c974ceb2a67f2cc8'; 
        $client = new Services_Twilio($account_sid, $auth_token); 

        $client->account->messages->create(array( 
            'To' => $msg_number, 
            'From' => "+16467982662", 
            'Body' => $msg_body,   
        ));
    }
    
    function verify_account()
    {
        $this->user_should_be_logged_in();
        
        if ($this->data['basic']->activation_code == $this->input->post('code'))
        {
            $this->Common_model->update(array("activation_code"=>0),"users",array("id"=>$this->data['basic']->id));
            redirect(base_url());
        }
        else
        {
            redirect(base_url()."Sports/activate_account");
        }
    }

	function login(){
		$this->user_should_not_be_logged_in();

		if($this->input->server('REQUEST_METHOD') == "GET"){
			
			$data['logged_in'] = false;
			$data['page_name'] = "login_register";
			$data['tab_active'] = "login";
			$this->load->template('login_register',$data);

		}else if($this->input->server('REQUEST_METHOD') == "POST"){
			
			$remember = (bool) $this->input->post('remember');
			if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember)){
				
				if($this->input->is_ajax_request()){
					$data['status'] = "success";
					$data['messages'] = $this->ion_auth->messages();
					echo json_encode($data);
				}else{
					redirect('/','refresh');
				}
			
			}else{
				
				$data['status'] = "failure";
				$data['messages'] = $this->ion_auth->errors();
				if($this->input->is_ajax_request()){
					echo json_encode($data);
				}else{
					$data['logged_in'] = false;
					$data['page_name'] = "login_register";
					$data['tab_active'] = "login";
					$this->load->template('login_register',$data);
				}

			}

		}

	}

	function signup(){
		$this->user_should_not_be_logged_in();
		if($this->input->server('REQUEST_METHOD') == "GET"){
			$data['logged_in'] = false;
			$data['page_name'] = "login_register";
			$data['tab_active'] = "join";
			$data['countries'] = $this->Common_model->getAllParameters('*','countries');
			$this->load->template('login_register',$data);
		}else if($this->input->server('REQUEST_METHOD') == "POST"){
			$username = strtolower($this->input->post('fname'));
			$email    = strtolower($this->input->post('email'));
			$password = $this->input->post('password');

			$additional_data = array(
                'activation_code' => mt_rand(100000,999999),
				'first_name' => $this->input->post('fname'),
				'last_name'  => $this->input->post('lname'),
				'country_id' => $this->input->post('country'),
				'city_id' => $this->input->post('cities'),
				'phone' => $this->input->post('mobile'),
				'profile_picture' => $this->input->post('profilepic'),
				'gender' => $this->input->post('gender'),
			);
			$id=$this->ion_auth->register($username, $password, $email, $additional_data);
			if ($id){
				$this->ion_auth->login($email, $password, false);
				if($this->input->is_ajax_request()){
					$invites=$this->Common_model->getAllParameters('*','invited_users',array('email'=>$this->input->post('email')));
					if($invites)
					{
						foreach($invites as $i)
						{
							if($this->Common_model->isExists(array("user_id"=>$id,"team_id"=>$i->team_id),"team_members"))
							{
							}
							else
							{
								$this->Common_model->save(array('team_id'=>$i->team_id,"user_id"=>$id,"role"=>3),"team_members");
							}
						}
					}
					$data['status'] = 'success';
					$data['messages'] = $this->ion_auth->messages();
					echo json_encode($data);
				}else{
					redirect('/','refresh');
				}
			}else{
				$data['status'] = 'failure';
				$data['messages'] = $this->ion_auth->errors();
				if($this->input->is_ajax_request()){
					echo json_encode($data);
				}else{
					$data['logged_in'] = false;
					$data['page_name'] = "login_register";
					$data['tab_active'] = "join";
					$this->load->template('login_register',$data);
				}
			}
		}
	}
	function getCities()
	{
		$cities=$this->Common_model->getAllParameters('*','cities',array('country_id'=>$this->input->post('id')));
		echo '<option value="">--Select--</option>';
		if($cities)
		{
			
			foreach($cities as $c)
			{
				echo '<option value="'.$c->id.'">'.$c->name.'</option>';
			}
		}
	}
	function about(){
		if ($this->ion_auth->logged_in()){
			$data['logged_in'] = true;
			$data['basic'] = $this->ion_auth->user()->row();
		}else{
			$data['logged_in'] = false;
		}
		$data['page_name'] = "about";
		$this->load->template('about',$data);
	}

	function account($purpose = "view"){
		$this->user_should_be_logged_in();
		$data['logged_in'] = true;
		if($purpose == "edit"){
			if($this->input->server('REQUEST_METHOD') == "GET"){

				$data['logged_in'] = true;
				$basic = $this->ion_auth->user()->row();
				$country=$this->Common_model->getParameters('name','countries',array('id'=>$basic->country_id));
				$city=$this->Common_model->getParameters('name','cities',array('id'=>$basic->city_id));
				$basic->country_name=$country->name;
				$basic->city_name=$city->name;
				$data['basic']=$basic;
				$data['countries']=$this->Common_model->getAllParameters('*','countries');
				$data['cities']=$this->Common_model->getAllParameters('*','cities',array('country_id'=>$basic->country_id));
				if ($this->data['basic']->activation_code == 0)
                $this->load->template('account_profile_edit', $data);
                else
                {
                    $this->load->view('activate_account_header',$data);
                    $this->load->view('account_profile_edit', $data);
                    $this->load->view('footer',$data);
                }

			}else if($this->input->server('REQUEST_METHOD') == "POST"){
				$this->Common_model->update(array("first_name"=>$this->input->post('fname'),'last_name'=>$this->input->post('lname'),'country_id'=>$this->input->post('country'),'city_id'=>$this->input->post('cities'),'gender'=>$this->input->post('gender'),"profile_picture"=>$this->input->post('profilepic')),"users",array("id"=>$this->ion_auth->get_user_id()));
				redirect(base_url(),'refresh');
			}
		}else{
			$basic = $this->ion_auth->user()->row();
			$country=$this->Common_model->getParameters('name','countries',array('id'=>$basic->country_id));
			$city=$this->Common_model->getParameters('name','cities',array('id'=>$basic->city_id));
			$basic->country_name=$country->name;
			$basic->city_name=$city->name;
			$data['basic']=$basic;
            if ($this->data['basic']->activation_code == 0)
                $this->load->template('account_profile', $data);
            else
            {
                $this->load->view('activate_account_header',$data);
                $this->load->view('account_profile', $data);
                $this->load->view('footer',$data);
            }
		}
	}
	
	function crop_image_square($source, $destination, $image_type, $square_size, $image_width, $image_height, $quality){
		if($image_width <= 0 || $image_height <= 0){return false;} //return false if nothing to resize
		
		if( $image_width > $image_height )
		{
			$y_offset = 0;
			$x_offset = ($image_width - $image_height) / 2;
			$s_size 	= $image_width - ($x_offset * 2);
		}else{
			$x_offset = 0;
			$y_offset = ($image_height - $image_width) / 2;
			$s_size = $image_height - ($y_offset * 2);
		}
		$new_canvas	= imagecreatetruecolor( $square_size, $square_size); //Create a new true color image
		
		//Copy and resize part of an image with resampling
		if(imagecopyresampled($new_canvas, $source, 0, 0, $x_offset, $y_offset, $square_size, $square_size, $s_size, $s_size)){
			$this->save_image($new_canvas, $destination, $image_type, $quality);
		}

		return true;
	}

	##### Saves image resource to file ##### 
	function save_image($source, $destination, $image_type, $quality){
		switch(strtolower($image_type)){//determine mime type
			case 'image/png': 
				imagepng($source, $destination); return true; //save png file
				break;
			case 'image/gif': 
				imagegif($source, $destination); return true; //save gif file
				break;          
			case 'image/jpeg': case 'image/pjpeg': 
				imagejpeg($source, $destination, $quality); return true; //save jpeg file
				break;
			default: return false;
		}
	}
	function normal_resize_image($source, $destination, $image_type, $max_size, $image_width, $image_height, $quality){
		
		if($image_width <= 0 || $image_height <= 0){return false;} //return false if nothing to resize
		
		//do not resize if image is smaller than max size
		if($image_width <= $max_size && $image_height <= $max_size){
			if($this->save_image($source, $destination, $image_type, $quality)){
				return true;
			}
		}
		
		//Construct a proportional size of new image
		$image_scale	= min($max_size/$image_width, $max_size/$image_height);
		$new_width		= ceil($image_scale * $image_width);
		$new_height		= ceil($image_scale * $image_height);
		
		$new_canvas		= imagecreatetruecolor( $new_width, $new_height ); //Create a new true color image
		
		//Copy and resize part of an image with resampling
		if(imagecopyresampled($new_canvas, $source, 0, 0, 0, 0, $new_width, $new_height, $image_width, $image_height)){
			$this->save_image($new_canvas, $destination, $image_type, $quality); //save resized image
		}

		return true;
	}
	function uploadImage()
	{
		$thumb_square_size 		= 200; //Thumbnails will be cropped to 200x200 pixels
		$max_image_size 		= 500; //Maximum image size (height and width)
		$thumb_prefix			= "thumb_"; //Normal thumb Prefix
		$destination_folder		= $_SERVER["DOCUMENT_ROOT"].'/sportsonwire/uploads/'; //upload directory ends with / (slash)
		$jpeg_quality 			= 90; //jpeg quality
		##########################################

		//continue only if $_POST is set and it is a Ajax request
		if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

			// check $_FILES['ImageFile'] not empty
			if(!isset($_FILES['image_file']) || !is_uploaded_file($_FILES['image_file']['tmp_name'])){
					die('Image file is Missing!'); // output error when above checks fail.
			}
		
			//uploaded file info we need to proceed
			$image_name = $_FILES['image_file']['name']; //file name
			$image_size = $_FILES['image_file']['size']; //file size
			$image_temp = $_FILES['image_file']['tmp_name']; //file temp

			$image_size_info 	= getimagesize($image_temp); //get image size
			
			if($image_size_info){
				$image_width 		= $image_size_info[0]; //image width
				$image_height 		= $image_size_info[1]; //image height
				$image_type 		= $image_size_info['mime']; //image type
			}else{
				die("Make sure image file is valid!");
			}

			//switch statement below checks allowed image type 
			//as well as creates new image from given file 
			switch($image_type){
				case 'image/png':
					$image_res =  imagecreatefrompng($image_temp); break;
				case 'image/gif':
					$image_res =  imagecreatefromgif($image_temp); break;			
				case 'image/jpeg': case 'image/pjpeg':
					$image_res = imagecreatefromjpeg($image_temp); break;
				default:
					$image_res = false;
			}

			if($image_res){
				//Get file extension and name to construct new file name 
				$image_info = pathinfo($image_name);
				$image_extension = strtolower($image_info["extension"]); //image extension
				$image_name_only = strtolower($image_info["filename"]);//file name only, no extension
				
				//create a random name for new image (Eg: fileName_293749.jpg) ;
				$new_file_name = $image_name_only. '_' .  rand(0, 9999999999) . '.' . $image_extension;
				
				//folder path to save resized images and thumbnails
				$thumb_save_folder 	= $destination_folder . $thumb_prefix . $new_file_name; 
				$image_save_folder 	= $destination_folder . $new_file_name;
				
				//call normal_resize_image() function to proportionally resize image
				if($this->normal_resize_image($image_res, $image_save_folder, $image_type, $max_image_size, $image_width, $image_height, $jpeg_quality))
				{
					//call crop_image_square() function to create square thumbnails
					if(!$this->crop_image_square($image_res, $thumb_save_folder, $image_type, $thumb_square_size, $image_width, $image_height, $jpeg_quality))
					{
						die('Error Creating thumbnail');
					}
					
					/* We have succesfully resized and created thumbnail image
					We can now output image to user's browser or store information in the database*/
					$data['status'] = 'success';
					$data['messages'] = $new_file_name;;
					echo json_encode($data);
				}
				
				imagedestroy($image_res); //freeup memory
			}
		}
	}
	function inviteEmail()
	{
		$this->user_should_be_logged_in();
		
		if($this->Common_model->isExists(array("email"=>$this->input->post('email')),"users"))
		{
			$id=$this->Common_model->isExists(array("email"=>$this->input->post('email')),"users",true);
			if($this->Common_model->isExists(array("team_id"=>$this->input->post('team'),'user_id'=>$id),"team_members"))
			{
				$response = Array(
					"status" => '-1',
					"message" => 'This user is already in your team'
				);
				print json_encode($response);
				return;
			}
			else
			{
				$id=$this->Common_model->isExists(array("email"=>$this->input->post('email')),"users",true);
				$this->Common_model->save(array("team_id"=>$this->input->post('team'),'user_id'=>$id,'role'=>1,'status'=>0),"team_members");
				$response = Array(
					"status" => '1',
					"message" => 'User is already on this website and team join request is sent to him.'
				);
				print json_encode($response);
				return;
			}
		}
		else
		{
			$this->Common_model->save(array('invited_by'=>$this->ion_auth->get_user_id(),"email"=>$this->input->post('email'),"team_id"=>$this->input->post('team')),"invited_users");
			$team=$this->Common_model->getParameters('*','teams',array('id'=>$this->input->post('team')));
			$basic = $this->ion_auth->user()->row();
			$message="<html>
				<body>
					<p>Hey, </p>
					<p>You have been invited to join ".$team->team_name." on Sportsonwire.com by ".	$basic->first_name." ".$basic->last_name."</p>
					<p><a target='_blank' href='".base_url()."sports/signup'>Click Here </a> to join Now.</p>
				</body>
			</html>";
			//echo $message;
			$to = $this->input->post('email');

			$subject = 'Join Your Team';

			$headers = "From: info@sportsonwire.com\r\n";
			$headers .= "Reply-To: info@sportsonwire.com\r\n";
			$headers .= "CC: omarfarooq.nu@gmail.com\r\n";
			$headers .= "Return-Path: info@sportsonwire.com\r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
			mail($to, $subject, $message, $headers);	
			$response = Array(
				"status" => '0',
				"message" => 'User is invited'
			);
			print json_encode($response);
			return;
		}
		
	}

	function coverphoto($purpose,$cover_type,$id){
		if($purpose == "upload"){

			$imagePath = $_SERVER["DOCUMENT_ROOT"].'/sportsonwire/uploads/';
			$allowedExts = array("jpeg", "jpg", "JPEG", "JPG");
			$temp = explode(".", $_FILES["img"]["name"]);
			$extension = end($temp);

			//Check write Access to Directory

			if(!is_writable($imagePath)){
				$response = Array(
					"status" => 'error',
					"message" => 'Can`t upload File; no write Access'
				);
				print json_encode($response);
				return;
			}

			if(in_array($extension, $allowedExts)){
				if ($_FILES["img"]["error"] > 0){
					$response = array(
						"status" => 'error',
						"message" => 'ERROR Return Code: '. $_FILES["img"]["error"],
					);
				}else{
					$filename = $_FILES["img"]["tmp_name"];
					list($width, $height) = getimagesize( $filename );


					move_uploaded_file($filename,  $imagePath . $_FILES["img"]["name"]);

					$response = array(
						"status" => 'success',
						"url" => base_url().'uploads/'.$_FILES["img"]["name"],
						"width" => $width,
						"height" => $height
					);
				}
			}else{
				$response = array(
					"status" => 'error',
					"message" => 'something went wrong, most likely file is to large for upload. check upload_max_filesize, post_max_size and memory_limit in you php.ini',
				);
			}

			print json_encode($response);
		}elseif ($purpose == "crop") {
			$imgUrl = $_POST['imgUrl'];
			// original sizes
			$imgInitW = $this->input->post('imgInitW');
			$imgInitH = $this->input->post('imgInitH');
			// resized sizes
			$imgW = $this->input->post('imgW');
			$imgH = $this->input->post('imgH');
			// offsets
			$imgY1 = $this->input->post('imgY1');
			$imgX1 = $this->input->post('imgX1');
			// crop box
			$cropW = $this->input->post('cropW');
			$cropH = $this->input->post('cropH');
			// rotation angle
			$angle = $this->input->post('rotation');

			$jpeg_quality = 100;

			date_default_timezone_set('Asia/Karachi');
			$output_file = 'teamCover_' . date('m-d-Y_H-i-s_', time()) . rand();
			$output_filename = $_SERVER["DOCUMENT_ROOT"].'/sportsonwire/uploads/' . $output_file;
			
			$what = getimagesize($imgUrl);

			switch(strtolower($what['mime'])){
			    case 'image/jpeg':
			        $img_r = imagecreatefromjpeg($imgUrl);
					$source_image = imagecreatefromjpeg($imgUrl);
					error_log("jpg");
					$type = '.jpeg';
			        break;
			    default: die('image type not supported');
			}

			//Check write Access to Directory
			if(!is_writable(dirname($output_filename))){
				$response = Array(
				    "status" => 'error',
				    "message" => 'Can`t write cropped File'
			    );	
			}else{
				// resize the original image to size of editor
			    $resizedImage = imagecreatetruecolor($imgW, $imgH);
				imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW, $imgH, $imgInitW, $imgInitH);
			    // rotate the rezized image
			    $rotated_image = imagerotate($resizedImage, -$angle, 0);
			    // find new width & height of rotated image
			    $rotated_width = imagesx($rotated_image);
			    $rotated_height = imagesy($rotated_image);
			    // diff between rotated & original sizes
			    $dx = $rotated_width - $imgW;
			    $dy = $rotated_height - $imgH;
			    // crop rotated image to fit into original rezized rectangle
				$cropped_rotated_image = imagecreatetruecolor($imgW, $imgH);
				imagecolortransparent($cropped_rotated_image, imagecolorallocate($cropped_rotated_image, 0, 0, 0));
				imagecopyresampled($cropped_rotated_image, $rotated_image, 0, 0, $dx / 2, $dy / 2, $imgW, $imgH, $imgW, $imgH);
				// crop image into selected area
				$final_image = imagecreatetruecolor($cropW, $cropH);
				imagecolortransparent($final_image, imagecolorallocate($final_image, 0, 0, 0));
				imagecopyresampled($final_image, $cropped_rotated_image, 0, 0, $imgX1, $imgY1, $cropW, $cropH, $cropW, $cropH);
				// finally output png image
				//imagepng($final_image, $output_filename.$type, $png_quality);
				imagejpeg($final_image, $output_filename.$type, $jpeg_quality);
				$response = Array(
				    "status" => 'success',
				    "url" => base_url() . 'uploads/' . $output_file.$type
			    );
			    if($cover_type=="team"){
			    	$this->Common_model->update(array("team_cover"=>$output_file.$type),"teams",array("id"=>$id));
			    	$cover=$this->Common_model->save(array('type'=>"2","type_id"=>$id,"path"=>$output_file.$type),"covers");
					$this->Common_model->save(array('team_id'=>$id,'post_type'=>3,'post_id'=>$cover,'post_by'=>1),"feed");
			    }else{
				    $this->Common_model->update(array("player_cover"=>$output_file.$type),"users",array("id"=>$id));
				    $cover=$this->Common_model->save(array('type'=>"1","type_id"=>$id,"path"=>$output_file.$type),"covers");
					$this->Common_model->save(array('user_id'=>$id,'post_type'=>3,'post_id'=>$cover,'post_by'=>2),"feed_user");
				}
			}
			print json_encode($response);
		}else{
			echo "getting here";
		}
	}

	function crestphoto($purpose,$id){
		if($purpose == "upload"){

			$imagePath = $_SERVER["DOCUMENT_ROOT"].'/sportsonwire/uploads/';
			$allowedExts = array("png", "PNG");
			$temp = explode(".", $_FILES["img"]["name"]);
			$extension = end($temp);

			//Check write Access to Directory

			if(!is_writable($imagePath)){
				$response = Array(
					"status" => 'error',
					"message" => 'Can`t upload File; no write Access'
				);
				print json_encode($response);
				return;
			}

			if(in_array($extension, $allowedExts)){
				if ($_FILES["img"]["error"] > 0){
					$response = array(
						"status" => 'error',
						"message" => 'ERROR Return Code: '. $_FILES["img"]["error"],
					);
				}else{
					$filename = $_FILES["img"]["tmp_name"];
					list($width, $height) = getimagesize( $filename );


					move_uploaded_file($filename,  $imagePath . $_FILES["img"]["name"]);

					$response = array(
						"status" => 'success',
						"url" => base_url().'uploads/'.$_FILES["img"]["name"],
						"width" => $width,
						"height" => $height
					);
				}
			}else{
				$response = array(
					"status" => 'error',
					"message" => 'something went wrong, most likely file is to large for upload. check upload_max_filesize, post_max_size and memory_limit in you php.ini',
				);
			}

			print json_encode($response);
		}elseif ($purpose == "crop") {
			$imgUrl = $_POST['imgUrl'];
			// original si9es
			$imgInitW = $this->input->post('imgInitW');
			$imgInitH = $this->input->post('imgInitH');
			// resized sizes
			$imgW = $this->input->post('imgW');
			$imgH = $this->input->post('imgH');
			// offsets
			$imgY1 = $this->input->post('imgY1');
			$imgX1 = $this->input->post('imgX1');
			// crop box
			$cropW = $this->input->post('cropW');
			$cropH = $this->input->post('cropH');
			// rotation angle
			$angle = $this->input->post('rotation');

			$png_quality = 9;

			date_default_timezone_set('Asia/Karachi');
			$output_file = 'teamCover_' . date('m-d-Y_H-i-s_', time()) . rand();
			$output_filename = $_SERVER["DOCUMENT_ROOT"].'/sportsonwire/uploads/' . $output_file;
			
			$what = getimagesize($imgUrl);

			switch(strtolower($what['mime'])){
			    case 'image/png':
			        $img_r = imagecreatefrompng($imgUrl);
					$source_image = imagecreatefrompng($imgUrl);
					$type = '.png';
			        break;
			    default: die('image type not supported');
			}

			//Check write Access to Directory
			if(!is_writable(dirname($output_filename))){
				$response = Array(
				    "status" => 'error',
				    "message" => 'Can`t write cropped File'
			    );	
			}else{
				// resize the original image to size of editor
			    $resizedImage = imagecreatetruecolor($imgW, $imgH);
				imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW, $imgH, $imgInitW, $imgInitH);
			    // rotate the rezized image
			    $rotated_image = imagerotate($resizedImage, -$angle, 0);
			    // find new width & height of rotated image
			    $rotated_width = imagesx($rotated_image);
			    $rotated_height = imagesy($rotated_image);
			    // diff between rotated & original sizes
			    $dx = $rotated_width - $imgW;
			    $dy = $rotated_height - $imgH;
			    // crop rotated image to fit into original rezized rectangle
				$cropped_rotated_image = imagecreatetruecolor($imgW, $imgH);
				imagecolortransparent($cropped_rotated_image, imagecolorallocate($cropped_rotated_image, 0, 0, 0));
				imagecopyresampled($cropped_rotated_image, $rotated_image, 0, 0, $dx / 2, $dy / 2, $imgW, $imgH, $imgW, $imgH);
				// crop image into selected area
				$final_image = imagecreatetruecolor($cropW, $cropH);
				imagecolortransparent($final_image, imagecolorallocate($final_image, 0, 0, 0));
				imagecopyresampled($final_image, $cropped_rotated_image, 0, 0, $imgX1, $imgY1, $cropW, $cropH, $cropW, $cropH);
				// finally output png image
				imagepng($final_image, $output_filename.$type, $png_quality);
				//imagejpeg($final_image, $output_filename.$type, $jpeg_quality);
				$response = Array(
				    "status" => 'success',
				    "url" => base_url() . 'uploads/' . $output_file.$type
			    );

			    $this->Common_model->update(array("team_logo"=>$output_file.$type),"teams",array("id"=>$id));
			}
			print json_encode($response);
		}else{
			echo "getting here";
		}
	}

	function getTeams(){
		$this->user_should_be_logged_in();
		if($this->input->get("type",TRUE) == "footy")
			$results = $this->Common_model->getMatches("teams","team_name",$this->input->get("q",TRUE),array("teams.sports"=>1));
		else
			$results = $this->Common_model->getMatches("teams","team_name",$this->input->get("q",TRUE),array("teams.sports"=>2));

		if($this->input->is_ajax_request()){
			echo json_encode($results);
		}else{
			$this->data['search_teams'] = $results;
			$this->load->template('team-search.php',$this->data);
		}
	}

	function AcceptRejectTeam()
	{
		if($this->input->post('status')==1)
		{
			$this->Common_model->update(array("status"=>1),"team_members",array("user_id"=>$this->ion_auth->get_user_id(),"team_id"=>$this->input->post('id')));
		}
		else
		{
			$this->Common_model->delete("team_members",array("user_id"=>$this->ion_auth->get_user_id(),"team_id"=>$this->input->post('id')));
		}
	}
	function AcceptRejectPlayer()
	{
		if($this->input->post('status')==1)
		{
			$this->Common_model->update(array("status"=>1),"team_members",array("user_id"=>$this->input->post('player'),"team_id"=>$this->input->post('id')));
		}
		else
		{
			$this->Common_model->delete("team_members",array("user_id"=>$this->input->post('player'),"team_id"=>$this->input->post('id')));
		}
	}
	function joinTeam()
	{
		$this->Common_model->save(array('team_id'=>$this->input->post('id'),"user_id"=>$this->ion_auth->get_user_id(),"team_id"=>$this->input->post('id'),"role"=>3,'status'=>-1),"team_members");	
	}
	function postToWall()
	{
		$id=$this->Common_model->save(array('wall'=>1,'text'=>$this->input->post('post'),"post_by"=>$this->ion_auth->get_user_id(),"post_type"=>1,'wall_id'=>$this->input->post('team')),"wall_posts");
		$this->Common_model->save(array('post_id'=>$id,"post_by_id"=>$this->ion_auth->get_user_id(),"post_type"=>5,'team_id'=>$this->input->post('team'),"post_by"=>2),"feed");
		
	}
	function postImageToWall()
	{
		$id=$this->Common_model->save(array('wall'=>1,'image_path'=>$this->input->post('pic'),'text'=>$this->input->post('post'),"post_by"=>$this->ion_auth->get_user_id(),"post_type"=>2,'wall_id'=>$this->input->post('team')),"wall_posts");
		$this->Common_model->save(array('post_id'=>$id,"post_by_id"=>$this->ion_auth->get_user_id(),"post_type"=>5,'team_id'=>$this->input->post('team'),"post_by"=>2),"feed");
		
	}
	function postToWallPlayer()
	{
		$id=$this->Common_model->save(array('wall'=>2,'text'=>$this->input->post('post'),"post_by"=>$this->ion_auth->get_user_id(),"post_type"=>1,'wall_id'=>$this->input->post('team')),"wall_posts");
		$this->Common_model->save(array('post_id'=>$id,"post_by_id"=>$this->ion_auth->get_user_id(),"post_type"=>5,'user_id'=>$this->input->post('team'),"post_by"=>2),"feed_user");
		
	}
	function postImageToWallPlayer()
	{
		$id=$this->Common_model->save(array('wall'=>2,'image_path'=>$this->input->post('pic'),'text'=>$this->input->post('post'),"post_by"=>$this->ion_auth->get_user_id(),"post_type"=>2,'wall_id'=>$this->input->post('team')),"wall_posts");
		$this->Common_model->save(array('post_id'=>$id,"post_by_id"=>$this->ion_auth->get_user_id(),"post_type"=>5,'user_id'=>$this->input->post('team'),"post_by"=>2),"feed_user");
		
	}
	function Featured($sports,$type)
	{
		if (!$this->ion_auth->logged_in()){
			redirect('sports/login', 'refresh');
		}else{
			$this->data['logged_in'] = true;
			$this->data['basic'] = $this->ion_auth->user()->row();
		}
		if($sports=="Football")
		{
			if($type=="Player")
			{
				$featuredPlayer=$this->Common_model->getParameters('*','featured_post',array("id"=>1));;
				$featuredPlayer->user=$this->Common_model->getParameters('*','users',array("id"=>$featuredPlayer->featured_id));;
				$featuredPlayer->user->city=$this->Common_model->getParameters('*','cities',array("id"=>$featuredPlayer->user->city_id));;
				$featuredPlayer->user->country=$this->Common_model->getParameters('*','countries',array("id"=>$featuredPlayer->user->country_id));;
				$this->data['featuredPlayer']=$featuredPlayer;
				$this->data['info']=$this->Common_model->getParameters('*','users',array("id"=>$featuredPlayer->featured_id));;
				$this->load->template('featuredplayer',$this->data);
			}
			else
			{
				$featuredTeam=$this->Common_model->getParameters('*','featured_post',array("id"=>2));;
				$featuredTeam->team=$this->Common_model->getParameters('*','teams',array("id"=>$featuredTeam->featured_id));;
				$featuredTeam->team->city=$this->Common_model->getParameters('*','cities',array("id"=>$featuredTeam->team->city_id));;
				$featuredTeam->team->country=$this->Common_model->getParameters('*','countries',array("id"=>$featuredTeam->team->country_id));;
				$this->data['featuredTeam']=$featuredTeam;
				$this->data['info']=$this->Common_model->getParameters('*','teams',array("id"=>$featuredTeam->featured_id));;
				$this->load->template('featuredteam',$this->data);				
			}
			
		}
	}
}
