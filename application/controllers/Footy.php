<?php defined('BASEPATH') OR exit('No direct script access allowed');

// this line loads the library 
require('../sportsonwire/twilio-php/Services/Twilio.php'); 

class Footy extends CI_Controller {

	protected $data = array("sports_page" => "footy");

	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));
		$this->load->model('Common_model');
		$this->load->model('Team');
		$this->load->model('Tournaments');
		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
		$this->user_should_be_logged_in();
	}

	function user_should_be_logged_in(){

		if (!$this->ion_auth->logged_in()){
			redirect('sports/login', 'refresh');
		}else{
			$this->data['logged_in'] = true;
			$this->data['basic'] = $this->ion_auth->user()->row();
			$this->data['page_title'] = "Football";
		}

	}

	//redirect if needed, otherwise display the user list
	function index(){	
		if($this->Common_model->isExists(array("user_id"=>$this->ion_auth->get_user_id()),"footy_profile"))
		{
			$featuredPlayer=$this->Common_model->getParameters('*','featured_post',array("id"=>1));;
			$featuredPlayer->user=$this->Common_model->getParameters('*','users',array("id"=>$featuredPlayer->featured_id));;
			$this->data['featuredPlayer']=$featuredPlayer;
			$featuredTeam=$this->Common_model->getParameters('*','featured_post',array("id"=>2));;
			$featuredTeam->team=$this->Common_model->getParameters('*','teams',array("id"=>$featuredTeam->featured_id));;
			$this->data['featuredTeam']=$featuredTeam;
			$this->data['footy']=$this->Common_model->getParameters('*','footy_profile',array("user_id"=>$this->ion_auth->get_user_id()));
			$this->data['tournaments']=$this->Tournaments->getTournis('*','tournaments',array('sports'=>1),5,'sdate','asc');
			$this->load->template('footy/home',$this->data);
		}
		else
		{
			$this->load->template('footy/register',$this->data);
			
		}
	}

	function register(){
		if($this->input->server('REQUEST_METHOD') == "GET"){
			$this->load->template('footy/register',$this->data);
		}else if($this->input->server('REQUEST_METHOD') == "POST"){
			$this->Common_model->save(array("user_id"=>$this->ion_auth->get_user_id(),"player_position"=>$this->input->post('position'),'player_foot'=>$this->input->post('foot'),'squad_number'=>$this->input->post('number'),'weight'=>$this->input->post('weight'),'height'=>$this->input->post('height')),"footy_profile");
			redirect('footy','refresh');
		}
	}

	function edit($id=0){
		if($this->Common_model->isExists(array("user_id"=>$this->ion_auth->get_user_id()),"footy_profile")){
			if($this->input->server('REQUEST_METHOD') == "GET"){

				if($id==$this->ion_auth->get_user_id()){
					$this->data['footy']=$this->Common_model->getParameters('*','footy_profile',array("user_id"=>$this->ion_auth->get_user_id()));
					$this->load->template('footy/edit',$this->data);
				}

			}else if($this->input->server('REQUEST_METHOD') == "POST"){

				$this->Common_model->update(array("player_position"=>$this->input->post('position'),'player_foot'=>$this->input->post('foot'),'squad_number'=>$this->input->post('number'),'weight'=>$this->input->post('weight'),'height'=>$this->input->post('height')),"footy_profile",array("user_id"=>$this->ion_auth->get_user_id()));
				redirect('footy','refresh');

			}
		}else{
			redirect('footy/register','refresh');
		}

	}

	function dashboard(){
		if($this->Common_model->isExists(array("user_id"=>$this->ion_auth->get_user_id()),"footy_profile")){
			$this->data['countries']=$this->Common_model->getAllParameters('*','countries');
			$this->data['cities']=$this->Common_model->getAllParameters('*','cities',array('country_id'=>$this->data['basic']->country_id));
			$this->data['my_teams'] = $this->Team->getTeamsManaged($this->ion_auth->get_user_id(),1);
			$this->data['my_teams'] = array_merge($this->data['my_teams'], $this->Team->getTeamsCaptained($this->ion_auth->get_user_id(),1));
			$this->data['my_teams'] = array_merge($this->data['my_teams'], $this->Team->getTeamsMember($this->ion_auth->get_user_id(),1));
			//print_r($this->data['my_teams']);die();
			$this->load->template('footy/dashboard',$this->data);
		}else{
			redirect('footy/register','refresh');
		}
	}

	function team($purpose="view",$team_name = null,$id='',$team_function=null){
		$team_name = $this->input->post("team_name");
		$sports_type = 1;
		$gender = $this->input->post("gender");
		$age_group = $this->input->post("age_group");
		$country_id = $this->input->post("country");
		$city_id = $this->input->post("city");
		$team_ground = $this->input->post("team_ground");
		$team_bio = $this->input->post("team_bio");

		if($purpose == "create"){
			$team_id = $this->Team->create(array("team_name"=>$team_name,"sports"=>$sports_type,"gender"=>$gender,"age_group"=>$age_group,"bio"=>$team_bio,"country_id"=>$country_id,"city_id"=>$city_id,"has_ground"=>$team_ground));
			$this->Team->addMember(array("team_id"=>$team_id,"user_id"=>$this->ion_auth->get_user_id(),"role"=>1));
			if($team_ground){
				$ground_id = $this->Team->addGroundInfo(array("name"=>$this->input->post('ground_name'),"type"=>2,"has_poles"=>$this->input->post("goal_posts")));
				$this->Team->linkTeamGround(array("team_id"=>$team_id,"ground_id"=>$ground_id));
			}
			$fanout = new Fanout\Fanout('ed446de2', 'jlQ0UR/EMaoOvFNScLQ75A==');
			$fanout->publish('create', 'Team Created!');
			redirect('footy/dashboard','refresh');
		}else if($purpose == "view"){
			if($this->Common_model->isExists(array("team_id"=>$id,"role"=>1,"user_id"=>$this->ion_auth->get_user_id()),"team_members"))
				{
					$this->data['iscaptain']=1;
				}
				else
				{
					$this->data['iscaptain']=0;
				}
			$this->data['info']=$this->Common_model->getParameters('*','teams',array('id'=>$id));
			$this->data['captain']=$this->Team->getTeamsManager($id);
			$this->data['members']=$this->Team->getTeamsMembers($id);
			if($this->Common_model->isExists(array("team_id"=>$id,"user_id"=>$this->ion_auth->get_user_id()),"team_members"))
			{
				$temp=$this->Common_model->getParameters("*","team_members",array("team_id"=>$id,"user_id"=>$this->ion_auth->get_user_id()));
				$this->data['isplayer']=1;
				$this->data['landingPlayer']=$temp;
			}
			else
			{
				$this->data['isplayer']=0;
			}
			if($team_function == "team-members"){
				$this->data['members']=$this->Team->getTeamsMembersFooty($id);
				if($this->Common_model->isExists(array("team_id"=>$id,"role"=>1,"user_id"=>$this->ion_auth->get_user_id()),"team_members"))
				{
					$this->data['iscaptain']=1;
				}
				else
				{
					$this->data['iscaptain']=0;
				}
				$this->load->template('team-members.php',$this->data);
			}elseif ($team_function == "team-gallery") {
				$this->load->template('team-gallery.php',$this->data);
			}
			elseif ($team_function == "Documents") {
				$this->load->template('team-documents.php',$this->data);
			}
			elseif ($team_function == "Message") {
				$this->load->template('team-message.php',$this->data);
			}
			else{
				$feed=$this->Common_model->getAllParameters('*','feed',array('team_id'=>$id),false,'',"default",false,'','time_stamp',$sort='desc');
				if($feed)
				{
					foreach($feed as $f)
					{
						if($f->post_type==2)
						{
							$user=$this->Common_model->getParameters('*','users',array('id'=>$f->post_id));
							$f->user=$user;
						}
						if($f->post_type==3)
						{
							$cover=$this->Common_model->getParameters('*','covers',array('id'=>$f->post_id));
							$f->cover=$cover;
						}
						if($f->post_type==5)
						{
							$wall=$this->Common_model->getParameters('*','wall_posts',array('id'=>$f->post_id));
							$f->wall=$wall;
							$user=$this->Common_model->getParameters('*','users',array('id'=>$f->post_by_id));
							$f->user=$user;
						}
						
					}
				}
				$this->data['feed']=$feed;
				$this->load->template('team',$this->data);
			}
		}
	}
	function playerProfile($id)
	{
		$this->data['info']=$this->Common_model->getParameters('*','users',array('id'=>$id));
		$this->data['teams']=$this->Team->getUserTeams($id);
		$feed=$this->Common_model->getAllParameters('*','feed_user',array('user_id'=>$id),false,'',"default",false,'','time_stamp',$sort='desc');
				if($feed)
				{
					foreach($feed as $f)
					{
						if($f->post_type==2)
						{
							$team=$this->Common_model->getParameters('*','teams',array('id'=>$f->post_id));
							$f->team=$team;
						}
						if($f->post_type==3)
						{
							$cover=$this->Common_model->getParameters('*','covers',array('id'=>$f->post_id));
							$f->cover=$cover;
						}
						if($f->post_type==5)
						{
							$wall=$this->Common_model->getParameters('*','wall_posts',array('id'=>$f->post_id));
							$f->wall=$wall;
							$user=$this->Common_model->getParameters('*','users',array('id'=>$f->post_by_id));
							$f->user=$user;
						}	
						
					}
				}
				$this->data['feed']=$feed;
		$this->load->template('player.php',$this->data);
	}
	function tournaments($purpose='view',$tournament_name='',$id='')
	{
		if($this->Common_model->isExists(array("user_id"=>$this->ion_auth->get_user_id()),"footy_profile")){
			if($purpose=='view')
			{
				$this->data['countries']=$this->Common_model->getAllParameters('*','countries');
				$this->data['cities']=$this->Common_model->getAllParameters('*','cities',array('country_id'=>$this->data['basic']->country_id));
				$this->data['tournaments'] = $this->Tournaments->getTournis('*','tournaments',array('sports'=>1),0,'sdate','asc');
				$this->load->template('footy/tournaments',$this->data);
			}
			else if($purpose=='create')
			{
				$tournament_name = $this->input->post("tournament_name");
				$sports_type = 1;
				$gender = $this->input->post("gender");
				$age_group = $this->input->post("age_group");
				$desc = $this->input->post("desc");
				$city_id = $this->input->post("city");
				$location= $this->input->post("location");
				$tournament_min_players = $this->input->post("tournament_min_players");
				$tournament_max_players = $this->input->post("tournament_max_players");
				$tournament = $this->Tournaments->create(array("name"=>$tournament_name,"min_players"=>$tournament_min_players,"max_players"=>$tournament_max_players,"gender"=>$gender,"agegroup"=>$age_group,"desc"=>$desc,"city"=>$city_id,"location"=>$location,"created_by"=>$this->ion_auth->get_user_id(),"sports"=>1,"sdate"=>$this->input->post("sdate"),"edate"=>$this->input->post("edate")));
				redirect('footy/tournaments','refresh');
			}
			else if($purpose=="details")
			{
				$this->data['info']=$this->Common_model->getParameters('*','tournaments',array('id'=>$id));
				$this->data['creator']=$this->Common_model->getParameters('*','users',array('id'=>$this->data['info']->created_by));
				$feed=$this->Common_model->getAllParameters('*','feed_tournament',array('tournament_id'=>$id),false,'',"default",false,'','time_stamp',$sort='desc');
				if($feed)
				{
					foreach($feed as $f)
					{
						if($f->post_type==2)
						{
							$team=$this->Common_model->getParameters('*','teams',array('id'=>$f->post_id));
							$f->team=$team;
						}
						if($f->post_type==3)
						{
							$cover=$this->Common_model->getParameters('*','covers',array('id'=>$f->post_id));
							$f->cover=$cover;
						}
						if($f->post_type==5)
						{
							$wall=$this->Common_model->getParameters('*','wall_posts',array('id'=>$f->post_id));
							$f->wall=$wall;
							$user=$this->Common_model->getParameters('*','users',array('id'=>$f->post_by_id));
							$f->user=$user;
						}
						
					}
				}
				$this->data['feed']=$feed;
                $current_user_id=$this->ion_auth->get_user_id();
                if ($current_user_id == $this->data['info']->created_by)
                {
                    $this->data['is_tournament_creator']=true;
                }
                else
                {
                    $this->data['is_tournament_creator']=false;
                }
                $this->data['join_leave_tournament_teams']=$this->Common_model->get_teams_for_join_tournament($id,$current_user_id);
                $this->data['participating_teams']=$this->Common_model->get_participating_teams_in_tournament($id);
				$this->load->template('footy/tournament_details',$this->data);
			}
		}
        else
        {
			redirect('footy/register','refresh');
		}
		
		
	}
    function joinTournament()
    {
        $this->Common_model->save(array('tournament_id' => $this->input->post('tournament_id'),'team_id' => $this->input->post('team_id')),"tournament_teams");
    }
    function leaveTournament()
    {
        $this->Common_model->delete("tournament_teams",array("tournament_id"=>$this->input->post('tournament_id'),"team_id"=>$this->input->post('team_id')));
    }
}
