<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cric extends CI_Controller {

	protected $data = array("sports_page" => "cric");

	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));
		$this->load->model('Common_model');
		$this->load->model('Team');
		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
		$this->user_should_be_logged_in();
	}

	function user_should_be_logged_in(){

		if (!$this->ion_auth->logged_in()){
			redirect('sports/login', 'refresh');
		}else{
			$this->data['logged_in'] = true;
			$this->data['basic'] = $this->ion_auth->user()->row();
			$this->data['page_title'] = "Cricket";
		}

	}

	//redirect if needed, otherwise display the user list
	function index()
	{
		/*if($this->Common_model->isExists(array("user_id"=>$this->ion_auth->get_user_id()),"cric_profile"))
		{
			$this->data['cric']=$this->Common_model->getParameters('*','cric_profile',array("user_id"=>$this->ion_auth->get_user_id()));
			$this->load->template('cric/home',$this->data);
		}
		else
		{
			$this->load->template('cric/register',$this->data);
			
		}*/
		$this->load->template('comingsoon',$this->data);

	}

	function register(){
		if($this->input->server('REQUEST_METHOD') == "GET"){
			$this->load->template('cric/register',$this->data);
		}else if($this->input->server('REQUEST_METHOD') == "POST"){
			$this->Common_model->save(array("user_id"=>$this->ion_auth->get_user_id(),"player_role"=>$this->input->post('role'),'batting_style'=>$this->input->post('batting'),'bowling_style'=>$this->input->post('bowling'),'bowling_arm'=>$this->input->post('bowling_arm')),"cric_profile");
			redirect('cric','refresh');
		}
	}

	function edit($id=0){
		if($this->Common_model->isExists(array("user_id"=>$this->ion_auth->get_user_id()),"cric_profile")){
			if($this->input->server('REQUEST_METHOD') == "GET"){

				if($id==$this->ion_auth->get_user_id()){
					$this->data['cric']=$this->Common_model->getParameters('*','cric_profile',array("user_id"=>$this->ion_auth->get_user_id()));
					$this->load->template('cric/edit',$this->data);
				}

			}else if($this->input->server('REQUEST_METHOD') == "POST"){

				$this->Common_model->update(array("player_role"=>$this->input->post('role'),'batting_style'=>$this->input->post('batting'),'bowling_style'=>$this->input->post('bowling'),'bowling_arm'=>$this->input->post('bowling_arm')),"cric_profile",array("user_id"=>$this->ion_auth->get_user_id()));
				redirect('cric','refresh');

			}
		}else{
			redirect('cric/register','refresh');
		}

	}

	function dashboard(){
		if($this->Common_model->isExists(array("user_id"=>$this->ion_auth->get_user_id()),"cric_profile")){
			$this->data['countries']=$this->Common_model->getAllParameters('*','countries');
			$this->data['cities']=$this->Common_model->getAllParameters('*','cities',array('country_id'=>$this->data['basic']->country_id));
			$this->data['my_teams'] = $this->Team->getTeamsManaged($this->ion_auth->get_user_id(),2);
			array_push($this->data['my_teams'], $this->Team->getTeamsCaptained($this->ion_auth->get_user_id(),2));
			array_push($this->data['my_teams'], $this->Team->getTeamsMember($this->ion_auth->get_user_id(),2));
			$this->load->template('cric/dashboard',$this->data);
		}else{
			redirect('cric/register','refresh');
		}
	}

	function team($purpose="view"){
		$team_name = $this->input->post("team_name");
		$sports_type = 2;
		$gender = $this->input->post("gender");
		$age_group = $this->input->post("age_group");
		$country_id = $this->input->post("country");
		$city_id = $this->input->post("city");
		$team_ground = $this->input->post("team_ground");

		if($purpose == "create"){
			$team_id = $this->Team->create(array("team_name"=>$team_name,"sports"=>$sports_type,"gender"=>$gender,"age_group"=>$age_group,"country_id"=>$country_id,"city_id"=>$city_id,"has_ground"=>$team_ground));
			$this->Team->addMember(array("team_id"=>$team_id,"user_id"=>$this->ion_auth->get_user_id(),"role"=>1));
			if($team_ground){
				$ground_id = $this->Team->addGroundInfo(array("name"=>$this->input->post('ground_name'),"type"=>1,"ground_size"=>$this->input->post("ground_size")));
				$this->Team->linkTeamGround(array("team_id"=>$team_id,"ground_id"=>$ground_id));
			}
			redirect('cric/dashboard','refresh');
		}else if($purpose == "view"){
			pass;
		}
	}
}