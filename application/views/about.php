<div class="divide80"></div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2>About SportsOnWire</h2>
            <p class="lead">
                SportsOnWire is an interactive and well designed sports management application. SportsOnWire take the hassle out of managing teams and tournaments. SportsOnWire not only provides an eye catching interface but also an easy to use user panel. We promise you that you would find SportsOnWire quite helful and also will notice the ease of access. If you do run into any problems our team is there to guide you through the problem. So lets Get into the Game.
            </p>
        </div>
    </div>
    <div class="divide40"></div>
    <div class="row">
        <div class="col-md-4 margin20">
            <div class="me-hobbies">
            	<img src="<?php echo base_url(); ?>img/cartoon-manager-players.png" alt="">
            </div>
        </div>
        <div class="col-md-8 margin20">
            <div class="me-hobbies">
                <h4><i class="fa fa-gamepad"></i> Manage a Team </h4>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut turpis magna, sodales quis ultrices non, semper id lorem. 
                </p>
            </div><!--me hobbies-->
        </div><!--hobbies col-->
    </div>
    <div class="divide40"></div>
    <div class="row">
        <div class="col-md-4 margin20">
            <div class="me-hobbies">
            	<img src="<?php echo base_url(); ?>img/cartoon-players.png" alt="">
            </div>
        </div>
        <div class="col-md-8 margin20">
            <div class="me-hobbies">
                <h4><i class="fa fa-gamepad"></i> Play for a Team </h4>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut turpis magna, sodales quis ultrices non, semper id lorem. 
                </p>
            </div><!--me hobbies-->
        </div><!--hobbies col-->
    </div>
</div><!--me page inner-->
<div class="divide80"></div>
<section class="contact-me">
    <div class="container">
        <div class="row">
            <div class="col-md-12 margin20 text-center">
                <h4>Follow SportsOnWire</h4>
            </div>
            <div class="col-md-12 text-center">
                <ul class="list-inline social-1">
                    <li><a href="<?php echo base_url(); ?>#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="<?php echo base_url(); ?>#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="<?php echo base_url(); ?>#"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="<?php echo base_url(); ?>#"><i class="fa fa-pinterest"></i></a></li>
                    <li><a href="<?php echo base_url(); ?>#"><i class="fa fa-dribbble"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</section>