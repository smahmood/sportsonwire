<div class="divide80"></div>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="center-heading">
                <h2>Search Results</h2>
                <span class="center-line"></span>
            </div>
        </div>                   
    </div><!--center heading row-->
    <div class="row">
        <div class="col-sm-12">
            <?php foreach($search_teams as $single_team){ ?>
                <div class="col-sm-4 margin30">
                    <div class="latest-new">
                        <a href="<?php echo base_url() . 'footy/team/view/' . str_replace(" ", "-", $single_team['name']).'/'.$single_team['id']; ?>"><img src="<?php echo base_url(); ?>img/empty-team-photo.jpg" class="img-responsive" alt="" style="width:100%;"></a>
                        <div class="blog-date">
                            <a href="<?php echo base_url() . 'footy/team/view/' . str_replace(" ", "-", $single_team['name']).'/'.$single_team['id']; ?>"><img src="<?php echo base_url() . 'uploads/'. $single_team['logo'];?>" class="img-responsive" alt=""></a>
                        </div>
                        <div class="l-news-desc account-info">
                            <h3><a href="<?php echo base_url() . 'footy/team/view/' . str_replace(" ", "-", $single_team['name']).'/'.$single_team['id']; ?>"><?php echo $single_team['name']; ?></a></h3>
                        </div>
                        <div class="panel-footer clearfix">
                            <span><?php echo $single_team['cityName']; ?></span><br/>
                            <span style="color:#989898;"><?php echo $single_team['countryName']; ?></span>	
                        </div>
                    </div><!--latest news-->
                </div><!--latest news col-->
            <?php } ?>
        </div>

    </div>
</div>
<div class="clear"></div>
<div class="divide80"></div>