<div class="divide80"></div>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default clearfix">
                <div class="panel-heading clearfix">
                    <h1 class="panel-title pull-left account-name">Account Profile</h1>
                    <div class="btn-group pull-right account-header-btn" role="group">
                        <a href="#" class="btn btn-default active">
                            <span><i class="fa fa-eye red"></i> View Account</span>
                        </a>
                        <a href="<?php echo base_url();?>sports/account/edit" class="btn btn-default">
                            <span><i class="fa fa-pencil red"></i> Edit Account</span>
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 account-info">
                            <span class="h4"><i class="fa fa-cogs"></i> <?php echo $basic->first_name . " " . $basic->last_name; ?></span>
                            <hr/>
                            <div class="col-md-6 account-info-title">Name</div><div class="col-md-6"><?php echo $basic->first_name . " " . $basic->last_name; ?></div>
                            <div class="divide40"></div>
                            <div class="col-md-6 account-info-title">Email</div><div class="col-md-6"><?php echo $basic->email; ?></div>
                            <div class="divide40"></div>
                            <div class="col-md-6 account-info-title">Location</div><div class="col-md-6"><?php echo $basic->city_name.",".$basic->country_name; ?></div>
                            <div class="divide40"></div>
							 <div class="col-md-6 account-info-title">Mobile</div><div class="col-md-6"><?php echo $basic->phone; ?></div>
							 <div class="divide40"></div>
                            <div class="col-md-6 account-info-title">Gender</div><div class="col-md-6"><?php if($basic->gender==1)
							{
								echo 'Male';
							}
							else
							{
								echo 'Female';
							}
							?></div>
                            <div class="divide40"></div>
                            <div class="col-md-6 account-info-title">Status</div><div class="col-md-6">Registered</div>
                            <div class="divide40"></div>
                        </div>
						<div class="col-md-3">
							<img src="<?php echo base_url();?>uploads/thumb_<?php if($basic->profile_picture == ''){echo 'default.jpg';}else{echo $basic->profile_picture;}?>" style="width:100%;"/>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="divide80"></div>