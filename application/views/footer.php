<footer id="footer">
            <div class="container">

                <div class="row">
                    <div class="col-md-6 col-md-offset-3 text-center margin30">
                        <div class="footer-col footer-3">
                            <h3>sportsonwire</h3>
                            <p>
                                Wanna know more about us? Join our ever growing community.
                            </p>
                            <ul class="list-inline social-1">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                            </ul>
                        </div>                        
                    </div><!--footer col-->
          
             
                   

                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="footer-btm">
                            <span>&copy;2015 SportsOnWire. All rights reserved.</span>
                        </div>
                    </div>
                </div>
            </div>
        </footer><!--default footer end here-->

        <!--scripts and plugins -->
        <!--must need plugin jquery-->
        <script src="<?php echo base_url(); ?>js/jquery.min.js"></script>        
		<script src="<?php echo base_url(); ?>js/jquery-ui.min.js"></script>        
        <!--bootstrap js plugin-->
        <script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js" type="text/javascript"></script>       
        <!--easing plugin for smooth scroll-->
        <script src="<?php echo base_url(); ?>js/jquery.easing.1.3.min.js" type="text/javascript"></script>
        <!--sticky header-->
        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.sticky.js"></script>
        <!--flex slider plugin-->
        <script src="<?php echo base_url(); ?>js/jquery.flexslider-min.js" type="text/javascript"></script>
        <!--parallax background plugin-->
        <script src="<?php echo base_url(); ?>js/jquery.stellar.min.js" type="text/javascript"></script>
        
        
        <!--digit countdown plugin-->
        <script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
        <!--digit countdown plugin-->
        <script src="<?php echo base_url(); ?>js/jquery.counterup.min.js" type="text/javascript"></script>
        <!--on scroll animation-->
        <script src="<?php echo base_url(); ?>js/wow.min.js" type="text/javascript"></script> 
        <!--owl carousel slider-->
        <script src="<?php echo base_url(); ?>js/owl.carousel.min.js" type="text/javascript"></script>
        <!--popup js-->
        <script src="<?php echo base_url(); ?>js/jquery.magnific-popup.min.js" type="text/javascript"></script>
        <!--you tube player-->
        <script src="<?php echo base_url(); ?>js/jquery.mb.YTPlayer.min.js" type="text/javascript"></script>
        
        
        <!--customizable plugin edit according to your needs-->
        <script src="<?php echo base_url(); ?>js/custom.js" type="text/javascript"></script>
        <!--revolution slider plugins-->
        <script src="<?php echo base_url(); ?>rs-plugin/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>rs-plugin/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js/revolution-custom.js" type="text/javascript"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.form.min.js"></script>
        <script src="http://ed446de2.fanoutcdn.com/bayeux/static/faye-browser-min.js"></script>
        <script src="<?php echo base_url(); ?>js/croppic.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/main.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js/validator.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js/handlebars.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js/bootstrap-typeahead.js" type="text/javascript"></script>
		<script src="<?php echo base_url();?>js/jquery.countdown.js" type="text/javascript"></script>
        <script>
            base_url='<?php echo base_url();?>';
        </script>
		<script>
			 $(function() {
				$( "#tournament_sdate" ).datepicker({
					minDate: 0,
					dateFormat: 'dd-mm-yy'
				});
				$( "#tournament_edate" ).datepicker({
					minDate: 0,
					dateFormat: 'dd-mm-yy'
				});
			});
		</script>
    </body>
</html>