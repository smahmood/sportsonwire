<div class="container">
	<!--<section class="me-intro parallax" data-stellar-background-ratio="0.5">
	    <div class="container text-left">
	        <h2 class="animated slideInLeft">Hello! myself assan</h2>
	        <p class="lead animated slideInLeft ">I am a Graphics and Web designer </p>
	        <p><a href="#" class="btn btn-lg btn-theme-bg">Hire Me</a></p>
	    </div>
	</section>me intro parallax-->
	<div class="clear"></div>
	<div class="clear"></div>
	<div class="divide80"></div>
	<div class="row">
		<div class="col-sm-4 margin30">
            <div class="latest-new">
                <img src="<?php echo base_url(); ?>uploads/<?php if($info->profile_picture==""){ echo "default.jpg"; }else{ echo $info->profile_picture; } ?>" class="img-responsive" alt="" style="width:100%;">
                <div class="l-news-desc account-info clearfix">
                    <h3><a href="#"><?php echo $info->first_name." ".$info->last_name ;?></a></h3>
					<p style="text-align:center;"><?php if($info->bio!=""){ echo substr($info->bio, 0, 70)."...";; } ?></p>
				<?php if($info->bio!=""){ ?>	<button class="btn border-theme pull-right ">Read More</button> <?php } ?>
                </div>
				
                <div class="panel-footer clearfix">
                    
                    <div class="pull-left clearfix">
						<p style="color:#989898;">Contact : <?php echo $info->phone ;?></p>
						<p style="color:#989898;">Email : <?php echo $info->email ;?></p>
						<p style="color:#989898;">City : <?php echo $featuredPlayer->user->city->name ;?></p>
						<p style="color:#989898;">Country : <?php echo $featuredPlayer->user->country->name ;?></p>
						<p style="color:#989898;">Gender : <?php if($info->gender==1){ echo "Male"; } else{ echo "Female"; }  ;?></p>
					</div>
                </div>
            </div><!--latest news-->
			
            <div class="divide40"></div>
            
        </div><!--latest news col-->
       <div class="col-md-8">
			<div class="center-heading">
                <h2>Feature <strong>Player</strong> of the week</h2>
                <span class="center-line"></span>
            </div>
			<div class="center-heading">
                <p><?php echo $featuredPlayer->featured_text; ?> </p>
               
            </div>
		</div>
	</div>
	<div class="clear"></div>
	<div class="divide80"></div>
</div>