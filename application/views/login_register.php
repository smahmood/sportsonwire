<div class="divide80"></div>
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <div class="alert-danger alert authentication-alert" style="display:none;"></div>
            <div role="tabpanel">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs text-center" role="tablist">
                    <li role="presentation" <?php if($tab_active == "login"){echo "class='active'";} ?>><a href="#login" aria-controls="login" role="tab" data-toggle="tab">Login</a></li>
                    <li role="presentation" <?php if($tab_active == "join"){echo "class='active'";} ?>><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Register</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane <?php if($tab_active == "login"){echo "active";} ?>" id="login">
                        <form action="#" class="login_form" id="signinForm1" role="form" data-toggle="validator">
                            <div class="form-group">
                                <label for="InputEmail1">Email address</label>
                                <input name="identity" required type="email" class="form-control" id="InputEmail1" placeholder="Enter email" data-error="Please enter a valid email">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="InputPassword1">Password</label>
                                <input name="password" required type="password" class="form-control" id="InputPassword1" placeholder="Password" data-error="Password field cannot be empty">
                                <div class="help-block with-errors"></div>
                            </div>
								<div id="err1" class="input-group" style="color:red">
										
								</div>									
							<div class="checkbox pull-left">
                                    <label>
                                        <input name="remember" type="checkbox"> Remember me
                                    </label>
                                </div>	
							<div class="clearfix"></div>
                            <div class="pull-left">

                                <p><a href="<?php echo base_url(); ?>#">Forget password?</a></p>

                            </div>
                            <div class="pull-right">
                                <button type="submit" class="btn btn-theme-dark">Login</button>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div><!--login tab end-->
                    <div role="tabpanel" class="tab-pane <?php if($tab_active == "join"){echo "active";} ?>" id="profile">
                        <form id="signupForm" role="form" data-toggle="validator" action="<?php echo base_url(); ?>sports/signup" class="signup-form" method="POST">
                            <div class="form-group">
                                <label for="Inputname">First Name</label>
                                <input required name="fname" type="text" class="form-control" id="Inputfname" placeholder="First Name" data-error="Please enter First Name">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="Inputlname">Last Name</label>
                                <input required name="lname" type="text" class="form-control" id="Inputlname" placeholder="Last Name" data-error="Please enter First Name">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="InputEmail11">Email address</label>
                                <input required name="email" type="email" class="form-control" id="InputEmail11" placeholder="Enter email" data-error="Please enter valid email">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label for="pass1">Password</label>
                                <input id="pass1" name="password" required type="password" class="form-control"  placeholder="Password"  data-minlength="6" data-error="Minimum 6 charachter">
                                <div class="help-block with-errors"></div>
                            </div>    
                            <div class="form-group">
                                <label for="pass2">Re-Password</label>
                                <input name="repassword" required type="password" class="form-control"  id="pass2" placeholder="Re-Password" data-match="#pass1" data-match-error="Password don't match">
                                <div class="help-block with-errors"></div>
                            </div>
							<div class="form-group">
                                <label for="country">Country</label>
                                <select onchange="getcities(this.value);" name="country" required class="form-control" id="country ">
									<option value="">--Select--</option>
									<?php if($countries)
									{
										foreach($countries as $c)
										{
											echo '<option value="'.$c->id.'">'.$c->name.'</option>';
										}
									} ?>
								</select>
                            </div>
							<div class="form-group">
                                <label for="cities">Cities</label>
                                <select  name="cities" required class="form-control" id="cities">
									<option value="">--Select--</option>
								</select>
                            </div>
							<div class="form-group">
                                <label for="gender">Gender</label>
                                <select  name="gender" required class="form-control" id="gender">
									<option value="">--Select--</option>
									<option value="1">Male</option>
									<option value="2">Female</option>
								</select>
                            </div>
							 <div class="form-group">
                                <label for="mobile">Mobile</label>
                                <input required name="mobile" type="text" class="form-control" id="mobile" placeholder="Mobile Number" data-error="Please enter Mobile Number">
                                <div class="help-block with-errors"></div>
                            </div>
							<div class="form-group">
                                <label for="picture">Profile Picture</label>
								<div>
								<button class="btn  btn-lg" type="button" onclick="$('#imageInput').click();" >Upload</button>
							   </div>
								<div>
								 <div class="progress-bar progress-bar-success progress-bar-striped" id="progressbar" role="progressbar"
								  aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:0%">
								  <span id="progressPercent" >0</span>
								  </div>
								 </div>
                            </div>
                            <div class="pull-left checkbox">
                                <label>
                                    <input required type="checkbox"> Accept terms & condition.
									<input  type="hidden"  id="profilepic" name="profilepic">
                                </label>

                            </div>
                            <div class="pull-right">
                                <button type="submit" class="btn btn-theme-dark btn-lg">Register</button>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div><!--register tab end-->
                </div>

            </div>
        </div>
    </div>
</div>
<form style="display:none;" action="<?php echo base_url();?>sports/uploadImage" method="post" enctype="multipart/form-data" id="MyUploadForm">
<input onchange="$('#MyUploadForm').submit();" name="image_file" id="imageInput" type="file" />
<input type="submit"  id="submit-btn" value="Upload" />

</form>
<div style="display:none;" id="output"></div>
<div class="divide80"></div>