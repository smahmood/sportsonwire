<div class="container">
    <!--<section class="me-intro parallax" data-stellar-background-ratio="0.5">
        <div class="container text-left">
            <h2 class="animated slideInLeft">Hello! myself assan</h2>
            <p class="lead animated slideInLeft ">I am a Graphics and Web designer </p>
            <p><a href="#" class="btn btn-lg btn-theme-bg">Hire Me</a></p>
        </div>
    </section>me intro parallax-->
    <div class="row">
		<div class="col-md-12">
			<?php if($info->team_cover != null): ?>
			<div class="cover-back" style="background: url('<?php echo base_url() . 'uploads/' . $info->team_cover; ?>') no-repeat;">
			<?php else: ?>
			<div class="cover-back" style="background: url('<?php echo base_url() . 'img/team-cover-default.jpg'; ?>') no-repeat;">
			<?php endif ?>
				<?php if($captain[0]['user_id']==$this->ion_auth->get_user_id())
						{ ?>
						<div id="cover-button" class="cover-img-button"><i class="fa fa-camera"></i></div>
						<?php } ?>	
				
				<div class="cover-photo" id="cover-header"></div>
			</div>
			<input type="hidden" class="cover_type" value="team" />
			<input type="hidden" class="cover_id" value="<?php echo $info->id ?>" />
		</div>
	</div>
	<div class="clear"></div>
	<div class="row">
		<div class="col-md-12">
			<div class="breadcrumb-wrap team-links">
			    <div class="navbar team-navbar">
		            <ul class="nav navbar-nav navbar-left team-navbar-nav">
						<li class="dropdown">
		                    <a href="<?php echo base_url() . 'footy/team/view/' . str_replace(' ', '-', $info->team_name).'/'.$info->id;?>"><?php echo $info->team_name;?></a>
		                </li>
		                <li class="dropdown">
		                    <a href="<?php echo base_url() . 'footy/team/view/' . str_replace(' ', '-', $info->team_name).'/'.$info->id.'/team-gallery'; ?>">Photos</a>
		                </li>
		                <li class="dropdown">
		                    <a href="<?php echo base_url() . 'footy/team/view/' . str_replace(' ', '-', $info->team_name).'/'.$info->id.'/team-members'; ?>">Squad</a>
		               
						</li>
						 <?php if($isplayer==1)
						{ ?>
						 <li class="dropdown">
						     <a href="<?php echo base_url() . 'footy/team/view/' . str_replace(' ', '-', $info->team_name).'/'.$info->id.'/Documents'; ?>">Team Documents</a>
		                </li>
						<?php } ?>
						<?php if($captain[0]['user_id']==$this->ion_auth->get_user_id())
						{ ?>
						<li class="dropdown">
						     <a href="<?php echo base_url() . 'footy/team/view/' . str_replace(' ', '-', $info->team_name).'/'.$info->id.'/Message'; ?>">Message Team Members</a>
		                </li>
						<?php } ?>
		            </ul>
					<?php if($isplayer==0)
						{ ?>
							
								<button class="btn boreder-theme pull-right" onclick="joinTeam(<?php echo $info->id ?>)" >Join Team</button>
							
						<?php }else{
							if($landingPlayer->status==-1)
							{
								echo '<p class="pull-right">Join Request Sent</p>';
							}
							else if($landingPlayer->status==0)
							{
								echo '<div  class="pull-right"><p style="color:red">Team Request Pending</p><button class="btn btn-alert pull-right" style="margin-left:10px" onclick="AcceptRejectTeam('.$info->id.',0)">Reject</button><button onclick="AcceptRejectTeam('.$info->id.',1)" class="btn btn-success pull-right">Accept</button></div>';
							}
						} ?>
		        </div>
			</div><!--breadcrumbs-->
		</div>
	</div>
    <div class="clear"></div>
    <div class="divide80"></div>
     <div class="row">
        <div class="center-heading">
            <h2><strong>Messaging</strong></h2>
            <span class="center-line"></span>
        </div>
		<center><p>Messaging will be available soon. Sorry for any inconvenience.</p></center>
    </div>
	
    
</div><!--gallery container-->