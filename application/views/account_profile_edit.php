        <div class="divide80"></div>
		<div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
				<form id="profileEdit" role="form" data-toggle="validator" action="<?php echo base_url(); ?>sports/account/edit" class="edit-form" method="POST">
                    <div class="panel panel-default clearfix">
                        <div class="panel-heading clearfix">
                            <h1 class="panel-title pull-left account-name">Account Profile</h1>
                            <div class="btn-group pull-right account-header-btn" role="group">
                                <a href="<?php echo base_url();?>sports/account/view" class="btn btn-default">
                                    <span><i class="fa fa-eye red"></i> View Account</span>
                                </a>
                                <a href="#" class="btn btn-default active">
                                    <span><i class="fa fa-pencil red"></i> Edit Account</span>
                                </a>
                            </div>
                        </div>
                        <div class="panel-body">
							
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3 account-info">
                                    <span class="h4"><i class="fa fa-cogs"></i> Sarib Mahmood</span>
                                    <hr/>
                                    <div class="form-group clearfix"><div class="col-md-4 account-info-title">First Name</div><div class="col-md-8"><input name="fname" type="text" class="form-control"  id="exampleInputname" placeholder="First Name" value="<?php echo $basic->first_name; ?>"></div></div>
                                    <div class="divide10"></div>
                                    <div class="form-group clearfix"><div class="col-md-4 account-info-title">Last Name</div><div class="col-md-8"><input type="text" class="form-control"  name="lname" id="exampleInputname" placeholder="Last Name" value="<?php echo $basic->last_name;?>"></div></div>
                                    <div class="divide10"></div>
                                    <div class="form-group clearfix"><div class="col-md-4 account-info-title">Email</div><div class="col-md-8"><input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email" readonly value="<?php echo $basic->email;?>" ></div></div>
                                    <div class="divide10"></div>
									<div class="form-group clearfix"><div class="col-md-4 account-info-title">Mobile</div><div class="col-md-8"><input type="text" class="form-control" readonly id="mobile" placeholder="Enter mobile" readonly value="<?php echo $basic->phone;?>" ></div></div>
                                    <div class="divide10"></div>
									<div class="form-group clearfix"><div class="col-md-4 account-info-title">Country</div><div class="col-md-8">
									<select onchange="getcities(this.value);" name="country" required class="form-control" id="country ">
									<option value="">--Select--</option>
									<?php if($countries)
									{
										foreach($countries as $c)
										{
											if($c->id==$basic->country_id)
											{
												echo '<option selected value="'.$c->id.'">'.$c->name.'</option>';
											}
											else
											{
												echo '<option value="'.$c->id.'">'.$c->name.'</option>';
											}
										}
									} ?>
								</select>									</div></div>
                                    <div class="divide10"></div>
                                    <div class="form-group clearfix"><div class="col-md-4 account-info-title">City</div><div class="col-md-8"> <select  name="cities" required class="form-control" id="cities">
										<option value="">--Select--</option>
										<?php if($cities)
										{
											foreach($cities as $c)
											{
												if($c->id==$basic->city_id)
												{
													echo '<option selected value="'.$c->id.'">'.$c->name.'</option>';
												}
												else
												{
													echo '<option  value="'.$c->id.'">'.$c->name.'</option>';
												}
											}
										} ?>
									</select></div></div>
                                    <div class="divide10"></div>
									
									<input  type="hidden" value="<?php echo $basic->profile_picture;?>" id="profilepic" name="profilepic">
                                    <div class="form-group clearfix"><div class="col-md-4 account-info-title">Gender</div><div class="col-md-8"><select name="gender" class="form-control"><option <?php if($basic->gender==1){ echo 'selected'; } ?> value="1">Male</option><option <?php if($basic->gender==2){ echo 'selected'; } ?> value="2">Female</option></select></div>
                                    <div class="divide10"></div></div>
                                </div>
								<div class="col-md-3">
									<img class="profile_pic_logged_in_user" onclick="$('#imageInput').click();" src="<?php echo base_url();?>uploads/thumb_<?php if($basic->profile_picture == ''){echo 'default.jpg';}else{echo $basic->profile_picture;}?>" style="width:100%;cursor:pointer;"/>
									<div>
									 <div class="progress-bar progress-bar-success progress-bar-striped" id="progressbar" role="progressbar"
									  aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:0%">
									  <span id="progressPercent" >0</span>
									  </div>
									 </div>
								</div>
                            </div>
							
                        </div>
                        <div class="panel-footer clearfix">
                            <input class="btn btn-primary pull-right" name="commit" type="submit" value="Save Info">
                            <a href="<?php echo base_url();?>sports/account/view" class="btn btn-default pull-left">Cancel</a>
                        </div>
                    </div>
					</form>
                </div>
            </div>
        </div>
        <div class="divide80"></div>
		<form style="display:none;" action="<?php echo base_url();?>sports/uploadImage" method="post" enctype="multipart/form-data" id="MyUploadForm">
		<input onchange="$('#MyUploadForm').submit();" name="image_file" id="imageInput" type="file" />
		<input type="submit"  id="submit-btn" value="Upload" />

		</form>
		<div style="display:none;" id="output"></div>