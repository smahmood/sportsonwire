<div class="container">
    <div class="divide80"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="center-heading">
                <h2>Activate Your Account</h2>
                <span class="center-line"></span>
            </div><!--center-heading-->
        </div>
    </div><!--row-->
    <div class="row row-center">
        <p>Please Activate your account by entering the code that have been sent to your mobile number in the the below textbox and submit</p>
        <?php echo form_open('Sports/verify_account'); ?>
            <input type="text" placeholder=" Enter Code Here" name="code"/>
            <input type="submit" value="Activate Account"/>
        </form>
        <br>
        <?php if ($basic->msgs_count < 3): ?>
        <p>Message delivery can take upto 5 minutes of time. If you still did not recieve your messsage, <button onclick="SendMessage();">Click here</button> to send code again (Maximum 3 Code Re-sends per day allowed | <?php echo 3-$basic->msgs_count; ?> Re-sends remaining for today)</p>
        <?php else: ?>
        <p>You have used all 3 of your daily Re-sends allowed for today. Please come back tomorrow for Re-sending of the Code.</p>
        <?php endif; ?>
        <p>If you want, You can change your mobile number by going to <b>Edit Account</b> section from the top bar drop down menu. Please enter complete number with country code for proper message delivery</p>
    </div>
</div>
<div class="divide80"></div>