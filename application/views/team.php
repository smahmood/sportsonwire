<div class="container">
	<!--<section class="me-intro parallax" data-stellar-background-ratio="0.5">
	    <div class="container text-left">
	        <h2 class="animated slideInLeft">Hello! myself assan</h2>
	        <p class="lead animated slideInLeft ">I am a Graphics and Web designer </p>
	        <p><a href="#" class="btn btn-lg btn-theme-bg">Hire Me</a></p>
	    </div>
	</section>me intro parallax-->
	<div class="row">
		<div class="col-md-12">
			<?php if($info->team_cover != null): ?>
			<div class="cover-back" style="background: url('<?php echo base_url() . 'uploads/' . $info->team_cover; ?>') no-repeat;">
			<?php else: ?>
			<div class="cover-back" style="background: url('<?php echo base_url() . 'img/team-cover-default.jpg'; ?>') no-repeat;">
			<?php endif ?>
				<?php if($captain[0]['user_id']==$this->ion_auth->get_user_id())
						{ ?>
						<div id="cover-button" class="cover-img-button"><i class="fa fa-camera"></i></div>
						<?php } ?>	
				
				<div class="cover-photo" id="cover-header"></div>
			</div>
			<input type="hidden" class="cover_type" value="team" />
			<input type="hidden" class="cover_id" value="<?php echo $info->id ?>" />
		</div>
	</div>
	<div class="clear"></div>
	<div class="row">
		<div class="col-md-12">
			<div class="breadcrumb-wrap team-links">
			    <div class="navbar team-navbar">
		            <ul class="nav navbar-nav navbar-left team-navbar-nav">
						<li class="dropdown">
		                    <a href="#"><?php echo $info->team_name;?></a>
		                </li>
		                <li class="dropdown">
		                    <a href="<?php echo base_url() . 'footy/team/view/' . str_replace(' ', '-', $info->team_name).'/'.$info->id.'/team-gallery'; ?>">Photos</a>
		                </li>
		                <li class="dropdown">
		                    <a href="<?php echo base_url() . 'footy/team/view/' . str_replace(' ', '-', $info->team_name).'/'.$info->id.'/team-members'; ?>">Squad</a>
		               
						</li>
						 <?php if($isplayer==1)
						{ ?>
						 <li class="dropdown">
						     <a href="<?php echo base_url() . 'footy/team/view/' . str_replace(' ', '-', $info->team_name).'/'.$info->id.'/Documents'; ?>">Team Documents</a>
		                </li>
						<?php } ?>
						<?php if($captain[0]['user_id']==$this->ion_auth->get_user_id())
						{ ?>
						<li class="dropdown">
						     <a href="<?php echo base_url() . 'footy/team/view/' . str_replace(' ', '-', $info->team_name).'/'.$info->id.'/Message'; ?>">Message Team Members</a>
		                </li>
						<?php } ?>
		            </ul>
					<?php if($captain[0]['user_id']==$this->ion_auth->get_user_id())
						{ ?>
						<button href="#add-member-modal" data-toggle="modal" class="btn border-theme pull-right"  >Add Members</button>
						<?php } ?>
					<?php if($isplayer==0)
						{ ?>
							
								<button class="btn border-theme pull-right" onclick="joinTeam(<?php echo $info->id ?>)" >Join Team</button>
							
						<?php }else{
							if($landingPlayer->status==-1)
							{
								echo '<p class="pull-right">Join Request Sent</p>';
							}
							else if($landingPlayer->status==0)
							{
								echo '<div  class="pull-right"><p style="color:red">Team Request Pending</p><button class="btn border-theme pull-right" style="margin-left:10px" onclick="AcceptRejectTeam('.$info->id.',0)">Reject</button><button onclick="AcceptRejectTeam('.$info->id.',1)" class="btn btn-success pull-right">Accept</button></div>';
							}
							else {
								if($captain[0]['user_id']!=$this->ion_auth->get_user_id())
								{
									echo '<div  class="pull-right"><button class="btn border-theme pull-right" style="margin-left:10px" onclick="AcceptRejectTeam('.$info->id.',0)">Leave Team</button></div>';
								}
							}
						} ?>
		        </div>
			</div><!--breadcrumbs-->
		</div>
	</div>
	<div class="clear"></div>
	<div class="divide80"></div>
	<div class="row">
		<div class="col-sm-4 margin30">
            <div class="latest-new">
                <img src="<?php echo base_url(); ?>img/empty-team-photo.jpg" class="img-responsive" alt="" style="width:100%;">
                <div class="blog-date">
                	<div id="crest-button" class="crest-img-button"><i class="fa fa-pencil"></i></div>
                	<div class="crest-holder" id="crest-header"><img src="<?php echo base_url() . 'uploads/' . $info->team_logo; ?>" alt=""></div>
                </div>
                <div class="l-news-desc account-info clearfix">
                    <h3><a href="#"><?php echo $info->team_name;?></a></h3>
					<p style="text-align:center;"><?php if($info->bio!=""){ echo substr($info->bio, 0, 70)."...";; } ?></p>
				<?php if($info->bio!=""){ ?>	<button class="btn border-theme pull-right ">Read More</button> <?php } ?>
                </div>
				
                <div class="panel-footer clearfix">
                    <div class="pull-left clearfix" >
						<a href="#"><img  style="width:40px;" class="empty-member-pic pull-left" src="<?php if($captain[0]['profile_picture']==""){ $captain[0]['profile_picture']="default.jpg" ; } echo base_url().'uploads/'.$captain[0]['profile_picture'];?>" alt=""></a>
					</div>
                    <div class="pull-left clearfix">
						<a href="<?php echo base_url();?>footy/playerProfile/<?php echo $captain[0]['user_id']?>"><?php echo $captain[0]['first_name'] . " " . $captain[0]['last_name']; ?></a><br/>
						<span style="color:#989898;">Captain</span><br>
						<span style="color:#989898;">Contact : <?php echo $captain[0]['phone'] ;?></span>
					</div>
                </div>
            </div><!--latest news-->
            <div class="divide40"></div>
            <div class="box box-danger">
	            <div class="box-header with-border">
	              <h3 class="box-title">Latest Members</h3>
	              <div class="box-tools pull-right">
	              </div>
	            </div><!-- /.box-header -->
	            <div class="box-body no-padding">
	              <ul class="users-list clearfix">
	                <?php if($members){ foreach($members as $m){ if($m['status']==1){
						if($m['profile_picture']==""){ $m['profile_picture']="default.jpg" ; }
							echo '<li>
							  <img src="'.base_url().'uploads/'.$m['profile_picture'].'" alt="User Image"/>
							  <a class="users-list-name" href="'.base_url().'footy/playerProfile/'.$m['user_id'].'">'.$m['first_name'].' '.$m['last_name'].'</a>
							  
							</li>';
							//<span class="users-list-date">Today</span>
						} }
					} ?>
	                
	              </ul><!-- /.users-list -->
	            </div><!-- /.box-body -->
	            <div class="box-footer text-center">
	              <a href="<?php echo base_url() . 'footy/team/view/' . str_replace(' ', '-', $info->team_name).'/'.$info->id.'/team-members'; ?>" class="uppercase">View All Users</a>
	            </div><!-- /.box-footer -->
	          </div><!--/.box -->
        </div><!--latest news col-->
        <div class="col-md-8">
			<div class="center-heading ">
				<button class="btn border-theme" onclick="$('#postImageToWall').hide();$('#postToWall').show();" > POST TO WALL </button>
				<button class="btn border-theme" onclick="$('#postToWall').hide();$('#postImageToWall').show();"  > UPLOAD IMAGE TO WALL </button>
			</div>	
			<div id="postToWall" class="clearfix" style="display:none">
				<form action="#" method="POST" id="postToWallForm" onsubmit="return postToWall()">
					<input maxlength="1000" type="text" required=""  placeholder="What's on your mind"  name="post" class="form-control">
					<input type="hidden" name="team" value="<?php echo $info->id;?>"/>
					<input class="btn border-theme pull-right " type="submit" value="POST" />
				</form>	
			</div>
			<div id="postImageToWall" class="clearfix" style="display:none">
				<form action="#" id="postImageToWallForm" onsubmit="return postImageToWall()" method="POST" onsubmit="return false">
					<input type="text"   placeholder="What's on your mind"  name="post" class="form-control">
					<input type="hidden" name="team" value="<?php echo $info->id;?>"/>
					<input type="hidden" name="pic" value="" id="wallpic"/>
					
					<input class="btn border-theme pull-right " type="submit" value="POST" />
					<input class="btn border-theme pull-right " onclick="$('#imageInput').click();" type="button" value="Upload" />
					<img style="display:none;"  src="" class="wallpicdisp pull-right" />
					<div style="width:70%;"><div  class="progress-bar progress-bar-success progress-bar-striped" id="progressbar" role="progressbar"
					aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:0%">
					<span id="progressPercent" >0</span>
					</div></div>
					
				</form>	
			</div>
        	<div class="center-heading ">
                <h2>Recent <strong>Activity</strong></h2>
                <span class="center-line"></span>
            </div>
			<?php if($feed)
			{ ?>
			<ul class="timeline">
				<!-- timeline time label date("j M Y", $ts)-->
				<?php $date='';$count=0; foreach($feed as $f)
				{
					$count=$count+1;
					$tdate=explode(" ",$f->time_stamp);
					$tdate=$tdate[0];
					if($date=='')
					{ $date=$tdate; ?>
						<li class="time-label">
							<span class="bg-red"><?php $time = strtotime($date);echo date("j M Y", $time);?> </span>
						</li>
					<?php }
					else if($date!=$tdate)
					{ $date=$tdate; ?>
						<li class="time-label">
							<span <?php if($count%2==1){ ?>class="bg-red" <?php } else{ ?>class="bg-green" <?php } ?> ><?php $time = strtotime($date);echo date("j M Y", $time);?> </span>
						</li>
					<?php } 
					if($f->post_type==0)
					{ ?>
						<li>
							<i class="fa fa-envelope bg-blue"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i> 12:05</span>
								<h3 class="timeline-header"><a href="#">Support Team</a> sent you an email</h3>
								<div class="timeline-body">
									Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
									weebly ning heekya handango imeem plugg dopplr jibjab, movity
									jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
									quora plaxo ideeli hulu weebly balihoo...
								</div>
								<div class='timeline-footer'>
									<a class="btn btn-primary btn-xs">Read more</a>
									<a class="btn btn-danger btn-xs">Delete</a>
								</div>
							</div>
						</li>
						<!-- END timeline item -->
						<!-- timeline item -->
						<li>
							<i class="fa fa-user bg-aqua"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i> 5 mins ago</span>
								<h3 class="timeline-header no-border"><a href="#">Sarah Young</a> accepted your friend request</h3>
							</div>
						</li>
						<!-- END timeline item -->
						<!-- timeline item -->
						<li>
							<i class="fa fa-comments bg-yellow"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i> 27 mins ago</span>
								<h3 class="timeline-header"><a href="#">Jay White</a> commented on your post</h3>
								<div class="timeline-body">
									Take me to your leader!
									Switzerland is small and neutral!
									We are more like Germany, ambitious and misunderstood!
								</div>
								<div class='timeline-footer'>
									<a class="btn btn-warning btn-flat btn-xs">View comment</a>
								</div>
							</div>
						</li>
						<!-- END timeline item -->
						<!-- timeline time label -->
						<li class="time-label">
							<span class="bg-green">3 Jan. 2014</span>
						</li>
						<!-- /.timeline-label -->
						<!-- timeline item -->
						<li>
							<i class="fa fa-camera bg-purple"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i> 2 days ago</span>
								<h3 class="timeline-header"><a href="#">Mina Lee</a> uploaded new photos</h3>
								<div class="timeline-body">
									<img src="http://placehold.it/150x100" alt="..." class='margin' />
									<img src="http://placehold.it/150x100" alt="..." class='margin'/>
									<img src="http://placehold.it/150x100" alt="..." class='margin'/>
									<img src="http://placehold.it/150x100" alt="..." class='margin'/>
								</div>
							</div>	
						</li>
						<!-- END timeline item -->
						<!-- timeline item -->
						<li>
							<i class="fa fa-video-camera bg-maroon"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i> 5 days ago</span>
								<h3 class="timeline-header"><a href="#">Mr. Doe</a> shared a video</h3>
								<div class="timeline-body">
									<div class="embed-responsive embed-responsive-16by9">
										<iframe src="https://player.vimeo.com/video/124168998" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
									</div>
								</div>
								<div class="timeline-footer">
									<a href="#" class="btn btn-xs bg-maroon">See comments</a>
								</div>
							</div>
						</li>
						<!-- END timeline item -->
						<li>
							<i class="fa fa-clock-o bg-gray"></i>
						</li>
					<?php }
					if($f->post_type==2)
					{ ?>
						<li>
							<i class="fa fa-user bg-aqua"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i> <?php $temp=explode(" ",$f->time_stamp); $temp=explode(":",$temp[1]); echo $temp[0].":".$temp[1]; ?> </span>
								<h3 class="timeline-header no-border"><a href="#"><b><?php echo $f->user->first_name." ".$f->user->last_name; ?></b></a> has been added to the team.</h3>
							</div>
						</li>
					<?php }
					if($f->post_type==3)
					{ ?>
						<li>
							<i class="fa fa-camera bg-purple"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i>  <?php $temp=explode(" ",$f->time_stamp); $temp=explode(":",$temp[1]); echo $temp[0].":".$temp[1]; ?> </span>
								<h3 class="timeline-header"><a href="#"><?php echo $info->team_name; ?></a> upated thier cover photo</h3>
								<div class="timeline-body">
									<img style="width:96%;"src="<?php echo base_url();?>uploads/<?php echo $f->cover->path;?>" alt="..." class='margin' />
								</div>
							</div>	
						</li>
					<?php }
					if($f->post_type==5)
					{ if($f->wall->post_type==1){ ?>
						<li>
							<i class="fa fa-comments bg-yellow"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i> <?php $temp=explode(" ",$f->time_stamp); $temp=explode(":",$temp[1]); echo $temp[0].":".$temp[1]; ?> </span>
								<h3 class="timeline-header"><a href="<?php echo base_url();?>footy/playerProfile/<?php echo $f->user->id;?>"><?php echo $f->user->first_name." ".$f->user->last_name; ?></a> posted on your wall</h3>
								<div class="timeline-body">
									<?php echo $f->wall->text; ?>
								</div>
							</div>
						</li>
					<?php }else{
					?>
						<li>
							<i class="fa fa-comments bg-yellow"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i> <?php $temp=explode(" ",$f->time_stamp); $temp=explode(":",$temp[1]); echo $temp[0].":".$temp[1]; ?> </span>
								<h3 class="timeline-header"><a href="<?php echo base_url();?>footy/playerProfile/<?php echo $f->user->id;?>"><?php echo $f->user->first_name." ".$f->user->last_name; ?></a> posted on your wall</h3>
								<div class="timeline-body">
									<b style="margin-botton:8px;"><?php echo $f->wall->text; ?></b>
									<img src="<?php echo base_url();?>uploads/<?php echo $f->wall->image_path; ?>" style="width:96%"/>
								</div>
							</div>
						</li>
					<?php 
					
					}	}
				}
				?>
				
				<!-- /.timeline-label -->
				<!-- timeline item -->
				<li>
							<i class="fa fa-clock-o bg-gray"></i>
						</li>
				<div class="divide40"></div>
			</ul>
			<?php } else
			{
				echo '<center><p>There is no recent activity on this page.</p></center>';
			}  ?>
        </div>
	</div>
	<div class="clear"></div>
	<div class="divide80"></div>
</div>
<div class="modal fade in" id="add-member-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?php echo base_url();?>footy/team/create" id="addmemberform" method="POST" role="form" onsubmit="return inviteEmail()" data-toggle="validator">
                <div class="modal-header text-center">
                    <h3 style="margin-top:33px;"><i class="fa fa-plus-circle"></i> Add Team Member</h3>
                </div>
                <div class="modal-body clearfix">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Invite by Email</label>
                                    <input type="email" class="form-control" name="email" data-minlength="3" placeholder="Enter Email" data-error="Please Enter Valid Email " required>
									<input type="hidden" name="team" value="<?php echo $info->id; ?>">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-sm-12" style="padding:20px 0;">
						<input type="submit" class="btn border-theme pull-right" value="Invite">
                        <a href="#" class="btn btn-default pull-left">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<form style="display:none;" action="<?php echo base_url();?>sports/uploadImage" method="post" enctype="multipart/form-data" id="MyUploadForm">
		<input onchange="$('#MyUploadForm').submit();" name="image_file" id="imageInput" type="file" />
		<input type="submit"  id="submit-btn" value="Upload" />

		</form>
		<div style="display:none;" id="output"></div>
