<div class="container">
    <div class="divide80"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="center-heading">
                <h2>Get in the Game</h2>
                <span class="center-line"></span>
                <!-- <p class="lead">Are you a cricket lover or football?</p>-->
            </div><!--center-heading-->
        </div>
    </div><!--row-->
    <div class="row row-center">
        <div class="col-md-6 margin20 hvr-float-shadow">
            <div class="team-wrap no-team">
                <a href="<?php echo base_url();?>cric"><img src="img/simple-cricket.png" class="img-responsive landing-type-img" alt=""></a>
                <div class="divide20"></div>
                <a href="<?php echo base_url();?>cric"><img src="img/cricket-text.png" class="img-responsive" alt="" style="margin:0 auto;"></a>
                <!-- <p>
                    Create/Join a cricket club. Manage the club. And also organize tournaments.
                </p>-->
            </div><!--team-wrap-->
        </div><!--team col-->
        <div class="col-md-6 margin20 hvr-float-shadow">
            <div class="team-wrap no-team">
                <a href="<?php echo base_url();?>footy"><img src="img/simple-football.png" class="img-responsive landing-type-img" alt=""></a>
                <div class="divide20"></div>
                <a href="<?php echo base_url();?>footy"><img src="img/football-text.png" class="img-responsive" alt="" style="margin:0 auto;"></a>
                <!-- <p>
                    Create/Join a football club. Manage the club. And also organize tournaments.
                </p>-->
            </div><!--team-wrap-->
        </div><!--team col-->
    </div>
</div>
<div class="divide80"></div>