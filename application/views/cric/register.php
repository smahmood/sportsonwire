<div class="divide80"></div>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form action="<?php echo base_url();?>cric/register" method="POST" role="form" data-validator="validator">
                <div class="panel panel-default clearfix">
                    <div class="panel-heading clearfix">
                        <h1 class="panel-title pull-left account-name">Create Cricket Profile</h1>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3 account-info">
                                <span class="h4"><i class="fa fa-cogs"></i> <?php echo $basic->first_name. " " . $basic->last_name; ?></span>
                                <hr/>
                                <div class="form-group clearfix">
                                    <label for="role_id">Playing Role</label>
                                    <select class="form-control" name="role" required>
                                        <option value="" disabled selected>Select Role</option>
                                        <option value="1" >Bowling</option>
                                        <option value="2" >Batting</option>
                                        <option value="3" >AllRounder</option>
                                        <option value="4" >Wicket Keepr</option>
                                    </select>
                                </div>
                                <div class="divide10"></div>
                                <div class="form-group clearfix">
                                    <label for="batting_id">Batting Style</label>
                                    <select class="form-control" name="batting" required>
                                        <option value="" disabled selected>Select Batting Style</option>
                                        <option value="1" >Left</option>
                                        <option value="2" >Right</option>
                                    </select>
                                </div>
                                <div class="divide10"></div>
                                <div class="form-group clearfix">
                                    <label for="bowling_id">Bowling Style</label>
                                    <select id="bowling_id" class="form-control" name="bowling" required>
                                        <option value="" disabled selected>Select Bowling Style</option>
                                        <option value="1" >Fast</option>
                                        <option value="2" >Medium</option>
                                        <option value="3" >Leg Spin</option>
                                        <option value="4" >Off Spin</option>
                                    </select>
                                </div>
                                <div class="divide10"></div>
                                <div class="form-group clearfix">
                                    <label for="bowling_arm_id">Bowling Arm</label>
                                    <select class="form-control" name="bowling_arm" required>
                                        <option value="" disabled selected>Select Bowling Arm</option>
                                        <option value="1" >Left</option>
                                        <option value="2" >Right</option>
                                    </select>
                                </div>
                                <div class="divide10"></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer clearfix">
                        <input class="btn btn-primary pull-right" name="commit" type="submit" value="Create">
                        <a href="<?php echo base_url(); ?>" class="btn btn-default pull-left">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="divide80"></div>