<div class="divide80"></div>
<div class="container">
    <div class="row">
        <div class="col-sm-3 ">
            <div class="sidebar-box margin40">
                <div class="panel panel-default clearfix" style="text-align:center;">
                    <div class="panel-heading">
                        <h4 class="panel-title">UPCOMING EVENTS (0)</h4>
                    </div>
                    <div class="panel-body">
                        <span class="text">No upcoming events</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-9">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="heading pull-left">Your Teams</h3>
                    <a href="#new-team-modal" data-toggle="modal" class="btn border-theme pull-right"><i class="fa fa-plus-circle"></i> Create new Team</a>
                </div>
            </div>
            <div class="row">
                <?php foreach($my_teams as $single_team){ ?>
                    <?php if(!empty($single_team)){ ?>
                    <div class="col-sm-4 margin30">
                        <div class="latest-new">
                            <img src="<?php echo base_url(); ?>img/empty-team-photo.jpg" class="img-responsive" alt="">
                            <div class="blog-date">
                                <img src="<?php echo base_url();?>img/crest-soccer.png" class="img-responsive" alt=""> 
                            </div>
                            <div class="l-news-desc account-info">
                                <h3><a href="#"><?php echo $single_team['team_name']; ?></a></h3>
                            </div>
                            <div class="panel-footer clearfix">
                                <a href="#"><img  class="empty-member-pic pull-left" src="<?php echo base_url();?>img/empty-profile-40x40.jpg" alt=""></a>
                                <a href="#"><?php echo $basic->first_name . " " . $basic->last_name; ?></a><br/>
                                <?php if($single_team['role'] == 1){ ?>
                                    <span style="color:#989898;">Manager</span>
                                <?php }else if($single_team['role'] == 2){ ?>
                                    <span style="color:#989898;">Captain</span>
                                <?php }else{ ?>
                                    <span style="color:#989898;">Player</span>
                                <?php } ?>
                            </div>
                        </div><!--latest news-->
                    </div><!--latest news col-->
                    <?php } ?>
                <?php } ?>
                <div class="col-sm-4 margin30">
                    <div class="panel team-list empty">
                        <a href="#new-team-modal" data-toggle="modal" class="btn border-theme"><i class="fa fa-plus-circle"></i> Create new Team</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="clear"></div>
<div class="divide80"></div>
<!-- Modal -->
<div class="modal fade in" id="new-team-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?php echo base_url();?>cric/team/create" id="new_team_form" method="POST" role="form" data-toggle="validator">
                <div class="modal-header text-center">
                    <h3 style="margin-top:33px;"><i class="fa fa-plus-circle"></i> Create New Team</h3>
                </div>
                <div class="modal-body clearfix">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Your Team Name</label>
                                    <input type="text" class="form-control" name="team_name" data-minlength="3" placeholder="Enter Team Name" data-error="Team name required" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Gender</label>
                                    <select class="form-control" name="gender" data-error="Please specify gender" required>
                                        <option value="" disabled selected>Gender</option>
                                        <option value="1">Male</option>
                                        <option value="2">Female</option>
                                        <option value="3">Coed</option>
                                    </select>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Age group</label>
                                    <select class="form-control" name="age_group" data-error="Please specify age group" required>
                                        <option value="" disabled selected>Age Group</option>
                                        <option value="1">Under 13</option>
                                        <option value="2">13-18</option>
                                        <option value="3">18+</option>
                                    </select>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Country</label>
                                    <select onchange="getcities(this.value);" class="form-control" name="country" id="country" data-error="Please select country" required>
                                        <option value="" disabled selected>--Select Country--</option>
                                        <?php if($countries){
                                            foreach($countries as $c){
                                                echo '<option value="'.$c->id.'">'.$c->name.'</option>';
                                            }
                                        } ?>
                                    </select>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-sm-6 margin30">
                                <div class="form-group">
                                    <label>City</label>
                                    <select class="form-control" name="city" id="cities" data-error="Please select city" required>
                                        <option value="" disabled selected>--Select City--</option>
                                        <?php if($cities){
                                            foreach($cities as $c){
                                                echo '<option  value="'.$c->id.'">'.$c->name.'</option>';
                                            }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <hr class="clear"/>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="col-sm-8">Do your team have a ground of their own?</div>
                                <div class="col-sm-4">
                                    <select name="team_ground" id="team_ground" class="form-control" data-error="Required" required>
                                        <option value="" disabled selected>Select One</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <hr class="clear ground_options"/>
                        <div class="col-sm-12 ground_options">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Ground Name (Optional)</label>
                                    <input type="text" class="form-control" name="ground_name" data-minlength="3" placeholder="Ground Name">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 ground_options">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Ground Size?</label>
                                    <select class="form-control" name="ground_size" data-error="Please specify ground size" required>
                                        <option value="" disabled selected>Select Ground Size</option>
                                        <option value="5">5-aside</option>
                                        <option value="7">7-aside</option>
                                        <option value="8">8-aside</option>
                                        <option value="9">9-aside</option>
                                        <option value="10">10-aside</option>
                                        <option value="11">11-aside</option>
                                    </select>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-sm-12" style="padding:20px 0;">
                        <input type="submit" class="btn border-theme pull-right" value="Create Team">
                        <a href="#" class="btn btn-default pull-left">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>