<section class="purchase-sec wow animated fadeInUp animated" style="visibility: visible;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="col-md-4 margin20"><span>Wanna be a part of Team?</span></div>
                <div class="col-md-6 margin20">
                    <div class="demo">
                        <form role="form" action="<?php echo base_url().'sports/getTeams'; ?>" method="get" class="subscribe-form">
                            <input type="hidden" name="mode" value="users">
                            <input type="hidden" id="page_type" name="page_type" value="cric">
                            <div class="Typeahead--twitterUsers">
                                <div class="u-posRelative">
                                    <input class="Typeahead-hint" type="text" tabindex="-1" readonly>
                                    <input class="Typeahead-input form-control" id="demo-input" type="text" name="q" placeholder="Enter Team Name...">
                                    <img class="Typeahead-spinner" src="img/spinner.gif">
                                </div>
                                <div class="Typeahead-menu"></div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-2 margin20">
                    <a href="<?php echo base_url() . $sports_page;?>/dashboard" class="btn btn-theme-dark btn-lg"><i class="fa fa-plus-circle"></i> Create Team</a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="portfolio">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="center-heading">
                    <h2>Upcoming Tournaments</h2>
                    <span class="center-line"></span>
                </div>
            </div>                   
        </div><!--center heading row-->
        <div class="row">
            <div class="col-md-12">
                <div id="work-carousel" class="owl-carousel owl-spaced">
                    <div>

                        <div class="item-img-wrap ">
                            <img src="<?php echo base_url(); ?>img/img-1.jpg" class="img-responsive" alt="workimg">
                            <div class="item-img-overlay">
                                <a href="img/img-1.jpg" class="show-image">
                                    <span></span>
                                </a>
                            </div>
                        </div> <!--item img wrap-->
                        <div class="work-desc">
                            <h3><a href="portfolio-single.html">FAST Gala</a></h3>
                            <span>Lahore, Pakistan</span>
                        </div><!--work desc-->
                    </div><!--owl item-->
                    <div> 
                        <div class="item-img-wrap ">
                            <img src="<?php echo base_url(); ?>img/img-2.jpg" class="img-responsive" alt="workimg">
                            <div class="item-img-overlay">
                                <a href="img/img-2.jpg" class="show-image">
                                    <span></span>
                                </a>
                            </div>
                        </div> 
                        <div class="work-desc">
                            <h3><a href="portfolio-single.html">LUMS Nationals</a></h3>
                            <span>Lahore, Pakistan</span>
                        </div><!--work desc-->
                    </div><!--owl item-->
                    <div> 
                        <div class="item-img-wrap ">
                            <img src="<?php echo base_url(); ?>img/img-3.jpg" class="img-responsive" alt="workimg">
                            <div class="item-img-overlay">
                                <a href="img/img-3.jpg" class="show-image">
                                    <span></span>
                                </a>
                            </div>
                        </div> 
                        <div class="work-desc">
                            <h3><a href="portfolio-single.html">Play-On Tournament</a></h3>
                            <span>Lahore, Pakistan</span>
                        </div><!--work desc-->
                    </div><!--owl item-->
                    <div> 
                        <div class="item-img-wrap ">
                            <img src="<?php echo base_url(); ?>img/img-4.jpg" class="img-responsive" alt="workimg">
                            <div class="item-img-overlay">
                                <a href="img/img-4.jpg" class="show-image">
                                    <span></span>
                                </a>
                            </div>
                        </div> 
                        <div class="work-desc">
                            <h3><a href="portfolio-single.html">Bestival - BNU</a></h3>
                            <span>BNU Islamabad, Pakistan</span>
                        </div><!--work desc-->
                    </div><!--owl item-->
                    <div> 
                        <div class="item-img-wrap ">
                            <img src="<?php echo base_url(); ?>img/img-5.jpg" class="img-responsive" alt="workimg">
                            <div class="item-img-overlay">
                                <a href="img/img-5.jpg" class="show-image">
                                    <span></span>
                                </a>
                            </div>
                        </div> 
                        <div class="work-desc">
                            <h3><a href="portfolio-single.html">PFL - Football Tournament</a></h3>
                            <span>Karachi, Pakistan</span>
                        </div><!--work desc-->
                    </div><!--owl item-->
                    <div> 
                        <div class="item-img-wrap ">
                            <img src="<?php echo base_url(); ?>img/img-6.jpg" class="img-responsive" alt="workimg">
                            <div class="item-img-overlay">
                                <a href="img/img-6.jpg" class="show-image">
                                    <span></span>
                                </a>
                            </div>
                        </div> 
                        <div class="work-desc">
                            <h3><a href="portfolio-single.html">Barclays Football Tournament</a></h3>
                            <span>Lahore, Pakistan</span>
                        </div><!--work desc-->
                    </div><!--owl item-->
                </div>
            </div>
        </div><!--row-->
        <div class="divide30"></div>
        <div class="row">
            <div class="col-md-12 text-center">
                <a href="#" class="btn border-theme btn-lg wow animated fadeInUp"><i class="fa fa-bars"></i> Load More Tournaments</a>
            </div>
        </div>
    </div><!--container-->
</section><!--recent work section -->
<div class="clear"></div>
<div class="divide80"></div>
<div class="container">
	<div class="row">
        <div class="col-sm-12">
            <div class="center-heading">
                <h2>Featured Section</h2>
                <span class="center-line"></span>
            </div><!--center-heading-->
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3 ">
            <div class="sidebar-box margin40">
                <div class="panel panel-default clearfix" style="text-align:center;">
            		<div class="panel-heading">
            			<h4 class="panel-title">UPCOMING EVENTS (0)</h4>
            		</div>
            		<div class="panel-body">
            			<span class="text">No upcoming events</span>
            		</div>
            	</div>
            </div>
        </div>

		<div class="col-sm-9">
            <div class="row">
                <div class="col-sm-6 margin30">
                    <div class="latest-new">
                    	<h4 class="panel-heading" style="margin-bottom:0;text-align:center;">Player</h4>
                        <img src="<?php echo base_url(); ?>img/img-6.jpg" class="img-responsive" alt="">
                        <div class="blog-date">
                            <img src="<?php echo base_url();?>img/crest-soccer.png" class="img-responsive" alt=""> 
                        </div>
                        <div class="l-news-desc account-info">
                            <h3><a href="#">Omar Farooq</a></h3>
                            <div class="col-md-10 col-md-offset-1"><div class="col-md-6 account-info-title">Position</div><div class="col-md-6">Defender</div><div class="clear divide10"></div></div>
                            <div class="col-md-10 col-md-offset-1"><div class="col-md-6 account-info-title">Location</div><div class="col-md-6">Lahore, Pakistan</div><div class="clear divide10"></div></div>
                            <div class="col-md-10 col-md-offset-1"><div class="col-md-6 account-info-title">Preferred Foot</div><div class="col-md-6">Right</div><div class="clear divide10"></div></div>
                            <div class="clear divide20"></div>
                        </div>
                        <div class="panel-footer clearfix">
                        	<a href="#" class="btn border-theme pull-right">See Player Profile</a>
                        </div>
                    </div><!--latest news-->
                </div><!--latest news col-->
                <div class="col-sm-6 margin30">
                    <div class="latest-new">
                    	<h4 class="panel-heading" style="margin-bottom:0;text-align:center;">Team</h4>
                        <img src="<?php echo base_url(); ?>img/img-5.jpg" class="img-responsive" alt="">
                        <div class="blog-date">
                            <img src="<?php echo base_url();?>img/crest-soccer.png" class="img-responsive" alt=""> 
                        </div>
                        <div class="l-news-desc account-info">
                            <h3><a href="#">Red Devils</a></h3>
                            <div class="col-md-6 col-md-offset-3"><div class="col-md-6 account-info-title">City</div><div class="col-md-6">Lahore</div><div class="clear divide10"></div></div>
                            <div class="clear divide20"></div>
                        </div>
                        <div class="panel-footer clearfix">
                        	<a href="#" class="btn border-theme pull-right">Visit Team Page</a>
                        </div>
                    </div><!--latest news-->
                </div><!--latest news col-->
            </div>
        </div>

    </div>
</div>
<div class="clear"></div>
<div class="divide80"></div>

<script id="result-template" type="text/x-handlebars-template">
    <div class="ProfileCard u-cf">
        <a href="<?php echo base_url(); ?>footy/team/view/{{name}}/{{id}}"><img class="ProfileCard-avatar" src="<?php echo base_url() . 'img/'; ?>{{logo}}"></a>

        <a href="<?php echo base_url(); ?>footy/team/view/{{name}}/{{id}}"><div class="ProfileCard-details text-left">
            <div class="ProfileCard-realName">{{name}}</div>
            <div class="ProfileCard-screenName">@ {{cityName}}</div>
            <div class="ProfileCard-description">{{countryName}}</div>
        </div></a>
    </div>
</script>

<script id="empty-template" type="text/x-handlebars-template">
    <div class="EmptyMessage">No Team Found.</div>
</script>