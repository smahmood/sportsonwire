<div class="fullwidthbanner">
    <div class="ken-burns-banner">
        <ul>    <!-- SLIDE  -->
            <li class="dark" data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-thumb="img/bg-1.jpg" data-delay="10000"  data-saveperformance="off" data-title="slider 1">
                <!-- MAIN IMAGE -->
                <img src="<?php echo base_url(); ?>img/bg-1.jpg"  alt="kenburns1"  data-bgposition="left center" data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone" data-bgfit="130" data-bgfitend="100" data-bgpositionend="right center">
                <!-- LAYERS -->
                <div class="caption customin customout tp-resizeme ken-burns-cap  font-alt" 
                     data-x="center" 
                     data-hoffset="0" 
                     data-y="center" 
                     data-voffset="-100" 
                     data-customin="x:-50;y:-300;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;" 
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 

                     data-speed="800" 
                     data-start="1000" 
                     data-startslide="1" 

                     data-easing="Power4.easeOut" 
                     data-endspeed="500" 
                     data-endeasing="Power4.easeIn">

                    Make your mark

                </div>

                <div class="caption customin customout tp-resizeme Ken-burns-heading font-alt" 
                     data-x="center" 
                     data-hoffset="0" 
                     data-y="center" 
                     data-voffset="0" 
                     data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" 
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 
                     data-speed="800" 
                     data-start="1300" 
                     data-startslide="1" 

                     data-easing="Power4.easeOut" 
                     data-endspeed="500" 
                     data-endeasing="Power4.easeIn">

                    Participate in Tournaments

                </div>


                <div class="caption customin customout tp-resizeme" 
                     data-x="center" 
                     data-hoffset="0" 
                     data-y="center" 
                     data-voffset="120" 
                     data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;" 
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 
                     data-speed="800" 
                     data-start="1500" 
                     data-startslide="1" 

                     data-easing="Power4.easeOut" 
                     data-endspeed="500" 
                     data-endeasing="Power4.easeIn">
                    <div class="rev-buttons">

                        <a href="#upcoming" class="btn btn-theme-bg btn-lg">
                            See all upcoming tournaments
                        </a>

                    </div>

                </div>
            </li>
            <!-- SLIDE  -->
            <li data-transition="slideup" data-slotamount="1" data-masterspeed="1500" data-thumb="img/bg-2.jpg" data-delay="10000"  data-saveperformance="off"  data-title="slider 2">
                <!-- MAIN IMAGE -->
                <img src="<?php echo base_url(); ?>img/bg-2.jpg"  alt="kenburns6"  data-bgposition="center bottom" data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone" data-bgfit="100" data-bgfitend="120" data-bgpositionend="center top">
                <!-- LAYERS -->


                <div class="caption customin customout tp-resizeme ken-burns-cap  font-alt" 
                     data-x="center" 
                     data-hoffset="0" 
                     data-y="center" 
                     data-voffset="-100" 
                     data-customin="x:-50;y:-300;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;" 
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 

                     data-speed="800" 
                     data-start="800" 
                     data-startslide="1" 

                     data-easing="Power4.easeOut" 
                     data-endspeed="500" 
                     data-endeasing="Power4.easeIn">

                    Announcement / Schedules / Personal Message

                </div>

                <div class="caption customin customout tp-resizeme Ken-burns-heading font-alt" 
                     data-x="center" 
                     data-hoffset="0" 
                     data-y="center" 
                     data-voffset="-14" 
                     data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" 
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 
                     data-speed="800" 
                     data-start="1200" 
                     data-startslide="1" 

                     data-easing="Power4.easeOut" 
                     data-endspeed="500" 
                     data-endeasing="Power4.easeIn">

                    Team communication made easy.

                </div>


                <div class="caption customin customout tp-resizeme" 
                     data-x="center" 
                     data-hoffset="0" 
                     data-y="center" 
                     data-voffset="83" 
                     data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;" 
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 
                     data-speed="800" 
                     data-start="1500" 
                     data-startslide="1" 

                     data-easing="Power4.easeOut" 
                     data-endspeed="500" 
                     data-endeasing="Power4.easeIn">

                    <div class="rev-buttons">

                        <a href="#" class="btn btn-theme-bg btn-lg">
                            See all features
                        </a>

                    </div>

                </div>
            </li>
            <!-- SLIDE  -->
            <li data-transition="slideup" data-slotamount="1" data-masterspeed="1500" data-thumb="img/bg-3.jpg" data-delay="10000"  data-saveperformance="off"  data-title="slider 3">
                <!-- MAIN IMAGE -->
                <img src="<?php echo base_url(); ?>img/bg-3.jpg"  alt="kenburns6"  data-bgposition="center bottom" data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone" data-bgfit="100" data-bgfitend="120" data-bgpositionend="center top">
                <!-- LAYERS -->


                <div class="caption customin customout tp-resizeme ken-burns-cap  font-alt" 
                     data-x="center" 
                     data-hoffset="0" 
                     data-y="center" 
                     data-voffset="-100" 
                     data-customin="x:-50;y:-300;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;" 
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 

                     data-speed="800" 
                     data-start="800" 
                     data-startslide="1" 

                     data-easing="Power4.easeOut" 
                     data-endspeed="500" 
                     data-endeasing="Power4.easeIn">

                    Exchange Notes / Pictures / Documents / Strategy

                </div>

                <div class="caption customin customout tp-resizeme Ken-burns-heading font-alt" 
                     data-x="center" 
                     data-hoffset="0" 
                     data-y="center" 
                     data-voffset="-14" 
                     data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" 
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 
                     data-speed="800" 
                     data-start="1200" 
                     data-startslide="1" 

                     data-easing="Power4.easeOut" 
                     data-endspeed="500" 
                     data-endeasing="Power4.easeIn">

                    Experience Virtual Dressing Room.

                </div>


                <div class="caption customin customout tp-resizeme" 
                     data-x="center" 
                     data-hoffset="0" 
                     data-y="center" 
                     data-voffset="83" 
                     data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;" 
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 
                     data-speed="800" 
                     data-start="1500" 
                     data-startslide="1" 

                     data-easing="Power4.easeOut" 
                     data-endspeed="500" 
                     data-endeasing="Power4.easeIn">

                    <div class="rev-buttons">

                        <a href="<?php echo base_url();?>sports/signup" class="btn btn-theme-bg btn-lg">
                            Join now
                        </a>

                    </div>

                </div>
            </li>
            <!-- SLIDE  -->
            <li data-transition="slideup" data-slotamount="1" data-masterspeed="1500" data-thumb="img/bg-6.jpg" data-delay="10000"  data-saveperformance="off"  data-title="slider 4">
                <!-- MAIN IMAGE -->
                <img src="<?php echo base_url(); ?>img/bg-6.jpg"  alt="kenburns6"  data-bgposition="center bottom" data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone" data-bgfit="100" data-bgfitend="120" data-bgpositionend="center top">
                <!-- LAYERS -->


                <div class="caption customin customout tp-resizeme ken-burns-cap  font-alt" 
                     data-x="center" 
                     data-hoffset="0" 
                     data-y="center" 
                     data-voffset="-100" 
                     data-customin="x:-50;y:-300;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;" 
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 

                     data-speed="800" 
                     data-start="800" 
                     data-startslide="1" 

                     data-easing="Power4.easeOut" 
                     data-endspeed="500" 
                     data-endeasing="Power4.easeIn">

                </div>

                <div class="caption customin customout tp-resizeme Ken-burns-heading font-alt" 
                     data-x="center" 
                     data-hoffset="0" 
                     data-y="center" 
                     data-voffset="-14" 
                     data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" 
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 
                     data-speed="800" 
                     data-start="1200" 
                     data-startslide="1" 

                     data-easing="Power4.easeOut" 
                     data-endspeed="500" 
                     data-endeasing="Power4.easeIn">

                    Arrange Tournaments - Invite Teams.

                </div>


                <div class="caption customin customout tp-resizeme" 
                     data-x="center" 
                     data-hoffset="0" 
                     data-y="center" 
                     data-voffset="83" 
                     data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;" 
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 
                     data-speed="800" 
                     data-start="1500" 
                     data-startslide="1" 

                     data-easing="Power4.easeOut" 
                     data-endspeed="500" 
                     data-endeasing="Power4.easeIn">

                    <div class="rev-buttons">

                        <a href="#" class="btn btn-theme-bg btn-lg">
                            Start a Tournament now
                        </a>

                    </div>

                </div>
            </li>
            <li class="dark" data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-thumb="img/cricket-bg-1.jpg" data-delay="10000"  data-saveperformance="off" data-title="slider 5">
                <!-- MAIN IMAGE -->
                <img src="<?php echo base_url(); ?>img/cricket-bg-1.jpg"  alt="kenburns1"  data-bgposition="left center" data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone" data-bgfit="130" data-bgfitend="100" data-bgpositionend="right center">
                <!-- LAYERS -->
                <div class="caption customin customout tp-resizeme ken-burns-cap  font-alt" 
                     data-x="center" 
                     data-hoffset="0" 
                     data-y="center" 
                     data-voffset="-100" 
                     data-customin="x:-50;y:-300;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;" 
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 

                     data-speed="800" 
                     data-start="1000" 
                     data-startslide="1" 

                     data-easing="Power4.easeOut" 
                     data-endspeed="500" 
                     data-endeasing="Power4.easeIn">

                    Make your mark

                </div>

                <div class="caption customin customout tp-resizeme Ken-burns-heading font-alt" 
                     data-x="center" 
                     data-hoffset="0" 
                     data-y="center" 
                     data-voffset="0" 
                     data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" 
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 
                     data-speed="800" 
                     data-start="1300" 
                     data-startslide="1" 

                     data-easing="Power4.easeOut" 
                     data-endspeed="500" 
                     data-endeasing="Power4.easeIn">

                    Participate in Tournaments

                </div>


                <div class="caption customin customout tp-resizeme" 
                     data-x="center" 
                     data-hoffset="0" 
                     data-y="center" 
                     data-voffset="120" 
                     data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;" 
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 
                     data-speed="800" 
                     data-start="1500" 
                     data-startslide="1" 

                     data-easing="Power4.easeOut" 
                     data-endspeed="500" 
                     data-endeasing="Power4.easeIn">
                    <div class="rev-buttons">

                        <a href="#upcoming" class="btn btn-theme-bg btn-lg">
                            See all upcoming tournaments
                        </a>

                    </div>

                </div>
            </li>
            <!-- SLIDE  -->
            <li data-transition="slideup" data-slotamount="1" data-masterspeed="1500" data-thumb="img/cricket-bg-2.jpg" data-delay="10000"  data-saveperformance="off"  data-title="slider 6">
                <!-- MAIN IMAGE -->
                <img src="<?php echo base_url(); ?>img/cricket-bg-2.jpg"  alt="kenburns6"  data-bgposition="center bottom" data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone" data-bgfit="100" data-bgfitend="120" data-bgpositionend="center top">
                <!-- LAYERS -->


                <div class="caption customin customout tp-resizeme ken-burns-cap  font-alt" 
                     data-x="center" 
                     data-hoffset="0" 
                     data-y="center" 
                     data-voffset="-100" 
                     data-customin="x:-50;y:-300;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;" 
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 

                     data-speed="800" 
                     data-start="800" 
                     data-startslide="1" 

                     data-easing="Power4.easeOut" 
                     data-endspeed="500" 
                     data-endeasing="Power4.easeIn">

                    Announcement / Schedules / Personal Message

                </div>

                <div class="caption customin customout tp-resizeme Ken-burns-heading font-alt" 
                     data-x="center" 
                     data-hoffset="0" 
                     data-y="center" 
                     data-voffset="-14" 
                     data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" 
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 
                     data-speed="800" 
                     data-start="1200" 
                     data-startslide="1" 

                     data-easing="Power4.easeOut" 
                     data-endspeed="500" 
                     data-endeasing="Power4.easeIn">

                    Team communication made easy.

                </div>


                <div class="caption customin customout tp-resizeme" 
                     data-x="center" 
                     data-hoffset="0" 
                     data-y="center" 
                     data-voffset="83" 
                     data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;" 
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 
                     data-speed="800" 
                     data-start="1500" 
                     data-startslide="1" 

                     data-easing="Power4.easeOut" 
                     data-endspeed="500" 
                     data-endeasing="Power4.easeIn">

                    <div class="rev-buttons">

                        <a href="#upcoming" class="btn btn-theme-bg btn-lg">
                            See all features
                        </a>

                    </div>

                </div>
            </li>
            <!-- SLIDE  -->
            <li data-transition="slideup" data-slotamount="1" data-masterspeed="1500" data-thumb="img/cricket-bg-3.jpg" data-delay="10000"  data-saveperformance="off"  data-title="slider 7">
                <!-- MAIN IMAGE -->
                <img src="<?php echo base_url(); ?>img/cricket-bg-3.jpg"  alt="kenburns6"  data-bgposition="center bottom" data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone" data-bgfit="100" data-bgfitend="120" data-bgpositionend="center top">
                <!-- LAYERS -->


                <div class="caption customin customout tp-resizeme ken-burns-cap  font-alt" 
                     data-x="center" 
                     data-hoffset="0" 
                     data-y="center" 
                     data-voffset="-100" 
                     data-customin="x:-50;y:-300;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;" 
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 

                     data-speed="800" 
                     data-start="800" 
                     data-startslide="1" 

                     data-easing="Power4.easeOut" 
                     data-endspeed="500" 
                     data-endeasing="Power4.easeIn">

                    Exchange Notes / Pictures / Documents / Strategy

                </div>

                <div class="caption customin customout tp-resizeme Ken-burns-heading font-alt" 
                     data-x="center" 
                     data-hoffset="0" 
                     data-y="center" 
                     data-voffset="-14" 
                     data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" 
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 
                     data-speed="800" 
                     data-start="1200" 
                     data-startslide="1" 

                     data-easing="Power4.easeOut" 
                     data-endspeed="500" 
                     data-endeasing="Power4.easeIn">

                    Experience Virtual Dressing Room.

                </div>


                <div class="caption customin customout tp-resizeme" 
                     data-x="center" 
                     data-hoffset="0" 
                     data-y="center" 
                     data-voffset="83" 
                     data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;" 
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 
                     data-speed="800" 
                     data-start="1500" 
                     data-startslide="1" 

                     data-easing="Power4.easeOut" 
                     data-endspeed="500" 
                     data-endeasing="Power4.easeIn">

                    <div class="rev-buttons">

                        <a href="<?php echo base_url();?>sports/signup" class="btn btn-theme-bg btn-lg">
                            Join now
                        </a>

                    </div>

                </div>
            </li>
            <!-- SLIDE  -->
            <li data-transition="slideup" data-slotamount="1" data-masterspeed="1500" data-thumb="img/cricket-bg-4.jpg" data-delay="10000"  data-saveperformance="off"  data-title="slider 8">
                <!-- MAIN IMAGE -->
                <img src="<?php echo base_url(); ?>img/cricket-bg-4.jpg"  alt="kenburns6"  data-bgposition="center bottom" data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone" data-bgfit="100" data-bgfitend="120" data-bgpositionend="center top">
                <!-- LAYERS -->


                <div class="caption customin customout tp-resizeme ken-burns-cap  font-alt" 
                     data-x="center" 
                     data-hoffset="0" 
                     data-y="center" 
                     data-voffset="-100" 
                     data-customin="x:-50;y:-300;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;" 
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 

                     data-speed="800" 
                     data-start="800" 
                     data-startslide="1" 

                     data-easing="Power4.easeOut" 
                     data-endspeed="500" 
                     data-endeasing="Power4.easeIn">

                </div>

                <div class="caption customin customout tp-resizeme Ken-burns-heading font-alt" 
                     data-x="center" 
                     data-hoffset="0" 
                     data-y="center" 
                     data-voffset="-14" 
                     data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" 
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 
                     data-speed="800" 
                     data-start="1200" 
                     data-startslide="1" 

                     data-easing="Power4.easeOut" 
                     data-endspeed="500" 
                     data-endeasing="Power4.easeIn">

                    Arrange Tournaments - Invite Teams.

                </div>


                <div class="caption customin customout tp-resizeme" 
                     data-x="center" 
                     data-hoffset="0" 
                     data-y="center" 
                     data-voffset="83" 
                     data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;" 
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 
                     data-speed="800" 
                     data-start="1500" 
                     data-startslide="1" 

                     data-easing="Power4.easeOut" 
                     data-endspeed="500" 
                     data-endeasing="Power4.easeIn">

                    <div class="rev-buttons">

                        <a href="#" class="btn btn-theme-bg btn-lg">
                            Start a Tournament now
                        </a>

                    </div>

                </div>
            </li>
        </ul>
    </div>
</div><!--full width banner-->

<section class="intro-text">
    <div class="container ">
        <div class="row">
            <div class="col-md-12 col-sm-12 text-center">
                <h2 class="wow animated fadeInDown">We are <span class="colored-text">Sports Enthusiastics</span></h2>
                <p class="lead animated fadeIn">
                    We are sports enthusiastics and always try to stay involved in the game. SportsOnWire is an effort from our side to do that. Bringing technology and sports together. We ensure a great experience and service to our clients. Also we try to improve in every aspect we can. So join us today and <strong>Get in the Game.</strong>
                </p>
                <div class="divide30"></div>
                <a href="#" class=" btn border-black btn-lg">Contact us today</a>
            </div>
        </div>
    </div>
</section><!--intro section end-->

<section class="services-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="center-heading">
                    <h2>SportsOnWire-Football Features</h2>
                    <span class="center-line"></span>
                </div>
            </div>                   
        </div><!--center heading row-->
        <div class="row">
            <div class="col-md-4 col-sm-6 margin20">
                <div class="services-box wow animated fadeInUp" data-wow-duration="700ms" data-wow-delay="100ms">
                    <div class="services-box-icon">
                        <i class="fa fa-users"></i>
                    </div><!--services icon-->
                    <div class="services-box-info">
                        <h4>Team Manager</h4>
                        <p>
                            SportsOnWire offers a unique panel specially built for the managers of the team, so they can keep track of the players progress and also deliver news and reports in timely manner.
                        </p>
                    </div>
                </div><!--services box-->
            </div><!--services col-->
            <div class="col-md-4 col-sm-6 margin20">
                <div class="services-box wow animated fadeInUp" data-wow-duration="700ms" data-wow-delay="200ms">
                    <div class="services-box-icon">
                        <i class="fa fa-user"></i>
                    </div><!--services icon-->
                    <div class="services-box-info">
                        <h4>Members</h4>
                        <p>
                            Members of a team also have a panel built just for them, so that they can communicate with other team members and also share important news. They can keep track of the latest news and events. So that they don't miss any opportunity.
                        </p>
                    </div>
                </div><!--services box-->
            </div><!--services col-->
            <div class="col-md-4 col-sm-6 margin20">
                <div class="services-box wow animated fadeInUp" data-wow-duration="700ms" data-wow-delay="300ms">
                    <div class="services-box-icon">
                        <i class="fa fa-file-text-o"></i>
                    </div><!--services icon-->
                    <div class="services-box-info">
                        <h4>Docs</h4>
                        <p>
                            SportsOnWire ensures a safe locker to everyone and every team so that important documents can be shared. You can rely on us, and we assure you that your documents will never go missing or go in the wrong hands.
                        </p>
                    </div>
                </div><!--services box-->
            </div><!--services col-->

            <div class="col-md-4 col-sm-6 margin20">
                <div class="services-box wow animated fadeInUp" data-wow-duration="700ms" data-wow-delay="400ms">
                    <div class="services-box-icon">
                        <i class="fa fa-picture-o"></i>
                    </div><!--services icon-->
                    <div class="services-box-info">
                        <h4>Pictures</h4>
                        <p>
                            Enjoy the good moments, SportsOnWire also provide the facility of sharing pictures. So that you can cherish those precious moments even after the game.
                        </p>
                    </div>
                </div><!--services box-->
            </div><!--services col-->
            <div class="col-md-4 col-sm-6 margin20">
                <div class="services-box wow animated fadeInUp" data-wow-duration="700ms" data-wow-delay="500ms">
                    <div class="services-box-icon">
                        <i class="fa fa-trophy"></i>
                    </div><!--services icon-->
                    <div class="services-box-info">
                        <h4>Tournaments</h4>
                        <p>
                            SportsOnWire provides you with the facility of not only taking part in a tournament but arranging a tournament. You will have access to all the necessary things. Also our team is always there to help you if you ever find yourself stuck in a problem.
                        </p>
                    </div>
                </div><!--services box-->
            </div><!--services col-->
            <div class="col-md-4 col-sm-6 margin20">
                <div class="services-box wow animated fadeInUp" data-wow-duration="700ms" data-wow-delay="600ms">
                    <div class="services-box-icon">
                        <i class="fa fa-mobile"></i>
                    </div><!--services icon-->
                    <div class="services-box-info">
                        <h4>Chatrooms</h4>
                        <p>
                            You will have access to chatrooms. You can discuss all the important details with your friends and others. Want to discuss some controversial decsion of a match, SportsOnWire take care of that by providing you with chatrooms that you can join.

                        </p>

                    </div>
                </div><!--services box-->
            </div><!--services col-->
        </div><!--services row-->
    </div>
</section><!--section services-->
<div class="divide40"></div>
<section class="portfolio">
    <div id="upcoming" class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="center-heading">
                    <h2>Upcoming Tournaments</h2>
                    <span class="center-line"></span>
                </div>
            </div>                   
        </div><!--center heading row-->
        <div class="row">
            <div class="col-md-12">
                <div id="work-carousel" class="owl-carousel owl-spaced">
                    <div>

                        <div class="item-img-wrap ">
                            <img src="<?php echo base_url(); ?>img/img-1.jpg" class="img-responsive" alt="workimg">
                            <div class="item-img-overlay">
                                <a href="img/img-1.jpg" class="show-image">
                                    <span></span>
                                </a>
                            </div>
                        </div> <!--item img wrap-->
                        <div class="work-desc">
                            <h3><a href="portfolio-single.html">FAST Gala</a></h3>
                            <span>Lahore, Pakistan</span>
                        </div><!--work desc-->
                    </div><!--owl item-->
                    <div> 
                        <div class="item-img-wrap ">
                            <img src="<?php echo base_url(); ?>img/img-2.jpg" class="img-responsive" alt="workimg">
                            <div class="item-img-overlay">
                                <a href="img/img-2.jpg" class="show-image">
                                    <span></span>
                                </a>
                            </div>
                        </div> 
                        <div class="work-desc">
                            <h3><a href="portfolio-single.html">LUMS Nationals</a></h3>
                            <span>Lahore, Pakistan</span>
                        </div><!--work desc-->
                    </div><!--owl item-->
                    <div> 
                        <div class="item-img-wrap ">
                            <img src="<?php echo base_url(); ?>img/img-3.jpg" class="img-responsive" alt="workimg">
                            <div class="item-img-overlay">
                                <a href="img/img-3.jpg" class="show-image">
                                    <span></span>
                                </a>
                            </div>
                        </div> 
                        <div class="work-desc">
                            <h3><a href="portfolio-single.html">Play-On Tournament</a></h3>
                            <span>Lahore, Pakistan</span>
                        </div><!--work desc-->
                    </div><!--owl item-->
                    <div> 
                        <div class="item-img-wrap ">
                            <img src="<?php echo base_url(); ?>img/img-4.jpg" class="img-responsive" alt="workimg">
                            <div class="item-img-overlay">
                                <a href="img/img-4.jpg" class="show-image">
                                    <span></span>
                                </a>
                            </div>
                        </div> 
                        <div class="work-desc">
                            <h3><a href="portfolio-single.html">Bestival - BNU</a></h3>
                            <span>BNU Islamabad, Pakistan</span>
                        </div><!--work desc-->
                    </div><!--owl item-->
                    <div> 
                        <div class="item-img-wrap ">
                            <img src="<?php echo base_url(); ?>img/img-5.jpg" class="img-responsive" alt="workimg">
                            <div class="item-img-overlay">
                                <a href="img/img-5.jpg" class="show-image">
                                    <span></span>
                                </a>
                            </div>
                        </div> 
                        <div class="work-desc">
                            <h3><a href="portfolio-single.html">PFL - Football Tournament</a></h3>
                            <span>Karachi, Pakistan</span>
                        </div><!--work desc-->
                    </div><!--owl item-->
                    <div> 
                        <div class="item-img-wrap ">
                            <img src="<?php echo base_url(); ?>img/img-6.jpg" class="img-responsive" alt="workimg">
                            <div class="item-img-overlay">
                                <a href="img/img-6.jpg" class="show-image">
                                    <span></span>
                                </a>
                            </div>
                        </div> 
                        <div class="work-desc">
                            <h3><a href="portfolio-single.html">Barclays Football Tournament</a></h3>
                            <span>Lahore, Pakistan</span>
                        </div><!--work desc-->
                    </div><!--owl item-->
                </div>
            </div>
        </div><!--row-->
        <div class="divide30"></div>
        <div class="row">
            <div class="col-md-12 text-center">
                <a href="#" class="btn border-theme btn-lg wow animated fadeInUp"><i class="fa fa-bars"></i> Load More Tournaments</a>
            </div>
        </div>
    </div><!--container-->
</section><!--recent work section -->

<section id="cta-1">
    <div class="container">
        <h1>Do you have a tournament or team to manage? Let us help you.</h1>
        <a href="<?php echo base_url();?>sports/signup" class="btn border-white btn-lg">Join us today</a>
    </div>
</section>