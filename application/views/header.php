<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php if(isset($page_title)): ?>
            <title>SportsOnWire - <?php echo $page_title; ?></title>
        <?php else: ?>
            <title>SportsOnWire</title>
        <?php endif ?>

        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- custom css (blue color by default) -->
        <!--<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="screen">-->
        <!-- custom css (green color ) -->
        <!--      <link href="<?php echo base_url(); ?>css/style-green.css" rel="stylesheet" type="text/css" media="screen">-->
        <!-- custom css (red color ) -->
                <link href="<?php echo base_url(); ?>css/style-red.css" rel="stylesheet" type="text/css" media="screen">
        <!-- custom css (yellow color ) -->
        <!--       <link href="<?php echo base_url(); ?>css/style-yellow.css" rel="stylesheet" type="text/css" media="screen">-->
        <!-- custom css (sea-greean color ) -->
        <!--      <link href="<?php echo base_url(); ?>css/style-sea-green.css" rel="stylesheet" type="text/css" media="screen">-->
        <!-- custom css (style-gold color ) -->
        <!--       <link href="<?php echo base_url(); ?>css/style-gold.css" rel="stylesheet" type="text/css" media="screen">-->
        <!-- font awesome for icons -->
        <link href="<?php echo base_url(); ?>font-awesome-4.3.0/css/font-awesome.min.css" rel="stylesheet">
        <!-- flex slider css -->
        <link href="<?php echo base_url(); ?>css/flexslider.css" rel="stylesheet" type="text/css" media="screen">
        <!-- animated css  -->
        <link href="<?php echo base_url(); ?>css/animate.css" rel="stylesheet" type="text/css" media="screen">
        <!--web fonts-->  


        <!--Revolution slider css-->
        <link href="<?php echo base_url(); ?>rs-plugin/css/settings.css" rel="stylesheet" type="text/css" media="screen">
        <link href="<?php echo base_url(); ?>css/rev-style.css" rel="stylesheet" type="text/css" media="screen">
        <!--google fonts-->
        <link href="<?php echo base_url(); ?>css/custom-style.css" rel="stylesheet" type="text/css">
        
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <!--owl carousel css-->
        <link href="<?php echo base_url(); ?>css/owl.carousel.css" rel="stylesheet" type="text/css" media="screen">
        <link href="<?php echo base_url(); ?>css/owl.theme.css" rel="stylesheet" type="text/css" media="screen">
        <!--mega menu -->
        <link href="<?php echo base_url(); ?>css/yamm.css" rel="stylesheet" type="text/css">
        <!--popups css-->
        <link href="<?php echo base_url(); ?>css/magnific-popup.css" rel="stylesheet" type="text/css">
        <!-- hover class -->
        <link href="<?php echo base_url(); ?>css/hover-min.css" rel="stylesheet" type="text/css" media="screen">

        <link href="<?php echo base_url(); ?>css/croppic.css" rel="stylesheet" type="text/css" media="screen">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="<?php echo base_url(); ?><?php echo base_url(); ?>https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="<?php echo base_url(); ?><?php echo base_url(); ?>https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        <div id="header-top" class="hidden-xs">
            <div class="container">
                <div class="top-bar">
                    <div class="pull-left sample-1right">
                        <a><i class="fa fa-phone"></i> Any questions? Call us: <span class="colored-text">+92 3236910600</span> </a> 
                         <a><i class="fa fa-envelope"></i> Mail us: <span class="colored-text">support@sportsonwire.com</span> </a>
                    </div>
                    <div class="pull-right">
                        <ul class="list-inline top-social">
                            <li>Follow us:</li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div><!--top bar end hidden in small devices-->

        <div class="navbar navbar-default navbar-static-top yamm sticky" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>img/logo.png" alt="SportsOnWire"></a>
                </div>

                <?php if($logged_in){ ?>
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a href="#" class=" dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i></a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-login-box animated fadeInUp">

                                        <div class="panel-encase">
                                            <div class="panel-empty">
                                                <img src="<?php echo base_url();?>img/troy_tips.png" alt="">
                                                <br/><br/>
                                                <b>All your alerts are up to date!</b>
                                            </div>
                                        </div>
                                </div>
                            </li> <!--alert menu li end here-->
                            <?php if(isset($sports_page)): ?>
                            <li class="dropdown">
                                <a href="<?php echo base_url() . $sports_page; ?>/dashboard">Dashboard</a>
                            </li>
							<li class="dropdown">
                                <a href="<?php echo base_url() . $sports_page; ?>/tournaments">Tournaments</a>
                            </li>
                            <?php endif ?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Sports Home <i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="<?php echo base_url() . 'footy';?>"><i class="fa fa-futbol-o"></i> Football</a></li>
                                    <li><a href="<?php echo base_url() . 'cric';?>"><i class="fa fa-pencil"></i> Cricket</a></li>
                                </ul>
                            </li>
                            <li class="dropdown active">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $basic->email; ?><i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="<?php echo base_url(); ?>sports/account/view"><i class="fa fa-cog"></i> Account Profile</a></li>
                                    <li><a href="<?php echo base_url(); ?>sports/account/edit"><i class="fa fa-pencil"></i> Edit Account</a></li>
                                    <li class="dropdown-submenu">
                                        <a tabindex="-1" href="#"><i class="fa fa-cogs"></i> Sports Profile </a>
                                        <ul class="dropdown-menu">
                                            <li><a href="<?php echo base_url(); ?>cric/edit/<?php echo $basic->id;?>">Cricket</a></li>
                                            <li><a href="<?php echo base_url(); ?>footy/edit/<?php echo $basic->id;?>">Football</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="<?php echo base_url();?>auth/logout"><i class="fa fa-sign-out"></i> Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div><!--/.nav-collapse -->
                <?php }else{ ?>
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown <?php if($page_name=='landing'){echo 'active';} ?>">
                                <a href="<?php echo base_url(); ?>">Home</a>
                            </li>
                            <!--menu home li end here-->
                            <li class="dropdown <?php if($page_name=='about'){echo 'active';} ?>">
                                <a href="<?php echo base_url();?>sports/about">About</a>
                            </li>
                            <!--mega menu end-->
                            <li class="dropdown">
                                <a href="#" class=" dropdown-toggle" data-toggle="dropdown">Login <i class="fa fa-lock"></i></a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-login-box animated fadeInUp">
                                    <div class="alert-danger alert authentication-alert" style="display:none;"></div>
                                    <form action="#" class="login_form" data-toggle="validator" id="signinForm" role="form" method="POST" action="<?php echo base_url();?>sports/login">
                                        <h4>Signin</h4>

                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                <input required name="identity" type="email" class="form-control" placeholder="Username" data-error="Please enter valid email" autofocus>
                                            </div>
                                            <br>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                                <input name="password" required type="password" class="form-control" placeholder="Password"  data-error="Please enter password">
                                            </div>
                                            <div id="err" class="input-group" style="color:red">
                                                    
                                            </div>
                                            <div class="checkbox pull-left">
                                                <label>
                                                    <input name="remember" type="checkbox"> Remember me
                                                </label>
                                            </div>
                                            <button type="submit" class="btn btn-theme-bg pull-right">Login</button>
                                            <!--                                        <button type="submit" class="btn btn-theme pull-right">Login</button>                 -->
                                            <div class="clearfix"></div>
                                            <hr>
                                            <p>Don't have an account! <a href="<?php echo base_url(); ?>/sports/signup">Register Now</a></p>
                                        </div>
                                    </form>
                                </div>
                            </li> <!--menu login li end here-->
                            <li class="dropdown <?php if($page_name=='login_register'){echo 'active';} ?>">
                                <a href="<?php echo base_url(); ?>sports/signup">Join For Free</a>
                            </li>
                        </ul>
                    </div><!--/.nav-collapse -->
                <?php } ?>
            </div><!--container-->
        </div><!--navbar-default-->