<div class="container">
	<!--<section class="me-intro parallax" data-stellar-background-ratio="0.5">
	    <div class="container text-left">
	        <h2 class="animated slideInLeft">Hello! myself assan</h2>
	        <p class="lead animated slideInLeft ">I am a Graphics and Web designer </p>
	        <p><a href="#" class="btn btn-lg btn-theme-bg">Hire Me</a></p>
	    </div>
	</section>me intro parallax-->
	<div class="row">
		<div class="col-md-12">
			
			<div class="cover-back" style="background: url('<?php echo base_url() . 'uploads/' . $info->image; ?>') no-repeat;">
			
				<?php if($info->created_by==$this->ion_auth->get_user_id())
						{ ?>
						<div id="cover-button" class="cover-img-button"><i class="fa fa-camera"></i></div>
						<?php } ?>	
				
				<div class="cover-photo" id="cover-header"></div>
			</div>
			<input type="hidden" class="cover_type" value="team" />
			<input type="hidden" class="cover_id" value="<?php echo $info->id ?>" />
		</div>
	</div>
	<div class="clear"></div>
	<div class="row">
		<div class="col-md-12">
			<div class="breadcrumb-wrap team-links">
			    <div class="navbar team-navbar">
		            <ul class="nav navbar-nav navbar-left team-navbar-nav">
						<li class="dropdown">
		                    <a href="#"><?php echo $info->name;?></a>
		                </li>
		                <li class="dropdown">
		                    <a href="<?php echo base_url() . 'footy/team/view/' . str_replace(' ', '-', $info->name).'/'.$info->id.'/team-gallery'; ?>">Photos</a>
		                </li>
		            </ul>
					<?php if($info->created_by==$this->ion_auth->get_user_id())
						{ ?>
						<button href="#add-member-modal" data-toggle="modal" class="btn border-theme pull-right"  >Invite Teams</button>
						<?php }  else
						{ ?>
                    <?php if (count($join_leave_tournament_teams['teams_to_leave']) > 0): ?>
                    <a href="#leave-team-modal" data-toggle="modal" class="btn border-theme pull-right"><i class="fa fa-minus-circle"></i>Leave Tournament</a>
                    <?php endif; ?>
                    <?php if (count($join_leave_tournament_teams['teams_to_join']) > 0): ?>
                    <a href="#join-team-modal" data-toggle="modal" class="btn border-theme pull-right"><i class="fa fa-plus-circle"></i>Join Tournament</a>
                    <?php endif; ?>
				<!--<button class="btn border-theme pull-right" onclick="joinTeam(<?php echo $info->id ?>)" >Join Tournament</button>-->
							
						<?php } ?>
		        </div>
			</div><!--breadcrumbs-->
		</div>
	</div>
	<div class="clear"></div>
	<div class="divide80"></div>
	<div class="row">
		<div class="col-sm-4 margin30">
            <div class="latest-new">
                <div class="l-news-desc account-info clearfix">
                    <h3><a href="#"><?php echo $info->name;?></a></h3>
					<p style="text-align:center;"><?php if($info->desc!=""){ echo substr($info->desc, 0, 70)."...";; } ?></p>
				<?php if($info->desc!=""){ ?>	<button class="btn border-theme pull-right ">Read More</button> <?php } ?>
                </div>
				
                <div class="panel-footer clearfix">
					<div class=" clearfix" >
						<h4>Organizer</h4>
					</div>
                    <div class="pull-left clearfix" >
						<a href="#"><img  style="width:40px;" class="empty-member-pic pull-left" src="<?php if($creator->profile_picture==""){ $creator->profile_picture="default.jpg" ; } echo base_url().'uploads/'.$creator->profile_picture;?>" alt=""></a>
					</div>
                    <div class="pull-left clearfix">
						<a href="<?php echo base_url();?>footy/playerProfile/<?php echo $creator->id; ?>"><?php echo $creator->first_name . " " . $creator->last_name; ?></a><br/>
						<span style="color:#989898;">Contact : <?php echo $creator->phone ;?></span>
					</div>
                </div>
            </div><!--latest news-->
            <div class="divide40"></div>
            <div class="box box-danger">
	            <div class="box-header with-border">
	              <h3 class="box-title">Participating Teams</h3>
	              <div class="box-tools pull-right">
	              </div>
	            </div><!-- /.box-header -->
	            <div class="box-body no-padding">
	              <ul class="users-list clearfix">
                      <?php if (!is_null($participating_teams)): ?>
                        <?php foreach ($participating_teams as $team): ?>
                            <li>
							  <img src="<?php echo base_url(); ?>uploads/<?php echo $team['team_logo']; ?>" alt="Team Image"/>
							  <a class="users-list-name" href="<?php echo base_url(); ?>footy/team/view/<?php echo $team['team_name']; ?>/<?php echo $team['id']; ?>"><?php echo $team['team_name']; ?></a>		  
							</li>
                        <?php endforeach; ?>
                      <?php endif; ?>
	              </ul><!-- /.teams-list -->
	            </div><!-- /.box-body -->
	            <!--<div class="box-footer text-center">
	            </div>--><!-- /.box-footer -->
	          </div><!--/.box -->
        </div><!--latest news col-->
        <div class="col-md-8">
			<div class="center-heading ">
				<button class="btn border-theme" onclick="$('#postImageToWall').hide();$('#postToWall').show();" > POST TO WALL </button>
				<button class="btn border-theme" onclick="$('#postToWall').hide();$('#postImageToWall').show();"  > UPLOAD IMAGE TO WALL </button>
			</div>	
			<div id="postToWall" class="clearfix" style="display:none">
				<form action="#" method="POST" id="postToWallForm" onsubmit="return postToWall()">
					<input maxlength="1000" type="text" required=""  placeholder="What's on your mind"  name="post" class="form-control">
					<input type="hidden" name="team" value="<?php echo $info->id;?>"/>
					<input class="btn border-theme pull-right " type="submit" value="POST" />
				</form>	
			</div>
			<div id="postImageToWall" class="clearfix" style="display:none">
				<form action="#" id="postImageToWallForm" onsubmit="return postImageToWall()" method="POST" onsubmit="return false">
					<input type="text"   placeholder="What's on your mind"  name="post" class="form-control">
					<input type="hidden" name="team" value="<?php echo $info->id;?>"/>
					<input type="hidden" name="pic" value="" id="wallpic"/>
					
					<input class="btn border-theme pull-right " type="submit" value="POST" />
					<input class="btn border-theme pull-right " onclick="$('#imageInput').click();" type="button" value="Upload" />
					<img style="display:none;"  src="" class="wallpicdisp pull-right" />
					<div style="width:70%;"><div  class="progress-bar progress-bar-success progress-bar-striped" id="progressbar" role="progressbar"
					aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:0%">
					<span id="progressPercent" >0</span>
					</div></div>
					
				</form>	
			</div>
        	<div class="center-heading ">
                <h2>Recent <strong>Activity</strong></h2>
                <span class="center-line"></span>
            </div>
			<?php if($feed)
			{ ?>
			<ul class="timeline">
				<!-- timeline time label date("j M Y", $ts)-->
				<?php $date='';$count=0; foreach($feed as $f)
				{
					$count=$count+1;
					$tdate=explode(" ",$f->time_stamp);
					$tdate=$tdate[0];
					if($date=='')
					{ $date=$tdate; ?>
						<li class="time-label">
							<span class="bg-red"><?php $time = strtotime($date);echo date("j M Y", $time);?> </span>
						</li>
					<?php }
					else if($date!=$tdate)
					{ $date=$tdate; ?>
						<li class="time-label">
							<span <?php if($count%2==1){ ?>class="bg-red" <?php } else{ ?>class="bg-green" <?php } ?> ><?php $time = strtotime($date);echo date("j M Y", $time);?> </span>
						</li>
					<?php } 
					if($f->post_type==0)
					{ ?>
						<li>
							<i class="fa fa-envelope bg-blue"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i> 12:05</span>
								<h3 class="timeline-header"><a href="#">Support Team</a> sent you an email</h3>
								<div class="timeline-body">
									Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
									weebly ning heekya handango imeem plugg dopplr jibjab, movity
									jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
									quora plaxo ideeli hulu weebly balihoo...
								</div>
								<div class='timeline-footer'>
									<a class="btn btn-primary btn-xs">Read more</a>
									<a class="btn btn-danger btn-xs">Delete</a>
								</div>
							</div>
						</li>
						<!-- END timeline item -->
						<!-- timeline item -->
						<li>
							<i class="fa fa-user bg-aqua"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i> 5 mins ago</span>
								<h3 class="timeline-header no-border"><a href="#">Sarah Young</a> accepted your friend request</h3>
							</div>
						</li>
						<!-- END timeline item -->
						<!-- timeline item -->
						<li>
							<i class="fa fa-comments bg-yellow"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i> 27 mins ago</span>
								<h3 class="timeline-header"><a href="#">Jay White</a> commented on your post</h3>
								<div class="timeline-body">
									Take me to your leader!
									Switzerland is small and neutral!
									We are more like Germany, ambitious and misunderstood!
								</div>
								<div class='timeline-footer'>
									<a class="btn btn-warning btn-flat btn-xs">View comment</a>
								</div>
							</div>
						</li>
						<!-- END timeline item -->
						<!-- timeline time label -->
						<li class="time-label">
							<span class="bg-green">3 Jan. 2014</span>
						</li>
						<!-- /.timeline-label -->
						<!-- timeline item -->
						<li>
							<i class="fa fa-camera bg-purple"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i> 2 days ago</span>
								<h3 class="timeline-header"><a href="#">Mina Lee</a> uploaded new photos</h3>
								<div class="timeline-body">
									<img src="http://placehold.it/150x100" alt="..." class='margin' />
									<img src="http://placehold.it/150x100" alt="..." class='margin'/>
									<img src="http://placehold.it/150x100" alt="..." class='margin'/>
									<img src="http://placehold.it/150x100" alt="..." class='margin'/>
								</div>
							</div>	
						</li>
						<!-- END timeline item -->
						<!-- timeline item -->
						<li>
							<i class="fa fa-video-camera bg-maroon"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i> 5 days ago</span>
								<h3 class="timeline-header"><a href="#">Mr. Doe</a> shared a video</h3>
								<div class="timeline-body">
									<div class="embed-responsive embed-responsive-16by9">
										<iframe src="https://player.vimeo.com/video/124168998" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
									</div>
								</div>
								<div class="timeline-footer">
									<a href="#" class="btn btn-xs bg-maroon">See comments</a>
								</div>
							</div>
						</li>
						<!-- END timeline item -->
						<li>
							<i class="fa fa-clock-o bg-gray"></i>
						</li>
					<?php }
					if($f->post_type==2)
					{ ?>
						<li>
							<i class="fa fa-user bg-aqua"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i> <?php $temp=explode(" ",$f->time_stamp); $temp=explode(":",$temp[1]); echo $temp[0].":".$temp[1]; ?> </span>
								<h3 class="timeline-header no-border"><a href="#"><b> <?php echo $f->team->team_name ?></b> </a> joined the tournament </h3>
							</div>
						</li>
					<?php }
					if($f->post_type==3)
					{ ?>
						<li>
							<i class="fa fa-camera bg-purple"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i>  <?php $temp=explode(" ",$f->time_stamp); $temp=explode(":",$temp[1]); echo $temp[0].":".$temp[1]; ?> </span>
								<h3 class="timeline-header"><a href="#"><?php echo $info->name; ?></a> upated thier cover photo</h3>
								<div class="timeline-body">
									<img style="width:96%;"src="<?php echo base_url();?>uploads/<?php echo $f->cover->path;?>" alt="..." class='margin' />
								</div>
							</div>	
						</li>
					<?php }
					if($f->post_type==5)
					{ if($f->wall->post_type==1){ ?>
						<li>
							<i class="fa fa-comments bg-yellow"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i> <?php $temp=explode(" ",$f->time_stamp); $temp=explode(":",$temp[1]); echo $temp[0].":".$temp[1]; ?> </span>
								<h3 class="timeline-header"><a href="<?php echo base_url();?>footy/playerProfile/<?php echo $f->user->id;?>"><?php echo $f->user->first_name." ".$f->user->last_name; ?></a> posted on your wall</h3>
								<div class="timeline-body">
									<?php echo $f->wall->text; ?>
								</div>
							</div>
						</li>
					<?php }else{
					?>
						<li>
							<i class="fa fa-comments bg-yellow"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i> <?php $temp=explode(" ",$f->time_stamp); $temp=explode(":",$temp[1]); echo $temp[0].":".$temp[1]; ?> </span>
								<h3 class="timeline-header"><a href="<?php echo base_url();?>footy/playerProfile/<?php echo $f->user->id;?>"><?php echo $f->user->first_name." ".$f->user->last_name; ?></a> posted on your wall</h3>
								<div class="timeline-body">
									<b style="margin-botton:8px;"><?php echo $f->wall->text; ?></b>
									<img src="<?php echo base_url();?>uploads/<?php echo $f->wall->image_path; ?>" style="width:96%"/>
								</div>
							</div>
						</li>
					<?php 
					
					}	}
				}
				?>
				
				<!-- /.timeline-label -->
				<!-- timeline item -->
				<li>
							<i class="fa fa-clock-o bg-gray"></i>
						</li>
				<div class="divide40"></div>
			</ul>
			<?php } else
			{
				echo '<center><p>There is no recent activity on this page.</p></center>';
			}  ?>
        </div>
	</div>
	<div class="clear"></div>
	<div class="divide80"></div>
</div>
<div class="modal fade in" id="add-member-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?php echo base_url();?>footy/team/create" id="addmemberform" method="POST" role="form" onsubmit="return inviteEmail()" data-toggle="validator">
                <div class="modal-header text-center">
                    <h3 style="margin-top:33px;"><i class="fa fa-plus-circle"></i> Add Team Member</h3>
                </div>
                <div class="modal-body clearfix">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Invite by Email</label>
                                    <input type="email" class="form-control" name="email" data-minlength="3" placeholder="Enter Email" data-error="Please Enter Valid Email " required>
									<input type="hidden" name="team" value="<?php echo $info->id; ?>">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-sm-12" style="padding:20px 0;">
						<input type="submit" class="btn border-theme pull-right" value="Invite">
                        <a href="#" class="btn btn-default pull-left">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<form style="display:none;" action="<?php echo base_url();?>sports/uploadImage" method="post" enctype="multipart/form-data" id="MyUploadForm">
		<input onchange="$('#MyUploadForm').submit();" name="image_file" id="imageInput" type="file" />
		<input type="submit"  id="submit-btn" value="Upload" />

		</form>
		<div style="display:none;" id="output"></div>

<!-- Modal -->
<div class="modal fade in" id="join-team-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
                <div class="modal-header text-center">
                    <h3 style="margin-top:33px;"><i class="fa fa-plus-circle"></i>Join Tournament</h3>
                </div>
                <div class="modal-body clearfix">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Select Team</label>
                                    <select class="form-control" id="team_id_join" data-error="Please select a team" required>
                                        <?php foreach ($join_leave_tournament_teams['teams_to_join'] as $team): ?>
                                            <option value="<?php echo $team['id']; ?>"><?php echo $team['team_name']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-sm-12" style="padding:20px 0;">
                        <input type="submit" class="btn border-theme pull-right" value="Join Tournament" onclick="joinTournament(<?php echo $info->id; ?>)">
                        <input type="submit" class="btn border-theme pull-left" value="Cancel" data-dismiss="modal" aria-hidden="true">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade in" id="leave-team-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
                <div class="modal-header text-center">
                    <h3 style="margin-top:33px;"><i class="fa fa-minus-circle"></i>Leave Tournament</h3>
                </div>
                <div class="modal-body clearfix">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Select Team</label>
                                    <select class="form-control" id="team_id_leave" data-error="Please select a team" required>
                                        <?php foreach ($join_leave_tournament_teams['teams_to_leave'] as $team): ?>
                                            <option value="<?php echo $team['id']; ?>"><?php echo $team['team_name']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-sm-12" style="padding:20px 0;">
                        <input type="submit" class="btn border-theme pull-right" value="Leave Tournament" onclick="leaveTournament(<?php echo $info->id; ?>)">
                        <input type="submit" class="btn border-theme pull-left" value="Cancel" data-dismiss="modal" aria-hidden="true">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>