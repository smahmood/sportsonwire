<div class="divide80"></div>
<div class="container">
    <div class="row">
      

        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="heading pull-left">Upcoming Tournaments</h3>
                    <a href="#new-team-modal" data-toggle="modal" class="btn border-theme pull-right"><i class="fa fa-plus-circle"></i> Create New Tournament</a>
                </div>
            </div>
            <div class="row">
                <?php foreach($tournaments as $tourney){ ?>
                    <?php if(!empty($tourney)){ ?>
                    <div class="col-sm-3 margin30">
                        <div class="latest-new">
                            <img src="<?php echo base_url(); ?>uploads/<?php echo $tourney->image;?>" class="img-responsive" alt="">
                            <div class="panel-footer clearfix">
                                <center><h3 style="margin-bottom:4px;"><a href="<?php echo base_url() . 'footy/tournaments/details/' . str_replace(" ", "-", $tourney->name).'/'.$tourney->id; ?>"><?php echo $tourney->name; ?></a></h3>
								<p style="margin-bottom:4px;"><?php echo $tourney->aside; ?> aside</p>
                                <p style="margin-bottom:4px;">City : <?php echo $tourney->cityName; ?></p>
								</center>
                            </div>
                            
                        </div><!--latest news-->
                    </div><!--latest news col-->
                    <?php } ?>
                <?php } ?>
                
            </div>
        </div>

    </div>
</div>
<div class="clear"></div>
<div class="divide80"></div>
<!-- Modal -->
<div class="modal fade in" id="new-team-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?php echo base_url();?>footy/tournaments/create" id="new_tournament_form" method="POST" role="form" data-toggle="validator">
                <div class="modal-header text-center">
                    <h3 style="margin-top:33px;"><i class="fa fa-plus-circle"></i> Create New Tournament</h3>
                </div>
                <div class="modal-body clearfix">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Your Tournament Name</label>
                                    <input type="text" class="form-control" name="tournament_name" data-minlength="3" placeholder="Enter Tournament Name" data-error="Tournament name required" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Min Players</label>
                                    <input type="number" min="1" max="15" class="form-control" name="tournament_min_players"  placeholder="Enter Minimum Players" data-error="Min Players required" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
							<div class="col-sm-6">
                                <div class="form-group">
                                    <label>Max Players</label>
                                    <input type="number" min="1" max="15" class="form-control" name="tournament_max_players"  placeholder="Enter Maximum Players" data-error="Max Players required" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
							
							<div class="col-sm-6">
                                <div class="form-group">
                                    <label>Start Date</label>
                                    <input id="tournament_sdate" type="text" min="1" max="15" class="form-control" name="sdate"  placeholder="Select Start Date" data-error="Start Date required" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
							<div class="col-sm-6">
                                <div class="form-group">
                                    <label>End Date</label>
                                    <input id="tournament_edate" type="text" min="1" max="15" class="form-control" name="edate"  placeholder="Select End Date" data-error="End Date required" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
							
							<div class="col-sm-6">
                                <div class="form-group">
                                    <label>Gender</label>
                                    <select class="form-control" name="gender" data-error="Please specify gender" required>
                                        <option value="" disabled selected>Gender</option>
                                        <option value="1">Male</option>
                                        <option value="2">Female</option>
                                        <option value="3">Coed</option>
                                    </select>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Age group</label>
                                    <select class="form-control" name="age_group" data-error="Please specify age group" required>
                                        <option value="" disabled selected>Age Group</option>
                                        <option value="1">Under 13</option>
                                        <option value="2">13-18</option>
                                        <option value="3">18+</option>
										<option value="4">Any</option>
                                    </select>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
							<div class="col-sm-12">
								<div class="form-group">
									<label>Desctiption</label>
									<textarea required style="max-width:100%;" class="form-control" name="desc"  maxlength="1000" placeholder="Desctiption" ></textarea>
									<div class="help-block with-errors"></div>
								</div>
							</div>
							<div class="col-sm-6 margin30">
                                <div class="form-group">
                                    <label>City</label>
                                    <select class="form-control" name="city" id="cities" data-error="Please select city" required>
                                        <option value="" disabled selected>--Select City--</option>
                                        <?php if($cities){
                                            foreach($cities as $c){
                                                echo '<option  value="'.$c->id.'">'.$c->name.'</option>';
                                            }
                                        } ?>
                                    </select>
                                </div>
                            </div>
							
							<div class="col-sm-12">
								<div class="form-group">
									<label>Location</label>
									<textarea required style="max-width:100%;" class="form-control" name="location"  maxlength="1000" placeholder="Location" ></textarea>
									<div class="help-block with-errors"></div>
								</div>
							</div>
                        </div>
                       
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-sm-12" style="padding:20px 0;">
                        <input type="submit" class="btn border-theme pull-right" value="Create Tournament">
                        <a href="#" class="btn btn-default pull-left">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
