<div class="divide80"></div>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form action="<?php echo base_url();?>footy/edit" method="POST" role="form" data-validator="validator">
                <div class="panel panel-default clearfix">
                    <div class="panel-heading clearfix">
                        <h1 class="panel-title pull-left account-name">Edit Football Profile</h1>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3 account-info">
                                <span class="h4"><i class="fa fa-cogs"></i> <?php echo $basic->first_name. " " . $basic->last_name; ?></span>
                                <hr/>
                                <div class="form-group clearfix">
                                    <label for="position">Position</label>
                                    <input class="form-control" value="<?php echo $footy->player_position; ?>" placeholder="Position" type="text" id="position" name="position" required/>
                                </div>
                                <div class="divide10"></div>
                                <div class="form-group clearfix">
                                    <label for="foot_id">Preferred Foot</label>
                                    <select class="form-control" id="foot_id" name="foot" required>
                                        <option value="" disabled selected>Select Preferred Foot</option>
                                        <option <?php if($footy->player_foot==1){ echo 'selected'; } ?> value="1" >Left</option>
                                        <option <?php if($footy->player_foot==2){ echo 'selected'; } ?> value="2" >Right</option>
                                    </select>
                                </div>
                                <div class="divide10"></div>
                                <div class="form-group clearfix">
                                    <label for="number">Squad Number</label>
                                    <input class="form-control" value="<?php echo $footy->squad_number ?>" placeholder="Squad Number" type="number" id="number" name="number" min="0" required/>
                                </div>
                                <div class="divide10"></div>
                                <div class="form-group clearfix">
                                    <label for="weight">Weight</label>
                                    <input class="form-control" value="<?php echo $footy->weight ?>" placeholder="Weight" type="number" id="weight" name="weight" min="0" required/>
                                </div>
                                <div class="divide10"></div>
                                <div class="form-group clearfix">
                                    <label for="height">Height</label>
                                    <input class="form-control" value="<?php echo $footy->height ?>" placeholder="Height" type="text" id="height" name="height" step="any" min="0" required/>
                                </div>
                                <div class="divide10"></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer clearfix">
                        <input class="btn btn-primary pull-right" name="commit" type="submit" value="Save Info">
                        <a href="<?php echo base_url(); ?>" class="btn btn-default pull-left">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="divide80"></div>