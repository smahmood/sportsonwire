<div class="container">
	<!--<section class="me-intro parallax" data-stellar-background-ratio="0.5">
	    <div class="container text-left">
	        <h2 class="animated slideInLeft">Hello! myself assan</h2>
	        <p class="lead animated slideInLeft ">I am a Graphics and Web designer </p>
	        <p><a href="#" class="btn btn-lg btn-theme-bg">Hire Me</a></p>
	    </div>
	</section>me intro parallax-->
	<div class="row">
		<div class="col-md-12">
			<?php if($info->player_cover != null): ?>
			<div class="cover-back" style="background: url('<?php echo base_url() . 'uploads/' . $info->player_cover; ?>') no-repeat;">
			<?php else: ?>
			<div class="cover-back" style="background: url('<?php echo base_url() . 'img/team-cover-default.jpg'; ?>') no-repeat;">
			<?php endif ?>
				<?php if($info->id==$this->ion_auth->get_user_id()){ ?><div id="cover-button" class="cover-img-button"><i class="fa fa-camera"></i></div> <?php } ?>
				<div class="cover-photo" id="cover-header"></div>
			</div>
			<input type="hidden" class="cover_type" value="player" />
			<input type="hidden" class="cover_id" value="<?php echo $info->id ?>" />
		</div>
	</div>
	<div class="clear"></div>
	<div class="row">
		<div class="col-md-12">
			<div class="breadcrumb-wrap team-links">
			    <div class="navbar team-navbar">
		            <ul class="nav navbar-nav navbar-left team-navbar-nav">
		                <li class="dropdown">
		                    <a href="#">Photos</a>
		                </li>
		            </ul>
		        </div>
			</div><!--breadcrumbs-->
		</div>
	</div>
	<div class="clear"></div>
	<div class="divide80"></div>
	<div class="row">
		<div class="col-sm-4 margin30">
            <div class="latest-new">
                <img src="<?php echo base_url(); ?>uploads/<?php if($info->profile_picture==""){ echo "default.jpg"; }else{ echo $info->profile_picture; } ?>" class="img-responsive" alt="" style="width:100%;">
                <div class="l-news-desc account-info clearfix">
                    <h3><a href="#"><?php echo $info->first_name." ".$info->last_name ;?></a></h3>
					<p style="text-align:center;"><?php if($info->bio!=""){ echo substr($info->bio, 0, 70)."...";; } ?></p>
				<?php if($info->bio!=""){ ?>	<button class="btn border-theme pull-right ">Read More</button> <?php } ?>
                </div>
				
                <div class="panel-footer clearfix">
                    
                    <div class="pull-left clearfix">
						<p style="color:#989898;">Contact : <?php echo $info->phone ;?></p>
						<p style="color:#989898;">Email : <?php echo $info->email ;?></p>
						<p style="color:#989898;">Gender : <?php if($info->gender==1){ echo "Male"; } else{ echo "Female"; }  ;?></p>
					</div>
                </div>
            </div><!--latest news-->
            <div class="divide40"></div>
            <div class="box box-danger">
	            <div class="box-header with-border">
	              <h3 class="box-title">Teams</h3>
	              <div class="box-tools pull-right">
	              </div>
	            </div><!-- /.box-header -->
	            <div class="box-body no-padding">
	              <ul class="users-list clearfix">
	                <?php if($teams){ foreach($teams as $m){
						if($m->team_logo==""){ $m->team_logo="crest-soccer.png" ; }
							echo '<li>
							  <img src="'.base_url().'uploads/'.$m->team_logo.'" alt="User Image"/>
							  <a class="users-list-name" href="'.base_url().'footy/team/view/'.str_replace(" ", "-", $m->team_name).'/'.$m->id.'" >'.$m->team_name.'</a>
							 
							</li>';
						}
					}else {
						echo '<center><p>No Team Joined.</p></center>';
					}?>
	                
	              </ul><!-- /.users-list -->
	            </div><!-- /.box-body -->
	          </div><!--/.box -->
        </div><!--latest news col-->
        <div class="col-md-8">
			<div class="center-heading ">
				<button class="btn border-theme" onclick="$('#postImageToWall').hide();$('#postToWall').show();" > POST TO WALL </button>
				<button class="btn border-theme" onclick="$('#postToWall').hide();$('#postImageToWall').show();"  > UPLOAD IMAGE TO WALL </button>
			</div>	
			<div id="postToWall" class="clearfix" style="display:none">
				<form action="#" method="POST" id="postToWallForm" onsubmit="return postToWallPlayer()">
					<input maxlength="1000" type="text" required=""  placeholder="What's on your mind"  name="post" class="form-control">
					<input type="hidden" name="team" value="<?php echo $info->id;?>"/>
					<input class="btn border-theme pull-right " type="submit" value="POST" />
				</form>	
			</div>
			<div id="postImageToWall" class="clearfix" style="display:none">
				<form action="#" id="postImageToWallForm" onsubmit="return postImageToWallPlayer()" method="POST" onsubmit="return false">
					<input type="text"   placeholder="What's on your mind"  name="post" class="form-control">
					<input type="hidden" name="team" value="<?php echo $info->id;?>"/>
					<input type="hidden" name="pic" value="" id="wallpic"/>
					
					<input class="btn border-theme pull-right " type="submit" value="POST" />
					<input class="btn border-theme pull-right " onclick="$('#imageInput').click();" type="button" value="Upload" />
					<img style="display:none;"  src="" class="wallpicdisp pull-right" />
					<div style="width:70%;"><div  class="progress-bar progress-bar-success progress-bar-striped" id="progressbar" role="progressbar"
					aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:0%">
					<span id="progressPercent" >0</span>
					</div></div>
					
				</form>	
			</div>
        	<div class="center-heading">
                <h2>Recent <strong>Activity</strong></h2>
                <span class="center-line"></span>
            </div>
			<?php if($feed)
			{ ?>
			<ul class="timeline">
				<!-- timeline time label date("j M Y", $ts)-->
				<?php $date='';$count=0; foreach($feed as $f)
				{
					$count=$count+1;
					$tdate=explode(" ",$f->time_stamp);
					$tdate=$tdate[0];
					if($date=='')
					{ $date=$tdate; ?>
						<li class="time-label">
							<span class="bg-red"><?php $time = strtotime($date);echo date("j M Y", $time);?> </span>
						</li>
					<?php }
					else if($date!=$tdate)
					{ $date=$tdate; ?>
						<li class="time-label">
							<span <?php if($count%2==1){ ?>class="bg-red" <?php } else{ ?>class="bg-green" <?php } ?> ><?php $time = strtotime($date);echo date("j M Y", $time);?> </span>
						</li>
					<?php } 
					if($f->post_type==0)
					{ ?>
						<li>
							<i class="fa fa-envelope bg-blue"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i> 12:05</span>
								<h3 class="timeline-header"><a href="#">Support Team</a> sent you an email</h3>
								<div class="timeline-body">
									Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
									weebly ning heekya handango imeem plugg dopplr jibjab, movity
									jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
									quora plaxo ideeli hulu weebly balihoo...
								</div>
								<div class='timeline-footer'>
									<a class="btn btn-primary btn-xs">Read more</a>
									<a class="btn btn-danger btn-xs">Delete</a>
								</div>
							</div>
						</li>
						<!-- END timeline item -->
						<!-- timeline item -->
						<li>
							<i class="fa fa-user bg-aqua"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i> 5 mins ago</span>
								<h3 class="timeline-header no-border"><a href="#">Sarah Young</a> accepted your friend request</h3>
							</div>
						</li>
						<!-- END timeline item -->
						<!-- timeline item -->
						<li>
							<i class="fa fa-comments bg-yellow"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i> 27 mins ago</span>
								<h3 class="timeline-header"><a href="#">Jay White</a> commented on your post</h3>
								<div class="timeline-body">
									Take me to your leader!
									Switzerland is small and neutral!
									We are more like Germany, ambitious and misunderstood!
								</div>
								<div class='timeline-footer'>
									<a class="btn btn-warning btn-flat btn-xs">View comment</a>
								</div>
							</div>
						</li>
						<!-- END timeline item -->
						<!-- timeline time label -->
						<li class="time-label">
							<span class="bg-green">3 Jan. 2014</span>
						</li>
						<!-- /.timeline-label -->
						<!-- timeline item -->
						<li>
							<i class="fa fa-camera bg-purple"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i> 2 days ago</span>
								<h3 class="timeline-header"><a href="#">Mina Lee</a> uploaded new photos</h3>
								<div class="timeline-body">
									<img src="http://placehold.it/150x100" alt="..." class='margin' />
									<img src="http://placehold.it/150x100" alt="..." class='margin'/>
									<img src="http://placehold.it/150x100" alt="..." class='margin'/>
									<img src="http://placehold.it/150x100" alt="..." class='margin'/>
								</div>
							</div>	
						</li>
						<!-- END timeline item -->
						<!-- timeline item -->
						<li>
							<i class="fa fa-video-camera bg-maroon"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i> 5 days ago</span>
								<h3 class="timeline-header"><a href="#">Mr. Doe</a> shared a video</h3>
								<div class="timeline-body">
									<div class="embed-responsive embed-responsive-16by9">
										<iframe src="https://player.vimeo.com/video/124168998" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
									</div>
								</div>
								<div class="timeline-footer">
									<a href="#" class="btn btn-xs bg-maroon">See comments</a>
								</div>
							</div>
						</li>
						<!-- END timeline item -->
						<li>
							<i class="fa fa-clock-o bg-gray"></i>
						</li>
					<?php }
					if($f->post_type==2)
					{ ?>
						<li>
							<i class="fa fa-user bg-aqua"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i> <?php $temp=explode(" ",$f->time_stamp); $temp=explode(":",$temp[1]); echo $temp[0].":".$temp[1]; ?> </span>
								<h3 class="timeline-header no-border">Joined Team<a href="#"><b> <?php echo $f->team->team_name ?></b></a> </h3>
							</div>
						</li>
					<?php }
					if($f->post_type==3)
					{ ?>
						<li>
							<i class="fa fa-camera bg-purple"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i>  <?php $temp=explode(" ",$f->time_stamp); $temp=explode(":",$temp[1]); echo $temp[0].":".$temp[1]; ?> </span>
								<h3 class="timeline-header"><a href="#"><?php echo $info->first_name." ".$info->last_name; ?></a> upated his cover photo</h3>
								<div class="timeline-body">
									<img style="width:96%;"src="<?php echo base_url();?>uploads/<?php echo $f->cover->path;?>" alt="..." class='margin' />
								</div>
							</div>	
						</li>
					<?php }
					if($f->post_type==5)
					{ if($f->wall->post_type==1){ ?>
						<li>
							<i class="fa fa-comments bg-yellow"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i> <?php $temp=explode(" ",$f->time_stamp); $temp=explode(":",$temp[1]); echo $temp[0].":".$temp[1]; ?> </span>
								<h3 class="timeline-header"><a href="<?php echo base_url();?>footy/playerProfile/<?php echo $f->user->id;?>"><?php echo $f->user->first_name." ".$f->user->last_name; ?></a> posted on your wall</h3>
								<div class="timeline-body">
									<?php echo $f->wall->text; ?>
								</div>
							</div>
						</li>
					<?php }else{
					?>
						<li>
							<i class="fa fa-comments bg-yellow"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i> <?php $temp=explode(" ",$f->time_stamp); $temp=explode(":",$temp[1]); echo $temp[0].":".$temp[1]; ?> </span>
								<h3 class="timeline-header"><a href="<?php echo base_url();?>footy/playerProfile/<?php echo $f->user->id;?>"><?php echo $f->user->first_name." ".$f->user->last_name; ?></a> posted on your wall</h3>
								<div class="timeline-body">
									<b style="margin-botton:8px;"><?php echo $f->wall->text; ?></b>
									<img src="<?php echo base_url();?>uploads/<?php echo $f->wall->image_path; ?>" style="width:96%"/>
								</div>
							</div>
						</li>
					<?php 
					
					}	}	
				}
				?>
				
				<!-- /.timeline-label -->
				<!-- timeline item -->
				<li>
					<i class="fa fa-clock-o bg-gray"></i>
				</li>
				<div class="divide40"></div>
			</ul>
			<?php } else
			{
				echo '<center><p>There is no recent activity on this page.</p></center>';
			}  ?>
        </div>
	</div>
	<div class="clear"></div>
	<div class="divide80"></div>
</div>
<form style="display:none;" action="<?php echo base_url();?>sports/uploadImage" method="post" enctype="multipart/form-data" id="MyUploadForm">
		<input onchange="$('#MyUploadForm').submit();" name="image_file" id="imageInput" type="file" />
		<input type="submit"  id="submit-btn" value="Upload" />

		</form>
		<div style="display:none;" id="output"></div>