var base_url='';
var uploading=0;

/*var client = new Faye.Client('http://ed446de2.fanoutcdn.com/bayeux');
client.subscribe('/create', function (data) {
	alert('got data: ' + data);
});*/

$(".ground_options").addClass("ground_hide_options");

$("#cover-button").hover(function(){
	$(this).append($("<span> Upload Cover Photo</span>"));
}, function(){
	$(this).find("span:last").remove();
});

$(".login_form").submit(function(event){
	$(".remove-alert").remove();
	$.ajax({
		url: base_url+"sports/login",
		context: document.body,
		data : $(this).serialize(),
		method : "POST"
	}).done(function(data){
		var temp = JSON.parse(data);
		if(temp.status == "success"){
			window.location.href = base_url;
		}else{
			$(".authentication-alert").html("Username or Password Incorrect");
			$(".authentication-alert").fadeIn();
		}
	});
	event.preventDefault();
});

$(".signup-form").submit(function(event){
	if(uploading==1)
	{
		alert('Profile Picture Upload in Progress');
		return false;
	}
	$.ajax({
		url: base_url+"sports/signup",
		context: document.body,
		data : $(this).serialize(),
		method : "POST"
	}).done(function(data) {
		var temp=JSON.parse(data);
		if(temp.status == "success"){
			window.location.href = base_url;
		}else{
			$(".authentication-alert").html(temp.messages);
			$(".authentication-alert").fadeIn();
		}
	});
	return false;
});

$(document).ready(function() { 
	var options = { 
		target: '#output',   // target element(s) to be updated with server response 
		beforeSubmit: beforeSubmit,  // pre-submit callback 
		success: afterSuccess,  // post-submit callback 
		uploadProgress: OnProgress, //upload progress callback 
		error:  afterFailure,
		resetForm: true        // reset the form after successful submit 
	}; 
	
		var austDay = new Date();
		austDay = new Date(austDay.getFullYear() + 1, 1 - 1, -202);
		$('#defaultCountdown').countdown({until: austDay});
		$('#year').text(austDay.getFullYear());
		
	$('#MyUploadForm').submit(function() { 
		$(this).ajaxSubmit(options);  			
		// always return false to prevent standard browser submit and page navigation 
		return false; 
	});

	/**** Team and Player cover photo functionality **********/

	var cover_type = $(".cover_type").val();
	var cover_id = $(".cover_id").val();

	var cover_cropperOptions = {
		uploadUrl: base_url+'sports/coverphoto/upload/'+cover_type+'/'+cover_id,
		cropUrl: base_url+'sports/coverphoto/crop/'+cover_type+'/'+cover_id,
		customUploadButtonId: 'cover-button',
		loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div>',
		onAfterImgCrop: function(){$(".cropControls").hide();}
	};
	var cover_cropperHeader = new Croppic('cover-header', cover_cropperOptions); 

	/**** End of Team and Player cover photo functionality **********/

	/**** Team crest photo functionality **********/

	var crest_cropperOptions = {
		uploadUrl: base_url+'sports/crestphoto/upload/'+cover_id,
		cropUrl: base_url+'sports/crestphoto/crop/'+cover_id,
		customUploadButtonId: 'crest-button',
		doubleZoomControls: false,
		rotateControls: false,
		loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div>',
		onAfterImgUpload: function(){$(".cropControlReset").hide();},
		onAfterImgCrop: function(){$(".cropControls").hide();}
	};
	var crest_cropperHeader = new Croppic('crest-header', crest_cropperOptions);

	/**** End of Team crest photo functionality **********/

	var engine, remoteHost, template, empty;

  	$.support.cors = true;

	remoteHost = base_url;
	template = Handlebars.compile($("#result-template").html());
	empty = Handlebars.compile($("#empty-template").html());

	var page_type = $("#page_type").val();

	engine = new Bloodhound({
		identify: function(o) { return o.id_str; },
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name', 'screen_name'),
		dupDetector: function(a, b) { return a.id_str === b.id_str; },
		//prefetch: remoteHost + '/sports/prefetch',
		remote: {
			url: remoteHost + 'sports/getTeams?type='+page_type+'&q=%QUERY',
			wildcard: '%QUERY'
		}
	});

	// ensure default users are read on initialization
	engine.get('1090217586', '58502284', '10273252', '24477185')

	function engineWithDefaults(q, sync, async) {
		if (q === '') {
			sync(engine.get('1090217586', '58502284', '10273252', '24477185'));
			async([]);
		}else{
			engine.search(q, sync, async);
		}
	}

	$('#demo-input').typeahead({
		hint: $('.Typeahead-hint'),
		menu: $('.Typeahead-menu'),
		minLength: 0,
		classNames: {
			open: 'is-open',
			empty: 'is-empty',
			cursor: 'is-active',
			suggestion: 'Typeahead-suggestion',
			selectable: 'Typeahead-selectable'
		}
	}, {
		source: engineWithDefaults,
		displayKey: 'screen_name',
		templates: {
		suggestion: template,
		empty: empty
		}
	})
	.on('typeahead:asyncrequest', function() {
		$('.Typeahead-spinner').show();
	})
	.on('typeahead:asynccancel typeahead:asyncreceive', function() {
		$('.Typeahead-spinner').hide();
	});

}); 

function afterFailure(){
	uploading=0;
	$('#progressbar').hide();
	
}

function OnProgress(event, position, total, percentComplete){
	//Progress bar
	$('#progressbar').show();
	$('#progressbar').width(percentComplete + '%') ;//update progressbar percent complete
	$('#progressPercent').html(percentComplete + '%'); //update status text
	
}

function afterSuccess(data){
	uploading=0;
	var temp=JSON.parse(data);
	if(temp.status == "success"){
		$('#progressbar').hide();
		$('#profilepic').val(temp.messages);
		$('#wallpic').val(temp.messages);
		$('.wallpicdisp').attr('src',base_url+'uploads/thumb_'+temp.messages);
		$('.wallpicdisp').show();
		$('.profile_pic_logged_in_user').attr('src',base_url+'uploads/thumb_'+temp.messages);
	}
	
}

//function to check file size before uploading.
function beforeSubmit(){
	$('#progressbar').width('0%') ;
	$('#progressPercent').html('0%');
	//check whether browser fully supports all File API
   if (window.File && window.FileReader && window.FileList && window.Blob)
	{
		
		
		
		var fsize = $('#imageInput')[0].files[0].size; //get file size
		var ftype = $('#imageInput')[0].files[0].type; // get file type
		

		//allow only valid image file types 
		switch(ftype)
		{
			case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
				break;
			default:
				alert(" Unsupported file type!");
				return false
		}
		
		//Allowed file size is less than 1 MB (1048576)
		if(fsize>1048576) 
		{
			alert("</b> Too big Image file! Please reduce the size of your photo using an image editor.");
			return false
		}
		uploading=1;		
		$('#submit-btn').hide(); //hide submit button
		$('#loading-img').show(); //hide submit button
		$("#output").html("");  
	}
	else
	{
		//Output error to older browsers that do not support HTML5 File API
		alert("Please upgrade your browser, because your current browser lacks some new features we need!");
		return false;
	}
}

//function to format bites bit.ly/19yoIPO
function bytesToSize(bytes) {
   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
   if (bytes == 0) return '0 Bytes';
   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}
function getcities(id){
	$.ajax({
		url: base_url+"sports/getCities",
		context: document.body,
		data : {id,id},
		method : "POST"
	}).done(function(data) {
		$('#cities').html(data);
	});
}

$("#team_ground").change(function(){
	//alert($(this).val());
	if($(this).val() == "1"){
		$(".ground_options").show();
	}else if($(this).val() == "0"){
		$(".ground_options").hide();
	}
});

function AcceptRejectTeam(id,status)
{
	$.ajax({
		url: base_url+"sports/AcceptRejectTeam",
		context: document.body,
		data : {id:id,status:status},
		method : "POST"
	}).done(function(data) {
		location.reload();
	});
}
function AcceptRejectPlayer(id,status,player)
{
	$.ajax({
		url: base_url+"sports/AcceptRejectPlayer",
		context: document.body,
		data : {id:id,status:status,player:player},
		method : "POST"
	}).done(function(data) {
		location.reload();
	});
}
function joinTeam(id)
{
	$.ajax({
		url: base_url+"sports/joinTeam",
		context: document.body,
		data : {id:id},
		method : "POST"
	}).done(function(data) {
		location.reload();
	});
}	
function joinTournament(tournament_id)
{
    var team_id=$('#team_id_join').val();
    $.ajax({
        url: base_url+"footy/joinTournament",
        context: document.body,
        data: {tournament_id:tournament_id,team_id:team_id},
        method: "POST"
    }).done(function(data){
        location.reload();
    });
}
function leaveTournament(tournament_id)
{
    var team_id=$('#team_id_leave').val();
    $.ajax({
        url: base_url+"footy/leaveTournament",
        context: document.body,
        data: {tournament_id:tournament_id,team_id:team_id},
        method: "POST"
    }).done(function(data){
        location.reload();
    });
}
function SendMessage()
{
    $.ajax({
        url: base_url+"Sports/send_msg/activation",
        context: document.body
    }).done(function(data){
        location.reload();
    });
}
function inviteEmail()
{
	$.ajax({
		url: base_url+"sports/inviteEmail",
		context: document.body,
		data : $('#addmemberform').serialize(),
		method : "POST"
	}).done(function(data) {
		var temp=JSON.parse(data);
		alert(temp.message);
		$('#addmemberform')[0].reset();

	});
	return false;
}	
function postToWall()
{
	$.ajax({
		url: base_url+"sports/postToWall",
		context: document.body,
		data : $('#postToWallForm').serialize(),
		method : "POST"
	}).done(function(data) {
		location.reload();
	});
	return false;
}
function postImageToWall()
{
	if($('#wallpic').val()=="")
	{
		alert('Please Upload a Picture First');
		return false;
	}
	if(uploading==1)
	{
		alert('Picture Upload in Progress');
		return false;
	}
	$.ajax({
		url: base_url+"sports/postImageToWall",
		context: document.body,
		data : $('#postImageToWallForm').serialize(),
		method : "POST"
	}).done(function(data) {
		location.reload();
	});
	return false;
}
function postToWallPlayer()
{
	$.ajax({
		url: base_url+"sports/postToWallPlayer",
		context: document.body,
		data : $('#postToWallForm').serialize(),
		method : "POST"
	}).done(function(data) {
		location.reload();
	});
	return false;
}
function postImageToWallPlayer()
{
	if($('#wallpic').val()=="")
	{
		alert('Please Upload a Picture First');
		return false;
	}
	if(uploading==1)
	{
		alert('Picture Upload in Progress');
		return false;
	}
	$.ajax({
		url: base_url+"sports/postImageToWallPlayer",
		context: document.body,
		data : $('#postImageToWallForm').serialize(),
		method : "POST"
	}).done(function(data) {
		location.reload();
	});
	return false;
}
