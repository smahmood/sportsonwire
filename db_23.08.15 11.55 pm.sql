-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 23, 2015 at 09:55 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sportsonwire`
--

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES
(1, 'Lahore', 1),
(2, 'Dehli', 2),
(3, 'Karachi', 1),
(4, 'Islamabad', 1),
(5, 'Gujranwala', 1),
(6, 'Mandi Bahauddin', 1),
(7, 'Multan', 1),
(8, 'Gujrat', 1);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `country_code` int(11) NOT NULL,
  `flag` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `country_code`, `flag`) VALUES
(1, 'Pakistan', 92, 'pakistan.png');

-- --------------------------------------------------------

--
-- Table structure for table `covers`
--

CREATE TABLE IF NOT EXISTS `covers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL COMMENT '1 for user, 2 for team',
  `type_id` int(11) NOT NULL,
  `path` varchar(150) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `covers`
--

INSERT INTO `covers` (`id`, `type`, `type_id`, `path`, `timestamp`) VALUES
(1, 2, 15, 'teamCover_07-10-2015_17-32-53_21915.jpeg', '2015-07-10 12:32:55'),
(2, 1, 13, 'teamCover_07-10-2015_17-38-08_32636.jpeg', '2015-07-10 12:38:09'),
(3, 2, 15, 'teamCover_08-05-2015_19-10-19_32697.jpeg', '2015-08-05 14:10:20');

-- --------------------------------------------------------

--
-- Table structure for table `cric_profile`
--

CREATE TABLE IF NOT EXISTS `cric_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_role` int(11) NOT NULL COMMENT '1 for batting 2 for bowling 3 for wicketkeeper 3 for alrounder',
  `batting_style` int(11) NOT NULL COMMENT '1 for left 2 for right',
  `bowling_style` int(11) NOT NULL COMMENT '1 for fast 2 for medium 3 for leg spin 4 for off spin',
  `user_id` int(11) NOT NULL,
  `bowling_arm` int(11) NOT NULL COMMENT '1 for left 2 for right',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `cric_profile`
--

INSERT INTO `cric_profile` (`id`, `player_role`, `batting_style`, `bowling_style`, `user_id`, `bowling_arm`) VALUES
(2, 2, 2, 1, 11, 2),
(3, 2, 1, 1, 3, 1),
(4, 1, 1, 1, 13, 1);

-- --------------------------------------------------------

--
-- Table structure for table `featured_post`
--

CREATE TABLE IF NOT EXISTS `featured_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `featured_type` int(11) NOT NULL COMMENT '1 for player 2 for team',
  `featured_id` int(11) NOT NULL,
  `featured_text` varchar(1000) NOT NULL,
  `sports` int(11) NOT NULL COMMENT '1 for football 2 for Cricket',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `featured_post`
--

INSERT INTO `featured_post` (`id`, `featured_type`, `featured_id`, `featured_text`, `sports`) VALUES
(1, 1, 13, 'Testing Featured Player Testing Featured Player Testing Featured Player Testing Featured Player Testing Featured Player Testing Featured Player Testing Featured Player Testing Featured Player Testing Featured Player Testing Featured Player Testing Featured Player Testing Featured Player Testing Featured Player Testing Featured Player Testing Featured Player ', 1),
(2, 2, 15, 'Testing Featured Team Testing Featured Team Testing Featured Team Testing Featured Team Testing Featured Team Testing Featured Team Testing Featured Team Testing Featured Team Testing Featured Team Testing Featured Team Testing Featured Team Testing Featured Team Testing Featured Team Testing Featured Team Testing Featured Team ', 1);

-- --------------------------------------------------------

--
-- Table structure for table `feed`
--

CREATE TABLE IF NOT EXISTS `feed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id` int(11) NOT NULL,
  `post_type` int(11) NOT NULL COMMENT '1 For Image | 2 For New User | 3 For Cover Change | 4 For Logo Change | 5 For Wall Comment',
  `post_id` int(11) NOT NULL,
  `post_by` int(11) NOT NULL COMMENT '1 For Team | 2 For User',
  `post_by_id` int(11) NOT NULL COMMENT 'only if post_by 2',
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `feed`
--

INSERT INTO `feed` (`id`, `team_id`, `post_type`, `post_id`, `post_by`, `post_by_id`, `time_stamp`) VALUES
(1, 15, 3, 1, 1, 0, '2015-07-10 12:32:55'),
(2, 15, 3, 3, 1, 0, '2015-08-05 14:10:20'),
(3, 15, 5, 3, 2, 13, '2015-08-05 14:10:34'),
(4, 15, 5, 4, 2, 13, '2015-08-05 14:10:48');

-- --------------------------------------------------------

--
-- Table structure for table `feed_tournament`
--

CREATE TABLE IF NOT EXISTS `feed_tournament` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tournament_id` int(11) NOT NULL,
  `post_type` int(11) NOT NULL COMMENT '1 For Image | 2 For New Team | 3 For Cover Change | 4 For Profile Change | 5 For Wall Comment',
  `post_id` int(11) NOT NULL,
  `post_by` int(11) NOT NULL COMMENT '2 For User',
  `post_by_id` int(11) NOT NULL COMMENT 'only if post_by 2',
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `feed_tournament`
--

INSERT INTO `feed_tournament` (`id`, `tournament_id`, `post_type`, `post_id`, `post_by`, `post_by_id`, `time_stamp`) VALUES
(1, 1, 5, 2, 1, 1, '2015-08-05 13:52:49');

-- --------------------------------------------------------

--
-- Table structure for table `feed_user`
--

CREATE TABLE IF NOT EXISTS `feed_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `post_type` int(11) NOT NULL COMMENT '1 For Image | 2 For New Team | 3 For Cover Change | 4 For Profile Change | 5 For Wall Comment',
  `post_id` int(11) NOT NULL,
  `post_by` int(11) NOT NULL COMMENT '2 For User',
  `post_by_id` int(11) NOT NULL COMMENT 'only if post_by 2',
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `feed_user`
--

INSERT INTO `feed_user` (`id`, `user_id`, `post_type`, `post_id`, `post_by`, `post_by_id`, `time_stamp`) VALUES
(1, 13, 5, 1, 2, 13, '2015-07-10 12:35:56'),
(2, 13, 5, 2, 2, 13, '2015-07-10 12:36:23'),
(3, 13, 3, 2, 2, 0, '2015-07-10 12:38:09');

-- --------------------------------------------------------

--
-- Table structure for table `footy_profile`
--

CREATE TABLE IF NOT EXISTS `footy_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `player_position` varchar(10) NOT NULL,
  `player_foot` int(11) NOT NULL COMMENT '1 for left 2 for right',
  `squad_number` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `footy_profile`
--

INSERT INTO `footy_profile` (`id`, `user_id`, `player_position`, `player_foot`, `squad_number`, `weight`, `height`) VALUES
(6, 13, 'Defender', 2, 23, 40, 6),
(7, 14, 'Playmaker', 2, 10, 59, 172);

-- --------------------------------------------------------

--
-- Table structure for table `grounds`
--

CREATE TABLE IF NOT EXISTS `grounds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `type` int(11) NOT NULL COMMENT '1 for Football 2 for Cricket',
  `has_poles` int(11) NOT NULL COMMENT 'Only if type is 1',
  `ground_size` int(11) NOT NULL COMMENT 'Only if type is 2 ',
  `lat` int(11) NOT NULL,
  `long` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `grounds`
--

INSERT INTO `grounds` (`id`, `name`, `type`, `has_poles`, `ground_size`, `lat`, `long`) VALUES
(1, 'Estadi Lionel Messi', 1, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `invited_users`
--

CREATE TABLE IF NOT EXISTS `invited_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invited_by` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `team_id` int(11) NOT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE IF NOT EXISTS `teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_name` varchar(100) NOT NULL,
  `team_logo` varchar(200) NOT NULL DEFAULT 'crest-soccer.png',
  `team_cover` varchar(150) DEFAULT NULL,
  `sports` int(1) NOT NULL COMMENT '1 for football 2 for cricket',
  `gender` int(1) NOT NULL COMMENT '1 for male 2 for female and 3 for coed',
  `age_group` int(1) NOT NULL COMMENT '1 for under 13, 2 for 13-18, 3 for 18+',
  `country_id` int(25) NOT NULL,
  `city_id` int(100) NOT NULL COMMENT 'link to cities table',
  `bio` varchar(1000) NOT NULL,
  `has_ground` tinyint(1) NOT NULL COMMENT 'whether a team has a ground or not',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `team_name`, `team_logo`, `team_cover`, `sports`, `gender`, `age_group`, `country_id`, `city_id`, `bio`, `has_ground`) VALUES
(15, 'Test Mustangs 2', 'teamCover_08-05-2015_19-12-55_1253.png', 'teamCover_08-05-2015_19-10-19_32697.jpeg', 1, 1, 1, 1, 1, 'Test', 0),
(16, 'Mustangs Cricket', 'crest-soccer.png', NULL, 2, 1, 1, 1, 1, '', 0),
(17, 'Real Lahore', 'crest-soccer.png', NULL, 1, 1, 1, 1, 1, '', 0),
(18, 'Catalunians', 'crest-soccer.png', NULL, 1, 1, 4, 1, 1, '', 0),
(19, 'Blaugrana', 'crest-soccer.png', NULL, 1, 1, 4, 1, 4, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `team_ground`
--

CREATE TABLE IF NOT EXISTS `team_ground` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id` int(11) NOT NULL,
  `ground_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `team_ground`
--

INSERT INTO `team_ground` (`id`, `team_id`, `ground_id`) VALUES
(1, 19, 1);

-- --------------------------------------------------------

--
-- Table structure for table `team_members`
--

CREATE TABLE IF NOT EXISTS `team_members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role` int(11) NOT NULL COMMENT '1 for Manager 2 for Captain 3 for simple player',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0 For pending request |1 for accepted',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `team_members`
--

INSERT INTO `team_members` (`id`, `team_id`, `user_id`, `role`, `status`) VALUES
(22, 15, 13, 1, 1),
(23, 16, 13, 1, 0),
(24, 17, 13, 1, 1),
(25, 18, 14, 1, 1),
(26, 19, 14, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tournaments`
--

CREATE TABLE IF NOT EXISTS `tournaments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `desc` text NOT NULL,
  `type` int(11) NOT NULL,
  `image` varchar(100) NOT NULL DEFAULT 'tournament_default.jpg',
  `location` text NOT NULL,
  `sports` int(11) NOT NULL,
  `sdate` datetime NOT NULL,
  `gender` int(11) NOT NULL,
  `agegroup` int(11) NOT NULL,
  `min_players` int(11) NOT NULL,
  `max_players` int(11) NOT NULL,
  `edate` datetime NOT NULL,
  `city` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tournaments`
--

INSERT INTO `tournaments` (`id`, `name`, `created_by`, `desc`, `type`, `image`, `location`, `sports`, `sdate`, `gender`, `agegroup`, `min_players`, `max_players`, `edate`, `city`) VALUES
(1, 'Tournament', 13, 'Test Desc', 0, 'tournament_default.jpg', 'Test Loc', 1, '0000-00-00 00:00:00', 1, 4, 5, 10, '0000-00-00 00:00:00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tournament_teams`
--

CREATE TABLE IF NOT EXISTS `tournament_teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tournament_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 for pending, 1 for approved',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Teams which have joined a specific tournament' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `msgs_count` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Number of msgs used by a user in 1 day',
  `msgs_day` varchar(30) DEFAULT NULL COMMENT 'Msgs day',
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `profile_picture` varchar(100) NOT NULL,
  `player_cover` varchar(150) DEFAULT NULL,
  `gender` int(11) NOT NULL COMMENT '1 for male 2 for female',
  `bio` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `msgs_count`, `msgs_day`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `country_id`, `city_id`, `profile_picture`, `player_cover`, `gender`, `bio`) VALUES
(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '0', 0, NULL, NULL, NULL, NULL, 1268889823, 1432063576, 1, 'Admin', 'istrator', 'ADMIN', '0', 0, 0, '', NULL, 0, ''),
(13, '127.0.0.1', 'omar', '$2y$08$Jg30JCV8DWbmDvjjs2wwHOPfxjdjb1yIGKb6qpGiHX0D/bejKZ66i', NULL, 'omarfarooq.nu@gmail.com', '0', 0, NULL, NULL, NULL, NULL, 1433115857, 1438934385, 1, 'Omar', 'Farooq', NULL, '03084328339', 1, 1, '935208_10200618031884880_1270664941_n_419559867.jpg', 'teamCover_07-10-2015_17-38-08_32636.jpeg', 1, ''),
(14, '::1', 'farrukh', '$2y$08$JWcpE8dkYBOYOjAu/TTHMOsY4MtigwbfTk/9HAV9lf7Ifi/ml.U/.', NULL, 'farrukhrana765@gmail.com', '0', 0, '2015:08:23', NULL, NULL, NULL, 1439120586, 1440359544, 1, 'Farrukh', 'Mushtaq', NULL, '+923227955317', 1, 1, '', NULL, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(5, 1, 1),
(16, 13, 2),
(17, 14, 2);

-- --------------------------------------------------------

--
-- Table structure for table `wall_posts`
--

CREATE TABLE IF NOT EXISTS `wall_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wall` int(11) NOT NULL COMMENT '1 For Team 2 For user 3 for Tournament',
  `post_by` int(11) NOT NULL,
  `post_type` int(11) NOT NULL COMMENT '1 For Comment 2 For Image',
  `text` varchar(1000) NOT NULL,
  `image_path` varchar(200) NOT NULL,
  `wall_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `wall_posts`
--

INSERT INTO `wall_posts` (`id`, `wall`, `post_by`, `post_type`, `text`, `image_path`, `wall_id`) VALUES
(1, 2, 13, 1, 'Testinggg', '', 13),
(2, 2, 13, 2, 'Testing', '935208_10200618031884880_1270664941_n_1259454173.jpg', 13),
(3, 1, 13, 1, 'heloooo', '', 15),
(4, 1, 13, 2, '', '471375_10150609698056571_1430995990_o_503773063.jpg', 15);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
